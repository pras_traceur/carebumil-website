<?php

use Phinx\Seed\AbstractSeed;

class PeranPenggunaRinciSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
		$data = array();
		
        $stmt = $this->query('SELECT * FROM pengguna'); // returns PDOStatement
        $rows = $stmt->fetchAll(); // returns the result as an array
		
		$stmt1 = $this->query('SELECT * FROM peran_rinci'); // returns PDOStatement
        $rows1 = $stmt1->fetchAll(); // returns the result as an array
		
		$i = 1;
		
		foreach($rows as $key => $row){
			foreach($rows1 as $key1 => $row1){
				$arr = array();
				
				$arr['id_peran_pengguna_rinci'] = $i;
				$arr['id_peran_pengguna'] = $row['id_peran_pengguna'];
				$arr['id_peran_rinci'] = $row1['id_peran_rinci'];
				$arr['status'] = 1;
				$arr['tanggal_buat'] = date('Y-m-d h:i:s');
				$arr['tanggal_ubah'] = date('Y-m-d h:i:s');
				
				$data[] = $arr;
				
				$i++;
			}
		}

		$posts = $this->table('peran_pengguna_rinci');
        $posts->insert($data)->save();
    }
}
