<?php

use Phinx\Seed\AbstractSeed;

class PenggunaSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
		$data = array(
            array(
				'id_pengguna' => 1
				,'id_peran_pengguna' => 1
				, 'nama_pengguna' => 'Administrator'
				, 'username' => 'administrator'
				, 'password' => md5('administrator')
				, 'tanggal_lahir_pengguna' => '1980-10-10'
				, 'tempat_lahir_pengguna' => 'Jakarta Selatan'
				, 'email_pengguna' => 'administrator@example.com'
				, 'alamat_pengguna' => 'Jakarta Selatan'
				, 'telepon_pengguna' => '081222224444'
				, 'status' => 1
				, 'tanggal_buat' => date('Y-m-d h:i:s')
				, 'tanggal_ubah' => date('Y-m-d h:i:s')
			)
         );

        $posts = $this->table('pengguna');
        $posts->insert($data)->save();
    }
}
