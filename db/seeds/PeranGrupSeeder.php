<?php

use Phinx\Seed\AbstractSeed;

class PeranGrupSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
		$data = array(
            array('id_peran_grup' => 1, 'nama_peran_grup' => 'Dasbor', 'paten' => 'Dasbor', 'flag' => 'DASBOR', 'status' => 1, 'tanggal_buat' => date('Y-m-d h:i:s'), 'tanggal_ubah' => date('Y-m-d h:i:s'))
			,array('id_peran_grup' => 2, 'nama_peran_grup' => 'Pengguna', 'paten' => 'Pengguna', 'flag' => 'MASTER', 'status' => 1, 'tanggal_buat' => date('Y-m-d h:i:s'), 'tanggal_ubah' => date('Y-m-d h:i:s'))
            ,array('id_peran_grup' => 3, 'nama_peran_grup' => 'Peran Pengguna', 'paten' => 'PeranPengguna', 'flag' => 'MASTER', 'status' => 1, 'tanggal_buat' => date('Y-m-d h:i:s'), 'tanggal_ubah' => date('Y-m-d h:i:s'))
            ,array('id_peran_grup' => 4, 'nama_peran_grup' => 'Pos', 'paten' => 'Pos', 'flag' => 'MASTER', 'status' => 1, 'tanggal_buat' => date('Y-m-d h:i:s'), 'tanggal_ubah' => date('Y-m-d h:i:s'))
            ,array('id_peran_grup' => 5, 'nama_peran_grup' => 'Pos Kategori', 'paten' => 'PosKategori', 'flag' => 'MASTER', 'status' => 1, 'tanggal_buat' => date('Y-m-d h:i:s'), 'tanggal_ubah' => date('Y-m-d h:i:s'))
            ,array('id_peran_grup' => 6, 'nama_peran_grup' => 'Multimedia', 'paten' => 'Multimedia', 'flag' => 'MASTER', 'status' => 1, 'tanggal_buat' => date('Y-m-d h:i:s'), 'tanggal_ubah' => date('Y-m-d h:i:s'))
         );

        $posts = $this->table('peran_grup');
        $posts->insert($data)->save();
    }
}
