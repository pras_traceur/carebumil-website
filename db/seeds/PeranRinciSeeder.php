<?php

use Phinx\Seed\AbstractSeed;

class PeranRinciSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
		$data = array();
		
		$act = array('Mengakses','Tambah', 'Edit', 'Menghapus', 'Ekspor');

        $stmt = $this->query('SELECT * FROM peran_grup'); // returns PDOStatement
        $rows = $stmt->fetchAll(); // returns the result as an array
		
		$i = 1;
		
		foreach($rows as $key => $row){
			foreach($act as $key => $val){
				$arr = array();
				
				$arr['id_peran_rinci'] = $i;
				$arr['id_peran_grup'] = $row['id_peran_grup'];
				$arr['nama_peran_rinci'] = $row['paten'] . $val;
				$arr['deskripsi'] = '-';
				$arr['status'] = 1;
				$arr['tanggal_buat'] = date('Y-m-d h:i:s');
				$arr['tanggal_ubah'] = date('Y-m-d h:i:s');
				
				$data[] = $arr;
				
				$i++;
			}
		}
		
		$posts = $this->table('peran_rinci');
        $posts->insert($data)->save();
    }
}
