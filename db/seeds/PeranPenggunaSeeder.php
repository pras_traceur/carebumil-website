<?php

use Phinx\Seed\AbstractSeed;

class PeranPenggunaSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
		$data = array(
            array(
				'id_peran_pengguna' => 1
				, 'nama_peran_pengguna' => 'Administrator'
				, 'status' => 1
				, 'deskripsi' => 'Peran yang dapat mengelola semua fitur yang ada pada sistem'
				, 'tanggal_buat' => date('Y-m-d h:i:s')
				, 'tanggal_ubah' => date('Y-m-d h:i:s')
			),
         );

        $posts = $this->table('peran_pengguna');
        $posts->insert($data)->save();
    }
}
