/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50720
 Source Host           : localhost
 Source Database       : start_dev

 Target Server Type    : MySQL
 Target Server Version : 50720
 File Encoding         : utf-8

 Date: 09/15/2018 16:12:34 PM
*/

SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `pengaturan`
-- ----------------------------
DROP TABLE IF EXISTS `pengaturan`;
CREATE TABLE `pengaturan` (
  `id_pengaturan` int(3) NOT NULL AUTO_INCREMENT,
  `field` varchar(50) NOT NULL,
  `label` varchar(50) NOT NULL,
  `tipe` varchar(20) NOT NULL,
  `tipe_param_value` varchar(100) DEFAULT NULL,
  `deskripsi` varchar(200) DEFAULT NULL,
  `foto` varchar(255) DEFAULT NULL,
  `grup` varchar(50) NOT NULL,
  `flag` tinyint(1) NOT NULL DEFAULT '1',
  `dibuat_oleh` int(11) NOT NULL,
  `tanggal_buat` datetime NOT NULL,
  `tanggal_ubah` datetime DEFAULT NULL,
  PRIMARY KEY (`id_pengaturan`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
--  Records of `pengaturan`
-- ----------------------------
BEGIN;
INSERT INTO `pengaturan` VALUES ('1', 'WEBSITE_LOGO', 'Logo', 'img', null, 'http://portal.mahirr.id/uploads/multimedia/logo.png', null, 'website', '0', '1', '2017-12-25 09:36:58', '2017-12-25 09:50:37'), ('2', 'KONTAK_ALAMAT', 'Alamat 1', 'textarea', null, 'LPSE KOTA BEKASI, Jl. A. Yani No 1 Bekasi', null, 'kontak', '1', '1', '2017-12-25 09:36:58', '2017-12-25 09:36:58'), ('3', 'KONTAK_ALAMAT_TAMBAHAN', 'Alamat 2', 'textarea', null, 'Bekasi, Indonesia', null, 'kontak', '1', '1', '2017-12-25 09:36:58', '2017-12-25 09:36:58'), ('4', 'KONTAK_NOMOR_TELEPON', 'Nomor Telepon 1', 'text', null, '(021) 29257711', null, 'kontak', '1', '1', '2017-12-25 09:36:58', '2017-12-25 09:36:58'), ('5', 'KONTAK_NOMOR_TELEPON_TAMBAHAN', 'Nomor Telepon 2', 'text', null, '(021) 22100028 (Pokja ULP)', null, 'kontak', '1', '1', '2017-12-25 09:36:58', '2017-12-25 09:36:58'), ('6', 'KONTAK_NOMOR_HP', 'Nomor HP 1', 'text', null, '', null, 'kontak', '1', '1', '2017-12-25 09:36:58', '2017-12-25 09:36:58'), ('7', 'KONTAK_NOMOR_HP_TAMBAHAN', 'Nomor HP 2', 'text', null, '', null, 'kontak', '1', '1', '2017-12-25 09:36:58', '2017-12-25 09:36:58'), ('8', 'KONTAK_FAX', 'Fax', 'text', null, '(021) 29257712', null, 'kontak', '1', '1', '2017-12-25 09:36:58', '2017-12-25 09:36:58'), ('9', 'KONTAK_EMAIL', 'Email 1', 'text', null, 'lpse@bekasikota.go.id', null, 'kontak', '1', '1', '2017-12-25 09:36:58', '2017-12-25 09:36:58'), ('10', 'KONTAK_EMAIL_TAMBAHAN', 'Email 2', 'text', null, '', null, 'kontak', '1', '1', '2017-12-25 09:36:58', '2017-12-25 09:36:58'), ('11', 'KONTAK_LATITUDE', 'Latitude', 'text', null, '-6.235921', null, 'kontak', '1', '1', '2017-12-25 09:36:58', '2017-12-25 09:36:58'), ('12', 'KONTAK_LONGITUDE', 'Longitude', 'text', null, '106.995281', null, 'kontak', '1', '1', '2017-12-25 09:36:58', '2017-12-25 09:36:58'), ('13', 'WEBSITE_FOOTER', 'Footer', 'textarea', null, 'Copyright © Layanan Secara Elektronik (LPSE) Bekasi | Portal v1.0.0', null, 'website', '1', '1', '2017-12-25 09:36:58', '2017-12-25 09:36:58'), ('14', 'WEBSITE_NAMA', 'Nama', 'text', null, 'Portal LPSE Bekasi Kota', null, 'website', '1', '1', '2017-12-25 09:36:58', '2017-12-25 09:36:58'), ('15', 'WEBSITE_PESAN_PEMBUKA', 'Pesan Pembuka', 'textarea', null, 'It is a long established fact that It is a long established fact that It is a long established fact that It is a long established fact that', null, 'website', '1', '1', '2017-12-25 09:36:58', '2017-12-25 09:36:58'), ('16', 'WEBSITE_SCRIPT_GOOGLE_ANALYTIC', 'Script Google Analytic', 'textarea', null, '', null, 'website', '1', '1', '2017-12-25 09:36:58', '2017-12-25 09:36:58'), ('17', 'WEBSITE_CAPTCHA_TYPE', 'Captcha Type', 'dropdown', 'captcha_type', 'NONE', null, 'website', '1', '1', '2017-12-25 09:36:58', '2017-12-25 09:36:58'), ('18', 'EMAIL_NAMA_PENGIRIM', 'Nama Pengirim', 'text', null, 'Administrator Portal LPSE Bekasi', null, 'email', '1', '1', '2017-12-25 09:36:58', '2017-12-25 09:36:58'), ('19', 'EMAIL_ALAMAT_PENGIRIM', 'Alamat Email Pengirim', 'text', null, 'sramadhan95@gmail.com', null, 'email', '1', '1', '2017-12-25 09:36:58', '2017-12-25 09:36:58'), ('20', 'EMAIL_SMTP_MAIL_SERVER', 'SMTP Mail Server', 'text', null, 'ssl://smtp.gmail.com', null, 'email', '1', '1', '2017-12-25 09:36:58', '2017-12-25 09:36:58'), ('21', 'EMAIL_SMTP_USERNAME', 'SMTP Username', 'text', null, 'sramadhan95@gmail.com', null, 'email', '1', '1', '2017-12-25 09:36:58', '2017-12-25 09:36:58'), ('22', 'EMAIL_SMTP_PASSWORD', 'SMTP Password', 'text', null, '1234567890', null, 'email', '1', '1', '2017-12-25 09:36:58', '2017-12-25 09:36:58'), ('23', 'EMAIL_SMTP_PORT', 'SMTP Port', 'text', null, '465', null, 'email', '1', '1', '2017-12-25 09:36:58', '2017-12-25 09:36:58'), ('24', 'EMAIL_SMTP_ENCRYPTION', 'SMTP Encryption', 'dropdown', 'smtp_encryption', 'NONE', null, 'email', '1', '1', '2017-12-25 09:36:58', '2017-12-25 09:36:58'), ('25', 'WEBSITE_KODE_WARNA', 'Kode Warna', 'text', null, '#00aeef', null, 'website', '1', '1', '2017-12-25 09:36:58', '2017-12-25 09:36:58'), ('26', 'WEBISTE_VERSI', 'Versi', 'text', null, '1.0.0', null, 'website', '1', '1', '2017-12-25 09:36:58', '2017-12-25 09:36:58'), ('27', 'WEBSITE_DESKRIPSI', 'Deskripsi', 'textarea', null, 'It is a long established fact that', null, 'website', '1', '1', '2017-12-25 09:36:58', '2017-12-25 09:36:58'), ('28', 'WEBSITE_JADWAL_PELAYANAN', 'Jadwal Pelayanan Helpdesk', 'textarea', null, 'Senin - Jumat : 09.00 WIB - 15.00 WIB', null, 'website', '1', '1', '2018-04-25 21:04:25', '2018-04-25 21:04:28');
COMMIT;

-- ----------------------------
--  Table structure for `pengaturan_dropdown`
-- ----------------------------
DROP TABLE IF EXISTS `pengaturan_dropdown`;
CREATE TABLE `pengaturan_dropdown` (
  `id_pengaturan_dropdown` int(11) NOT NULL AUTO_INCREMENT,
  `grup_dropdown` varchar(100) NOT NULL,
  `value_dropdown` varchar(100) NOT NULL,
  `label_dropdown` varchar(200) NOT NULL,
  `flag` tinyint(1) NOT NULL,
  `dibuat_oleh` int(11) NOT NULL,
  `tanggal_buat` datetime NOT NULL,
  `tanggal_ubah` datetime DEFAULT NULL,
  PRIMARY KEY (`id_pengaturan_dropdown`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
--  Records of `pengaturan_dropdown`
-- ----------------------------
BEGIN;
INSERT INTO `pengaturan_dropdown` VALUES ('1', 'smtp_encryption', 'NONE', 'None', '1', '1', '2018-06-18 16:29:44', '2018-06-18 16:29:46'), ('2', 'smtp_encryption', 'TLS', 'TLS', '1', '1', '2018-06-18 16:30:10', '2018-06-18 16:30:13'), ('3', 'smtp_encryption', 'SSL', 'SSL', '1', '1', '2018-06-18 16:30:35', '2018-06-18 16:30:37'), ('4', 'captcha_type', 'NONE', 'None', '1', '1', '2018-06-18 16:31:25', '2018-06-18 16:31:28'), ('5', 'captcha_type', 'CAPTCHA', 'Captcha', '1', '1', '2018-06-18 16:31:53', '2018-06-18 16:31:55'), ('6', 'captcha_type', 'RECAPTCHA', 'Re-Captcha', '1', '1', '2018-06-18 16:32:16', '2018-06-18 16:32:18'), ('7', 'jabatan_pengguna', 'CEO', 'CEO', '1', '1', '2018-06-19 20:32:25', '2018-06-19 20:32:28'), ('8', 'jabatan_pengguna', 'OWNER', 'OWNER', '1', '1', '2018-06-19 20:33:05', '2018-06-19 20:33:09'), ('9', 'jabatan_pengguna', 'FOUNDER', 'FOUNDER', '1', '1', '2018-09-15 14:59:02', '2018-09-15 14:59:05'), ('10', 'jabatan_pengguna', 'CO_FOUNDER', 'CO-FOUNDER', '1', '1', '2018-09-15 14:59:33', '2018-09-15 14:59:36'), ('11', 'jabatan_pengguna', 'STAF', 'STAF', '1', '1', '2018-09-15 15:00:07', '2018-09-15 15:00:09');
COMMIT;

-- ----------------------------
--  Table structure for `pengguna`
-- ----------------------------
DROP TABLE IF EXISTS `pengguna`;
CREATE TABLE `pengguna` (
  `id_pengguna` int(11) NOT NULL AUTO_INCREMENT,
  `id_peran_pengguna` int(11) NOT NULL,
  `nama_pengguna` varchar(255) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `foto` varchar(255) NOT NULL,
  `email_pengguna` varchar(100) NOT NULL,
  `tanggal_lahir_pengguna` date NOT NULL,
  `tempat_lahir_pengguna` varchar(100) NOT NULL,
  `alamat_pengguna` text NOT NULL,
  `telepon_pengguna` varchar(15) NOT NULL,
  `jabatan` varchar(50) DEFAULT NULL,
  `lihat` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL,
  `flag` tinyint(1) NOT NULL DEFAULT '1',
  `dibuat_oleh` int(11) NOT NULL,
  `tanggal_buat` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `tanggal_ubah` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_pengguna`),
  UNIQUE KEY `idx_pengguna` (`id_pengguna`),
  KEY `peran_rinci_idx_peran_pengguna` (`id_peran_pengguna`),
  CONSTRAINT `peran_rinci_id_peran_penggua` FOREIGN KEY (`id_peran_pengguna`) REFERENCES `peran_pengguna` (`id_peran_pengguna`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
--  Records of `pengguna`
-- ----------------------------
BEGIN;
INSERT INTO `pengguna` VALUES ('1', '1', 'Administrator', 'administrator', '200ceb26807d6bf99fd6f4f0d1ca54d4', 'Ipin.png', 'administrator@example.com', '1980-10-10', 'Jakarta Selatan', 'Jakarta Selatan', '081222224444', 'OWNER', '0', '1', '1', '0', '2017-09-24 04:52:58', '2018-09-15 08:02:09'), ('2', '1', 'Syahrul Ramadhan', 'syahrulramadh4n', '25d55ad283aa400af464c76d713c07ad', 'sketch1478028195646.png', 'sramadhan95@gmail.com', '1991-04-13', 'Tangerang Selatan', 'Jl. Arimbi 2', '085777778888', 'CEO', '0', '1', '1', '0', '2017-09-26 16:16:11', '2018-09-15 08:02:22');
COMMIT;

-- ----------------------------
--  Table structure for `peran_grup`
-- ----------------------------
DROP TABLE IF EXISTS `peran_grup`;
CREATE TABLE `peran_grup` (
  `id_peran_grup` int(11) NOT NULL AUTO_INCREMENT,
  `nama_peran_grup` varchar(255) NOT NULL,
  `paten` varchar(50) NOT NULL,
  `id_peran_grup_menu` int(11) NOT NULL,
  `flag` tinyint(1) NOT NULL DEFAULT '1',
  `tanggal_buat` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `tanggal_ubah` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_peran_grup`),
  UNIQUE KEY `idx_peran_grup` (`id_peran_grup`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
--  Records of `peran_grup`
-- ----------------------------
BEGIN;
INSERT INTO `peran_grup` VALUES ('1', 'Dasbor', 'Dasbor', '1', '1', '2017-09-24 04:52:58', '2017-09-24 04:52:58'), ('2', 'Pengguna', 'Pengguna', '2', '1', '2017-09-24 04:52:58', '2017-09-24 04:52:58'), ('3', 'Peran Pengguna', 'PeranPengguna', '2', '1', '2017-09-24 04:52:58', '2017-09-24 04:52:58'), ('4', 'Pengaturan', 'Pengaturan', '2', '1', '2017-12-25 22:18:12', '2017-12-25 22:18:12'), ('5', 'Peran Pengguna Log', 'PeranPenggunaLog', '2', '1', '2018-03-17 17:47:55', '2018-03-17 17:47:58');
COMMIT;

-- ----------------------------
--  Table structure for `peran_grup_menu`
-- ----------------------------
DROP TABLE IF EXISTS `peran_grup_menu`;
CREATE TABLE `peran_grup_menu` (
  `id_peran_grup_menu` int(11) NOT NULL AUTO_INCREMENT,
  `nama_peran_grup_menu` varchar(255) NOT NULL,
  `flag` tinyint(1) NOT NULL DEFAULT '1',
  `tanggal_buat` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `tanggal_ubah` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_peran_grup_menu`),
  UNIQUE KEY `idx_peran_grup_menu` (`id_peran_grup_menu`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
--  Records of `peran_grup_menu`
-- ----------------------------
BEGIN;
INSERT INTO `peran_grup_menu` VALUES ('1', 'KONTEN', '1', '2017-10-02 20:21:16', '2017-10-02 20:21:16'), ('2', 'PENGATURAN', '1', '2017-10-02 20:21:23', '2017-10-02 20:21:23');
COMMIT;

-- ----------------------------
--  Table structure for `peran_pengguna`
-- ----------------------------
DROP TABLE IF EXISTS `peran_pengguna`;
CREATE TABLE `peran_pengguna` (
  `id_peran_pengguna` int(11) NOT NULL AUTO_INCREMENT,
  `nama_peran_pengguna` varchar(255) NOT NULL,
  `deskripsi` text NOT NULL,
  `flag` tinyint(1) NOT NULL DEFAULT '1',
  `tanggal_buat` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `tanggal_ubah` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dibuat_oleh` int(11) NOT NULL,
  PRIMARY KEY (`id_peran_pengguna`),
  UNIQUE KEY `idx_peran_pengguna` (`id_peran_pengguna`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
--  Records of `peran_pengguna`
-- ----------------------------
BEGIN;
INSERT INTO `peran_pengguna` VALUES ('1', 'Administrator', 'Peran yang dapat mengelola semua fitur yang ada pada sistem', '1', '2017-09-24 04:52:58', '2018-09-15 06:25:29', '1'), ('2', 'Administrator Content', 'Peran yang dapat mengelola berhubungan dengan fitur pos yang ada pada sistem', '1', '2017-09-26 16:18:47', '2018-09-15 06:25:58', '1');
COMMIT;

-- ----------------------------
--  Table structure for `peran_pengguna_log`
-- ----------------------------
DROP TABLE IF EXISTS `peran_pengguna_log`;
CREATE TABLE `peran_pengguna_log` (
  `id_peran_pengguna_log` int(11) NOT NULL AUTO_INCREMENT,
  `id_pengguna` int(11) NOT NULL,
  `id_peran_pengguna` int(11) NOT NULL,
  `label_peran_pengguna_log` varchar(100) DEFAULT NULL,
  `deskripsi` text,
  `flag` tinyint(1) NOT NULL DEFAULT '1',
  `tanggal_buat` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `tanggal_ubah` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `tanggal_mulai` date DEFAULT NULL,
  `tanggal_selesai` date DEFAULT NULL,
  `dibuat_oleh` int(11) NOT NULL,
  PRIMARY KEY (`id_peran_pengguna_log`),
  UNIQUE KEY `idx_peran_pengguna` (`id_peran_pengguna_log`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
--  Table structure for `peran_pengguna_rinci`
-- ----------------------------
DROP TABLE IF EXISTS `peran_pengguna_rinci`;
CREATE TABLE `peran_pengguna_rinci` (
  `id_peran_pengguna_rinci` int(11) NOT NULL AUTO_INCREMENT,
  `id_peran_pengguna` int(11) NOT NULL,
  `id_peran_rinci` int(11) NOT NULL,
  `flag` tinyint(1) NOT NULL DEFAULT '1',
  `tanggal_buat` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `tanggal_ubah` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dibuat_oleh` int(11) NOT NULL,
  PRIMARY KEY (`id_peran_pengguna_rinci`),
  UNIQUE KEY `idx_peran_pengguna_rinci` (`id_peran_pengguna_rinci`),
  KEY `peran_pengguna_rinci_idx_peran_pengguna` (`id_peran_pengguna`),
  KEY `iperan_pengguna_rinci_dx_peran_rinci` (`id_peran_rinci`),
  CONSTRAINT `peran_pengguna_rinci_id_peran_pengguna` FOREIGN KEY (`id_peran_pengguna`) REFERENCES `peran_pengguna` (`id_peran_pengguna`) ON UPDATE CASCADE,
  CONSTRAINT `peran_pengguna_rinci_id_peran_rinci` FOREIGN KEY (`id_peran_rinci`) REFERENCES `peran_rinci` (`id_peran_rinci`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1228 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
--  Records of `peran_pengguna_rinci`
-- ----------------------------
BEGIN;
INSERT INTO `peran_pengguna_rinci` VALUES ('1203', '1', '1', '1', '2018-09-15 06:25:29', '2018-09-15 06:25:29', '1'), ('1204', '1', '65', '1', '2018-09-15 06:25:29', '2018-09-15 06:25:29', '1'), ('1205', '1', '66', '1', '2018-09-15 06:25:29', '2018-09-15 06:25:29', '1'), ('1206', '1', '67', '1', '2018-09-15 06:25:29', '2018-09-15 06:25:29', '1'), ('1207', '1', '6', '1', '2018-09-15 06:25:29', '2018-09-15 06:25:29', '1'), ('1208', '1', '7', '1', '2018-09-15 06:25:29', '2018-09-15 06:25:29', '1'), ('1209', '1', '8', '1', '2018-09-15 06:25:29', '2018-09-15 06:25:29', '1'), ('1210', '1', '9', '1', '2018-09-15 06:25:29', '2018-09-15 06:25:29', '1'), ('1211', '1', '10', '1', '2018-09-15 06:25:29', '2018-09-15 06:25:29', '1'), ('1212', '1', '94', '1', '2018-09-15 06:25:29', '2018-09-15 06:25:29', '1'), ('1213', '1', '11', '1', '2018-09-15 06:25:29', '2018-09-15 06:25:29', '1'), ('1214', '1', '12', '1', '2018-09-15 06:25:29', '2018-09-15 06:25:29', '1'), ('1215', '1', '13', '1', '2018-09-15 06:25:29', '2018-09-15 06:25:29', '1'), ('1216', '1', '14', '1', '2018-09-15 06:25:29', '2018-09-15 06:25:29', '1'), ('1217', '1', '15', '1', '2018-09-15 06:25:29', '2018-09-15 06:25:29', '1'), ('1218', '1', '95', '1', '2018-09-15 06:25:29', '2018-09-15 06:25:29', '1'), ('1219', '1', '83', '1', '2018-09-15 06:25:29', '2018-09-15 06:25:29', '1'), ('1220', '1', '84', '1', '2018-09-15 06:25:29', '2018-09-15 06:25:29', '1'), ('1221', '1', '85', '1', '2018-09-15 06:25:29', '2018-09-15 06:25:29', '1'), ('1222', '1', '86', '1', '2018-09-15 06:25:29', '2018-09-15 06:25:29', '1'), ('1223', '1', '87', '1', '2018-09-15 06:25:29', '2018-09-15 06:25:29', '1'), ('1224', '1', '107', '1', '2018-09-15 06:25:29', '2018-09-15 06:25:29', '1'), ('1225', '2', '1', '1', '2018-09-15 06:25:58', '2018-09-15 06:25:58', '1');
COMMIT;

-- ----------------------------
--  Table structure for `peran_rinci`
-- ----------------------------
DROP TABLE IF EXISTS `peran_rinci`;
CREATE TABLE `peran_rinci` (
  `id_peran_rinci` int(11) NOT NULL AUTO_INCREMENT,
  `id_peran_grup` int(11) NOT NULL,
  `nama_peran_rinci` varchar(255) NOT NULL,
  `deskripsi` text NOT NULL,
  `flag` tinyint(1) NOT NULL DEFAULT '1',
  `tanggal_buat` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `tanggal_ubah` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_peran_rinci`),
  UNIQUE KEY `idx_peran_rinci` (`id_peran_rinci`),
  KEY `peran_rinci_idx_peran_grup` (`id_peran_grup`),
  CONSTRAINT `peran_rinci_id_peran_grup` FOREIGN KEY (`id_peran_grup`) REFERENCES `peran_grup` (`id_peran_grup`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=109 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
--  Records of `peran_rinci`
-- ----------------------------
BEGIN;
INSERT INTO `peran_rinci` VALUES ('1', '1', 'DasborMengakses', '-', '1', '2017-10-02 03:36:29', '2017-10-02 03:36:29'), ('6', '2', 'PenggunaMengakses', '-', '1', '2017-10-02 03:36:29', '2017-10-02 03:36:29'), ('7', '2', 'PenggunaTambah', '-', '1', '2017-10-02 03:36:29', '2017-10-02 03:36:29'), ('8', '2', 'PenggunaEdit', '-', '1', '2017-10-02 03:36:29', '2017-10-02 03:36:29'), ('9', '2', 'PenggunaMenghapus', '-', '1', '2017-10-02 03:36:29', '2017-10-02 03:36:29'), ('10', '2', 'PenggunaEkspor', '-', '1', '2017-10-02 03:36:29', '2017-10-02 03:36:29'), ('11', '3', 'PeranPenggunaMengakses', '-', '1', '2017-10-02 03:36:29', '2017-10-02 03:36:29'), ('12', '3', 'PeranPenggunaTambah', '-', '1', '2017-10-02 03:36:29', '2017-10-02 03:36:29'), ('13', '3', 'PeranPenggunaEdit', '-', '1', '2017-10-02 03:36:29', '2017-10-02 03:36:29'), ('14', '3', 'PeranPenggunaMenghapus', '-', '1', '2017-10-02 03:36:29', '2017-10-02 03:36:29'), ('15', '3', 'PeranPenggunaEkspor', '-', '1', '2017-10-02 03:36:29', '2017-10-02 03:36:29'), ('65', '4', 'PengaturanMengakses', '-', '1', '2017-12-25 22:19:28', '2017-12-25 22:19:28'), ('66', '4', 'PengaturanEdit', '-', '1', '2017-12-25 22:19:57', '2017-12-25 22:19:57'), ('67', '4', 'PengaturanEkspor', '-', '1', '2017-12-25 22:20:30', '2017-12-25 22:20:30'), ('83', '5', 'PeranPenggunaLogMengakses', '-', '1', '2018-03-17 17:48:21', '2018-03-17 17:48:24'), ('84', '5', 'PeranPenggunaLogTambah', '-', '1', '2018-03-17 17:49:33', '2018-03-17 17:49:36'), ('85', '5', 'PeranPenggunaLogEdit', '-', '1', '2018-03-17 17:49:54', '2018-03-17 17:49:58'), ('86', '5', 'PeranPenggunaLogMenghapus', '-', '1', '2018-03-17 17:50:13', '2018-03-17 17:50:18'), ('87', '5', 'PeranPenggunaLogEkspor', '-', '1', '2018-03-17 17:50:32', '2018-03-17 17:50:35'), ('94', '2', 'PenggunaFlag', '-', '1', '2018-03-17 17:50:32', '2018-03-17 17:50:35'), ('95', '3', 'PeranPenggunaFlag', '-', '1', '2018-03-17 17:50:32', '2018-03-17 17:50:35'), ('107', '5', 'PeranPenggunaLogFlag', '-', '1', '2018-03-17 17:50:32', '2018-03-17 17:50:35');
COMMIT;

-- ----------------------------
--  Table structure for `phinxlog`
-- ----------------------------
DROP TABLE IF EXISTS `phinxlog`;
CREATE TABLE `phinxlog` (
  `version` bigint(20) NOT NULL,
  `migration_name` varchar(100) DEFAULT NULL,
  `start_time` timestamp NULL DEFAULT NULL,
  `end_time` timestamp NULL DEFAULT NULL,
  `breakpoint` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
--  Records of `phinxlog`
-- ----------------------------
BEGIN;
INSERT INTO `phinxlog` VALUES ('20170924134753', 'CreateTablePeranPengguna', '2017-09-24 16:51:55', '2017-09-24 16:51:55', '0'), ('20170924134802', 'CreateTablePengguna', '2017-09-24 16:51:55', '2017-09-24 16:51:55', '0'), ('20170924134914', 'CreateTablePeranGrup', '2017-09-24 16:51:55', '2017-09-24 16:51:56', '0'), ('20170924134919', 'CreateTablePeranRinci', '2017-09-24 16:51:56', '2017-09-24 16:51:56', '0'), ('20170924134926', 'CreateTablePeranPenggunaRinci', '2017-09-24 16:51:56', '2017-09-24 16:51:56', '0');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
