<?php

use Phinx\Migration\AbstractMigration;

class CreateTablePengguna extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
		if(!$this->hasTable('pengguna')) {
            $table = $this->table('pengguna', array('id' => 'id_pengguna'));
			
			$table->addColumn('id_peran_pengguna', 'integer');
            $table->addColumn('nama_pengguna', 'string', array('limit' => 255));
            $table->addColumn('username', 'string', array('limit' => 100));
            $table->addColumn('password', 'string', array('limit' => 100));
            $table->addColumn('foto', 'string', array('limit' => 255));
            $table->addColumn('email_pengguna', 'string', array('limit' => 100));
            $table->addColumn('tanggal_lahir_pengguna', 'date');
            $table->addColumn('tempat_lahir_pengguna', 'string', array('limit' => 100));
            $table->addColumn('alamat_pengguna', 'text');
			$table->addColumn('telepon_pengguna', 'string', array('limit' => 15));
			$table->addColumn('status', 'integer', array('default' => 1, 'limit' => 1));
            $table->addColumn('tanggal_buat', 'timestamp', array('default' => 'CURRENT_TIMESTAMP'));
            $table->addColumn('tanggal_ubah', 'timestamp', array('default' => 'CURRENT_TIMESTAMP'));

			$table->addIndex(array('id_pengguna'), array('unique' => true, 'name' => 'idx_pengguna'));
			$table->addIndex(array('id_peran_pengguna'), array('name' => 'peran_rinci_idx_peran_pengguna'));

			$table->addForeignKey(array('id_peran_pengguna'),
					'peran_pengguna',
					array('id_peran_pengguna'),
					array('delete'=> 'RESTRICT', 'update'=> 'CASCADE', 'constraint' => 'peran_rinci_id_peran_penggua'));

			$table->create();
        }
    }
}
