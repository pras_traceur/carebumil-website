<?php

use Phinx\Migration\AbstractMigration;

class CreateTablePeranRinci extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
		if(!$this->hasTable('peran_rinci')) {
            $table = $this->table('peran_rinci', array('id' => 'id_peran_rinci'));

			$table->addColumn('id_peran_grup', 'integer');
            $table->addColumn('nama_peran_rinci', 'string', array('limit' => 255));
            $table->addColumn('deskripsi', 'text');
			$table->addColumn('status', 'integer', array('default' => 1, 'limit' => 1));
            $table->addColumn('tanggal_buat', 'timestamp', array('default' => 'CURRENT_TIMESTAMP'));
            $table->addColumn('tanggal_ubah', 'timestamp', array('default' => 'CURRENT_TIMESTAMP'));

			$table->addIndex(array('id_peran_rinci'), array('unique' => true, 'name' => 'idx_peran_rinci'));
			$table->addIndex(array('id_peran_grup'), array('name' => 'peran_rinci_idx_peran_grup'));

			$table->addForeignKey(array('id_peran_grup'),
					'peran_grup',
					array('id_peran_grup'),
					array('delete'=> 'RESTRICT', 'update'=> 'CASCADE', 'constraint' => 'peran_rinci_id_peran_grup'));

			$table->create();
        }
    }
}
