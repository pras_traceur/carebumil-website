<?php

use Phinx\Migration\AbstractMigration;

class CreateTablePeranPenggunaRinci extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
		if(!$this->hasTable('peran_pengguna_rinci')) {
            $table = $this->table('peran_pengguna_rinci', array('id' => 'id_peran_pengguna_rinci'));

			$table->addColumn('id_peran_pengguna', 'integer');
			$table->addColumn('id_peran_rinci', 'integer');
			$table->addColumn('status', 'integer', array('default' => 1, 'limit' => 1));
            $table->addColumn('tanggal_buat', 'timestamp', array('default' => 'CURRENT_TIMESTAMP'));
            $table->addColumn('tanggal_ubah', 'timestamp', array('default' => 'CURRENT_TIMESTAMP'));
			 
			$table->addIndex(array('id_peran_pengguna_rinci'), array('unique' => true, 'name' => 'idx_peran_pengguna_rinci'));
			$table->addIndex(array('id_peran_pengguna'), array('name' => 'peran_pengguna_rinci_idx_peran_pengguna'));
			$table->addIndex(array('id_peran_rinci'), array('name' => 'iperan_pengguna_rinci_dx_peran_rinci'));
            
			$table->addForeignKey(array('id_peran_pengguna'),
					'peran_pengguna',
					array('id_peran_pengguna'),
					array('delete'=> 'RESTRICT', 'update'=> 'CASCADE', 'constraint' => 'peran_pengguna_rinci_id_peran_pengguna'));
			
			$table->addForeignKey(array('id_peran_rinci'),
					'peran_rinci',
					array('id_peran_rinci'),
					array('delete'=> 'RESTRICT', 'update'=> 'CASCADE', 'constraint' => 'peran_pengguna_rinci_id_peran_rinci'));
			
			$table->create();
        }
    }
}
