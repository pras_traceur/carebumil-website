<?php

use Phinx\Migration\AbstractMigration;

class CreateTablePeranGrup extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
		if(!$this->hasTable('peran_grup')) {
            $table = $this->table('peran_grup', array('id' => 'id_peran_grup'));

            $table->addColumn('nama_peran_grup', 'string', array('limit' => 255));
            $table->addColumn('paten', 'string', array('limit' => 50));
            $table->addColumn('flag', 'string', array('null' => true, 'limit' => 20, 'after' => 'paten'));
			$table->addColumn('status', 'char', array('default' => 1, 'limit' => 1));
            $table->addColumn('tanggal_buat', 'timestamp', array('default' => 'CURRENT_TIMESTAMP'));
            $table->addColumn('tanggal_ubah', 'timestamp', array('default' => 'CURRENT_TIMESTAMP'));

			$table->addIndex(array('id_peran_grup'), array('unique' => true, 'name' => 'idx_peran_grup'));

			$table->create();
        }
    }
}
