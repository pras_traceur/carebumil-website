<?php
class Mpengguna extends CI_Model {
	var $label = 'Pengguna';
	var $table = 'pengguna';
    var $column = array('id_pengguna','nama_pengguna','username','email_pengguna','flag');
    var $options = array('id_pengguna','nama_pengguna');
	
    function __construct()
    {
        parent::__construct();
    }
	
	public function get_select_options()
    {
        $this->db->from($this->table);
        $this->db->select(implode(',', $this->options));

        $query = $this->db->get();

		if($query->num_rows() > 0) {
			$rows = $query->result_array();
			
			$result = array('' => sprintf('-- Pilih%1$s --', ' ' .$this->label));

			foreach ($rows as $row) {
				$result[$row[$this->options[0]]] = $row[$this->options[1]];
			}

			return $result;
		}else{
			return false;
		}
	}
	
	public function paginate($page, $limit, $keyword = null)
    {	
		$offset = ($page - 1) * $limit;

        $this->db->select('pengguna.id_pengguna, pengguna.nama_pengguna, pengguna.foto, pengguna.email_pengguna, pengguna.jabatan, pengguna.tanggal_buat, pengguna.tanggal_ubah');
        $this->paginate_queries($keyword);
        $this->db->order_by('pengguna.tanggal_buat', 'desc');
		$this->db->limit($limit, $offset);

        $query = $this->db->get();

        return $query->result_array();
    }

    public function paginate_count($keyword = null){
        $this->db->select('COUNT(*) as total_rows');

        $this->paginate_queries($keyword);
        
        $query = $this->db->get();

        return $query->row_array();
    }

    public function paginate_queries($keyword){
        if ($keyword) {
            $this->db->like('pengguna.nama_pengguna', $keyword);
        }

        $this->db->from($this->table);
		$this->db->where(array('pengguna.flag' => 1, 'pengguna.status' => 1));
    }
    
    public function getEmailPassword($email,$password){
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->where('status', 1);
        $this->db->where('flag', 1);
        $this->db->where('email_pengguna', $email);
        $this->db->where('password', $password);

		$query = $this->db->get();
		
        if($query->num_rows() > 0) {
			return $query->row_array();
        } return false;
    }

}