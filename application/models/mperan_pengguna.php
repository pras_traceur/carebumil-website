<?php
class Mperan_pengguna extends CI_Model {
	var $label = 'Peran Pengguna';
	var $table = 'peran_pengguna';
	var $tableoption1 = 'peran_pengguna_rinci';
    var $column = array('id_nama_pengguna','nama_peran_pengguna','flag','keterangan');
    var $options = array('id_peran_pengguna','nama_peran_pengguna');
	
    function __construct()
    {
        parent::__construct();
    }
	
	public function get_select_options()
    {
        $this->db->from($this->table);
        $this->db->select(implode(',', $this->options));

        $query = $this->db->get();

		if($query->num_rows() > 0) {
			$rows = $query->result_array();
			
			$result = array('' => sprintf('-- Pilih%1$s --', ' ' .$this->label));

			foreach ($rows as $row) {
				$result[$row[$this->options[0]]] = $row[$this->options[1]];
			}

			return $result;
		}else{
			return false;
		}
    }
	
	public function insert(){
		$this->db->trans_begin();

		$data = array(
			'nama_peran_pengguna' => $this->input->post('nama_peran_pengguna'),
			'deskripsi' => $this->input->post('deskripsi'),
			'dibuat_oleh' => $this->session->userdata('id_pengguna'),
			'tanggal_buat' => date('Y-m-d H:i:s'),
			'tanggal_ubah' => date('Y-m-d H:i:s')
		);
		
		if(permission('PeranPenggunaFlag'))
			$data['flag'] = $this->input->post('flag');

		$this->db->insert($this->table, $data);
		$id = $this->db->insert_id();
		
		$id_peran_rinci = $this->input->post('id_peran_rinci');

		foreach ($id_peran_rinci as $row) {
			$rows = array(
				'id_peran_rinci' => $row
				,'id_peran_pengguna' => $id
				,'flag' => 1
				,'dibuat_oleh' => $this->session->userdata('id_pengguna')
				,'tanggal_buat' => date('Y-m-d H:i:s')
				,'tanggal_ubah' => date('Y-m-d H:i:s')
			);
			
			$data1[] = $rows;
		}
		
		$this->db->insert_batch($this->tableoption1, $data1);
		
		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
		}
		else{
			$this->db->trans_commit();
		}
	}
	
	public function update($id){
		$data = array(
			'nama_peran_pengguna' => $this->input->post('nama_peran_pengguna'),
			'deskripsi' => $this->input->post('deskripsi'),
			'dibuat_oleh' => $this->session->userdata('id_pengguna')
		);

		if(permission('PeranPenggunaFlag'))
			$data['flag'] = $this->input->post('flag');
			
		$this->Mcommon->update($this->table, $data, array('id_peran_pengguna' => $id));
		
		$id_peran_rinci = $this->input->post('id_peran_rinci');

		foreach ($id_peran_rinci as $row) {
			$rows = array(
				'id_peran_rinci' => $row
				,'id_peran_pengguna' => $id
				,'flag' => 1
				,'dibuat_oleh' => $this->session->userdata('id_pengguna')
				,'tanggal_buat' => date('Y-m-d H:i:s')
				,'tanggal_ubah' => date('Y-m-d H:i:s')
			);
			
			$data1[] = $rows;
		}
		
		$this->Mcommon->delete($this->tableoption1, array('id_peran_pengguna' => $id));
		$this->Mcommon->insert_batch_array($this->tableoption1, $data1);
	}
}