<?php
class Mcommon extends CI_Model {
    function __construct()
    {
        parent::__construct();
    }
	
	function list_array($select, $table, $where = "", $orderby = "", $limit = ""){
		$this->db->select($select);
		$this->db->from($table);
		$this->db->where('flag', 1);
		
		if($where)
			$this->db->where($where);
		
		if($orderby)
			$this->db->order_by($orderby);

		if($limit)
			$this->db->limit($limit);

		$query = $this->db->get();

        if($query->num_rows() > 0) {
			return $query->result_array();
        } return false;
	}
	
	function list_in_array($id, $table, $where){
		$this->db->select($id);
		$this->db->from($table);
		$this->db->where('flag', 1);
		$this->db->where($where);

		$query = $this->db->get();
		
        $result = array();
		
        foreach ($query->result_array() as $row) {
            $result[] = $row[$id];
        }
		
        return $result;
	}
	
	function update($table, $data, $where){
		//$data['dibuat_oleh'] = $this->session->userdata('id_pengguna');
		$data['tanggal_ubah'] = date('Y-m-d H:i:s');
		
    	$this->db->where($where);
    	$this->db->update($table, $data);
    }

	function update_default($table, $data, $where){
    	$this->db->where($where);
    	$this->db->update($table, $data);
	}

	function insert_single($table, $data){
		$this->db->insert($table, $data);
		return $this->db->insert_id();
	}

	function update_single($table, $data, $where){
    	$this->db->where($where);
    	return $this->db->update($table, $data);
	}
	
    function insert($table, $data, $fieldescapes=false){
		$data['dibuat_oleh'] = $this->session->userdata('id_pengguna');
		$data['tanggal_buat'] = date('Y-m-d H:i:s');
		$data['tanggal_ubah'] = date('Y-m-d H:i:s');

		if($fieldescapes){
			if(is_array($fieldescapes)) {
				foreach ($fieldescapes as $fieldescape) {
					foreach ($data as $field => $valuefield) {
						($field == $fieldescape) ? $this->db->set($field, $valuefield, FALSE) : $this->db->set($field, $valuefield);
					}
				}
			}else {
				foreach ($data as $field => $valuefield) {
					($field == $fieldescapes) ? $this->db->set($field, $valuefield, FALSE) : $this->db->set($field, $valuefield);
				}
			}
			$this->db->insert($table);
		}

		else $this->db->insert($table, $data);
		
		return $this->db->insert_id();
    }
	
	function insert_batch_array($table, $data){
		$this->db->insert_batch($table, $data); 
	}

	function find($select, $table, $where){
		$this->db->select($select);
		$this->db->from($table);
		$this->db->where($where);

		$query = $this->db->get();
		
        if($query->num_rows() > 0) {
			return $query->row_array();
        } return false;
	}

	public function get_select_options($table, $options, $label, $where=false, $orderby=false)
	{
		$this->db->from($table);
		$this->db->where('flag', 1);
		
		if($where){
			$this->db->where($where);
		}

		if($orderby){
			$this->db->order_by($orderby);
		}

		$this->db->select(implode(',', $options));

		$query = $this->db->get();

		$result = array('' => sprintf('-- Pilih%1$s --', ' ' .$label));

		if($query->num_rows() > 0) {
			$rows = $query->result_array();

			foreach ($rows as $row) {
				$result[$row[$options[0]]] = $row[$options[1]];
			}
		}

		return $result;
	}
	
	public function delete($table, $where){
		$this->db->delete($table, $where); 
	}

	function last_find($select, $table, $where){
		$this->db->select($select);
		$this->db->from($table);
		$this->db->where($where);
		$this->db->order_by('tanggal desc');

		$query = $this->db->get();
		
        if($query->num_rows() > 0) {
			return $query->row_array();
        } return false;
	}

	function result_bidan(){
		$sql = "SELECT p.id_pengguna, p.rekam_medik, p.nama_pengguna, (SELECT t.tanggal from test t where t.id_pengguna = p.id_pengguna order by tanggal desc limit 1) as tanggal, (SELECT t.score from test t where t.id_pengguna = p.id_pengguna order by tanggal desc limit 1) as score, (SELECT t.hasil from test t where t.id_pengguna = p.id_pengguna order by tanggal desc limit 1) as hasil from pengguna p where p.id_peran_pengguna = 3 and p.flag = 1 order by p.nama_pengguna asc";

        $query = $this->db->query($sql);
		return $query->result_array();
	}

}