<?php
class Mlogin extends CI_Model {
	function __construct()
	{
		parent::__construct();
	}
	
	public function peran($username = false, $password = false)
    {
		if($username)
			$this->db->where('username', $username);
		
		if($password)
			$this->db->where('password', $password);

        $this->db->where('flag', 1);
        
        $query = $this->db->get('pengguna');

        return $query->row_array();
    }
	
	public function auth($id_peran_pengguna)
    {
        $arr = array();
        $this->db->select('peran_rinci.nama_peran_rinci');
		$this->db->join('peran_rinci', 'peran_rinci.id_peran_rinci = peran_pengguna_rinci.id_peran_rinci', 'left');
        $this->db->where('peran_pengguna_rinci.flag', 1);
        $this->db->where('peran_pengguna_rinci.id_peran_pengguna', $id_peran_pengguna);
        $query = $this->db->get('peran_pengguna_rinci');
        $query = $query->result_array();
        
		foreach ($query as $row) {
            $arr[] = $row['nama_peran_rinci'];
        }
		
        return $arr;
    }
	
	public function peran_pengguna($id_peran_pengguna)
    {
        $this->db->where('id_peran_pengguna', $id_peran_pengguna);
        $this->db->where('flag', 1);
        $query = $this->db->get('peran_pengguna');

        return $query->row_array();
    }

    public function getUserInfoByEmail($email){  
        $q = $this->db->get_where('pengguna', array('email_pengguna' => $email), 1);  
        if($this->db->affected_rows() > 0){  
            $row = $q->row();  
            return $row;  
        }  
    }

   public function insertToken($id_pengguna)  
   {    
     $token = substr(sha1(rand()), 0, 30);   
     $date = date('Y-m-d');  
       
     $string = array(  
         'token'=> $token,  
         'id_pengguna'=>$id_pengguna,  
         'created'=>$date  
       );  
     $query = $this->db->insert_string('tokens',$string);  
     $this->db->query($query);  
     return $token;  
   }

}