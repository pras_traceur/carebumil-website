<?php
class Mpengaturan extends CI_Model {
	function __construct()
	{
		parent::__construct();
    }

    function jumlah_field($list_grup = array()){
        if($list_grup){
            $sql = "SELECT ";

            foreach($list_grup as $row){
                if($row['grup'])
                    $select[] = '(SELECT count(*) FROM pengaturan WHERE grup ="' . $row['grup'] . '" AND flag = 1) as ' . $row['grup'];
            }

            $sql = $sql . implode(",", $select); 

            $query = $this->db->query($sql);

            if($query->num_rows() > 0) {
                return $query->row_array();
            } return false;
        } return false;
    }

    function list_grup(){
        $sql = "SELECT grup FROM pengaturan WHERE flag = 1 GROUP BY grup ORDER BY grup ASC";

        $query = $this->db->query($sql);

        if($query->num_rows() > 0) {
			return $query->result_array();
        } return false;
    }
}