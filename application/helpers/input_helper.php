<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if (!function_exists('request')) {
    function request($key, $default = null){
        return (isset($_GET[$key]) ? $_GET[$key] : (isset($_POST[$key]) ? $_POST[$key] : $default));
    }
}

if(!function_exists('input_clear')){
	function input_clear($data) {
	  $data = trim($data);
	  $data = stripslashes($data);
	  $data = htmlspecialchars($data);
	  return $data;
	}
}

if(!function_exists('active_menu')){
	function active_menu($param){
		$CI =& get_instance();
		
		$arr1 = explode(',', $param);

		if($arr1){
			return in_array($CI->uri->segment(2), $arr1) ;
		}
		
		return false;
	}
}

if(!function_exists('permission')){
	function permission($param){
		$CI =& get_instance();
		
		if($CI->session->userdata('username_logged_in') && $CI->session->userdata('password_logged_in')){
			$arr1 = explode(',', $param);
			
			if($arr1){
				$arr2 = json_decode($CI->session->userdata('id_peran_rinci'));

				$result = array_intersect($arr1, $arr2);
				
				return ($result) ? true : false;
			}
			
			return false;
		}
		
		redirect(URLBACK . '/login');
	}
}

if ( ! function_exists('flagOptions')){
	function flagOptions($q = ''){
		if($q == 'label'){
			$result = array(
				'' => ''
				,1 => '<span class="label label-success">Aktif</span>'
				,0 => '<span class="label label-danger">Tidak Aktif</span>'
			);
		}else{
			$result = array(
				'' => '-- Pilih Status --'
				,1 => 'Aktif'
				,0 => 'Tidak Aktif'
			);
		}

		return $result;
	}
}

if ( ! function_exists('statusSetujuOptions')){
	function statusSetujuOptions($q = ''){
		if($q == 'label'){
			$result = array(
				'' => ''
				,1 => '<span class="label label-success">Setuju</span>'
				,2 => '<span class="label label-danger">Tidak Setuju</span>'
				,3 => '<span class="label label-default">Menunggu</span>'
			);
		}else{
			$result = array(
				'' => '-- Pilih Status --'
				,1 => 'Setuju'
				,2 => 'Tidak Setuju'
				,3 => 'Menunggu'
			);
		}

		return $result;
	}
}

if ( ! function_exists('diBaca')){
	function diBaca(){
		$result = array(
			1 => '<i class="glyphicon glyphicon-eye-open"></i>'
			,0 => '<i class="glyphicon glyphicon-eye-close"></i>'
		);

		return $result;
	}
}

if ( ! function_exists('statusOptions')){
	function statusOptions($q = ''){
		if($q == 'label'){
			$result = array(
				'' => ''
				,1 => '<span class="label label-success">PUBLISH</span>'
				,0 => '<span class="label label-default">DRAFT</span>'
				,2 => '<span class="label label-warning">UNPUBLISH</span>'
			);
		}else{
			$result = array(
				'' => '-- Pilih Status --'
				,1 => 'PUBLISH'
				,0 => 'DRAFT'
				,2 => "UNPUBLISH"
			);
		}

		return $result;
	}
}

if ( ! function_exists('statusAplikasiOptions')){
	function statusAplikasiOptions($q = ''){
		if($q == 'label'){
			$result = array(
				'' => ''
				,1 => '<span class="label label-success">CHECK</span>'
				,0 => '<span class="label label-default">UNCHECK</span>'
			);
		}else{
			$result = array(
				'' => '-- Pilih Status --'
				,1 => 'CHECK'
				,0 => 'UNCHECK'
			);
		}

		return $result;
	}
}

if ( ! function_exists('hariOptions')){
	function hariOptions(){
		$result = array(
			'' => '-- Pilih Hari --'
			,0 => 'Minggu'
			,1 => 'Senin'
			,2 => 'Selasa'
			,3 => 'Rabu'
			,4 => 'Kamis'
			,5 => 'Jumat'
			,6 => 'Sabtu'
		);

		return $result;
	}
}

if ( ! function_exists('jenisKelaminOptions')){
	function jenisKelaminOptions(){
		$result = array(
			'' => '-- Pilih Jenis Kelamin --'
		,1 => 'Laki - Laki'
		,2 => 'Perempuan'
		);

		return $result;
	}
}


if ( ! function_exists('hubunganOptions')){
	function hubunganOptions(){
		$result = array(
			'' => '-- Pilih Hubungan --'
		,1 => 'Suami'
		,2 => 'Istri'
		,3 => 'Ayah/Ibu'
		,4 => 'Anak'
		,5 => 'Kakak/Adik'
		,6 => 'Lainnya'
		);

		return $result;
	}
}

if ( ! function_exists('pendidikanOptions')){
	function pendidikanOptions(){
		$result = array(
			'' => '-- Pilih Pendidikan --'
		,1 => 'Dibawah SMA/SMU/SMK'
		,2 => 'SMA/SMU/SMK'
		,3 => 'D3'
		,4 => 'S1'
		,5 => 'S2'
		,6 => 'S3'
		);

		return $result;
	}
}

if ( ! function_exists('jenisIDOptions')){
	function jenisIDOptions(){
		$result = array(
			'' => '-- Pilih Jenis ID --'
		,'KTP' => 'KTP'
		,'SIM' => 'SIM'
		,'Passport' => 'Passport'
		);

		return $result;
	}
}

if ( ! function_exists('statusPernikahanOptions')){
	function statusPernikahanOptions(){
		$result = array(
			'' => '-- Pilih Status Pernikahan --'
			,1 => 'Belum Menikah'
			,2 => 'Menikah'
		);
		
		return $result;
	}
}

if ( ! function_exists('agamaOptions')){
	function agamaOptions(){
		$result = array(
			'' => '-- Pilih Agama --'
		,1 => 'Budha'
		,2 => 'Hindu'
		,3 => 'Islam'
		,4 => 'Kristen Katolik'
		,5 => 'Kristen Protestan'
		,6 => 'Lainnya'
		);

		return $result;
	}
}

if ( ! function_exists('tglSekarang')){
	function tglSekarang(){
		$result = date("Ymd");

		return $result;
	}
}

if ( ! function_exists('dateOutput')){
	function dateOutput($dateInput){
		$dateOutput = NULL;

		if($dateInput){
			$dateInput = explode('/', $dateInput);
			$dateOutput = $dateInput[2] . '-' . $dateInput[1] . '-' . $dateInput[0];
		}

		return $dateOutput;
	}
}

if ( ! function_exists('dateInput')){
	function dateInput($dateOutput){
		$dateInput = NULL;

		if($dateOutput){
			$dateOutput = explode('-', $dateOutput);
			$dateInput = $dateOutput[2] . '/' . $dateOutput[1] . '/' . $dateOutput[0];
		}

		return $dateInput;
	}
}

if ( ! function_exists('word_limit')){
	function word_limit($content, $sum = 5) {
		if (count(explode(' ', $content)) > $sum) {
			$content = implode(' ', array_slice(explode(' ', strip_tags($content)), 0, $sum)) . " ...";
		} 
		
		return $content;
	}
}

if ( ! function_exists('tanggalid')){
	function tanggalid($tgl){
		if(is_date($tgl)){
			$ubah = gmdate($tgl, time()+60*60*8);
			$pecah = explode("-",$ubah);
			$tanggal = $pecah[2];
			$bulan = bulan($pecah[1]);
			$tahun = $pecah[0];

			return $tanggal.' '.$bulan.' '.$tahun;
		}else{
			return "-";
		}
	}
}

if ( ! function_exists('is_date')){
	function is_date($str){
		$stamp = strtotime( $str );
		if (!is_numeric($stamp)){
			return FALSE;
		}

		$month = date( 'm', $stamp );
		$day   = date( 'd', $stamp );
		$year  = date( 'Y', $stamp );

		if (checkdate($month, $day, $year)){
			return TRUE;
		}
		return FALSE;
	}
} 


if ( ! function_exists('datetimeconverttanggalid')){
	function datetimeconverttanggalid($tanggal){
		$d = date('d-m-Y-G-i', strtotime($tanggal));

		$pecah = explode("-",$d);
		$tanggal = $pecah[0];
		$bulan = bulan($pecah[1]);
		$tahun = $pecah[2];
		$jam = $pecah[3];
		$menit = $pecah[4];

		return $tanggal . ' ' . $bulan . ' ' . $tahun . ', ' . $jam . ':' . $menit . " WIB";
	}
}

if ( ! function_exists('bulan')){
	function bulan($bln){
		switch ($bln){
			case 1:
				return "Januari";
				break;
			case 2:
				return "Februari";
				break;
			case 3:
				return "Maret";
				break;
			case 4:
				return "April";
				break;
			case 5:
				return "Mei";
				break;
			case 6:
				return "Juni";
				break;
			case 7:
				return "Juli";
				break;
			case 8:
				return "Agustus";
				break;
			case 9:
				return "September";
				break;
			case 10:
				return "Oktober";
				break;
			case 11:
				return "November";
				break;
			case 12:
				return "Desember";
				break;
		}
	}
}

if ( ! function_exists('hariid')){
	function hariid($tanggal){
		$ubah = gmdate($tanggal, time()+60*60*8);
		$pecah = explode("-",$ubah);
		$tgl = $pecah[2];
		$bln = $pecah[1];
		$thn = $pecah[0];

		$nama = date("l", mktime(0,0,0,$bln,$tgl,$thn));
		$nama_hari = "";
		if($nama=="Sunday") {$nama_hari="Minggu";}
		else if($nama=="Monday") {$nama_hari="Senin";}
		else if($nama=="Tuesday") {$nama_hari="Selasa";}
		else if($nama=="Wednesday") {$nama_hari="Rabu";}
		else if($nama=="Thursday") {$nama_hari="Kamis";}
		else if($nama=="Friday") {$nama_hari="Jumat";}
		else if($nama=="Saturday") {$nama_hari="Sabtu";}
		
		return $nama_hari;
	}
}

if ( ! function_exists('hitung_mundur')){
	function hitung_mundur($wkt){
		$waktu=array(	
			365*24*60*60	=> "tahun",
			30*24*60*60	=> "bulan",
			7*24*60*60		=> "minggu",
			24*60*60			=> "hari",
			60*60				=> "jam",
			60					=> "menit",
			1						=> "detik"
		);

		$hitung = (strtotime(gmdate ("Y-m-d H:i:s", time () + 60 * 60 * 8)) - 3600) - $wkt;

		$hasil = array();
		
		if($hitung<5){
			$hasil = 'kurang dari 5 detik yang lalu';
		}else{
			$stop = 0;
			foreach($waktu as $periode => $satuan){
				if($stop >= 6 || ($stop > 0 && $periode < 60)) {
					break;
				}
				
				$bagi = floor($hitung/$periode);
				
				if($bagi > 0){
					$hasil[] = $bagi . ' ' . $satuan;
					$hitung -= $bagi * $periode;
					$stop++;
				} else if($stop > 0) {
					$stop++;
				}
			}
			
			//$hasil = implode(' ', $hasil) . ' yang lalu';
			$hasil = $hasil[0] . ' yang lalu';
		}
		
		return $hasil;
	}
}

if ( ! function_exists('calculate_file_size')){
	function calculate_file_size($size)
		{
		$sizes = ['B', 'KB', 'MB', 'GB'];
		$count=0;
		if ($size < 1024) {
			return $size . " " . $sizes[$count];
			} else{
			while ($size>1024){
				$size=round($size/1024,2);
				$count++;
			}
			return $size . " " . $sizes[$count];
		}
	}
}

if ( ! function_exists('date_published')){ 
	function date_published($date = "") {
		$dt = ($date) ? $date : '2014-08-30 08:00:00';

		$datetime = new DateTime($dt);
		
		$result = $datetime->format('Y-m-d\TH:i:sP');
		
		return $result;
	}
}

if ( ! function_exists('date_modified')){ 
	function date_modified($updated_at = "", $created_at = "") {
		$result = ($updated_at != '0000-00-00 00:00:00') ? date_published($updated_at) : date_published($created_at);
		
		return $result;
	}
}

if ( ! function_exists('get_konten')){
	function get_konten($id = 0, $konten = ""){
		$CI =& get_instance();

		$CI->db->select('id_konten_statis, judul, lihat, deskripsi, tanggal_buat, tanggal_ubah');
		$CI->db->from('konten_statis');
		$CI->db->where(array('id_konten_statis' => $id));

		$query = $CI->db->get();
		
		if($query->num_rows() > 0) {
			$data = array();

			if($id && $konten){
				$result = $query->row_array();
				
				if($id != 0){
					$CI->db->where(array('id_konten_statis' => $id));
					$CI->db->update('konten_statis', array('lihat' => $result['lihat'] + 1));
				}

				if($result){
					$data = array(
						"konten_" . $konten . "_id" => $result['id_konten_statis'],
						"konten_" . $konten . "_judul" => $result['judul'],
						"konten_" . $konten . "_url_title" => url_title($result['judul'], '-', TRUE),
						"konten_" . $konten . "_deskripsi" => $result['deskripsi']
					);
				}
			}
	
			return $data;
		} return false;
	}
}

if ( ! function_exists('alert')){
	function alert($class, $message){
		$d = array(
			'success' => "Success!",
			'info' => "Info!",
			'warning' => "Warning!",
			'danger' => "Danger!"
		);

		$alert = "<div class='alert alert-$class'><strong>$d[$class]</strong> $message</div>";

		return $alert;
	}
}

if ( ! function_exists('konten_statis')){
	function konten_statis() {
		$CI =& get_instance();
		
		$query = $CI->db->select('ks.id_konten_statis, ks.judul, ks.kata_kunci, ks.foto, ks.deskripsi, ks.dibuat_oleh, ks.lihat, ks.flag, ks.tanggal_buat, ks.tanggal_ubah, ks.status')
				->from('konten_statis ks')
				->where(array('ks.status' => 1, 'ks.hapus !=' => 1))
				->get();      

		if ($query->num_rows() > 0) {
			$i = 0;

            foreach ($query->result_array() as $row)
            {
                $id_konten_statis = !isset($row['id_konten_statis']) ? "" : $row['id_konten_statis'];
                $judul = !isset($row['judul']) ? "" : $row['judul'];
        
                $data[$i] = array(
                    "konten_statis_id" => $id_konten_statis,
                    "konten_statis_judul" => $judul,
                    "kegiatan_statis_judul_url" => '<a href="' . base_url('konten-statis/read/' . $id_konten_statis) . '/' . url_title($judul, '-', TRUE) . '">' . word_limit($judul, 5) . '</a>',
                );
                
                $i++;
			}

			return $data;
		}

		return false;
	}
}

if ( ! function_exists('pengaturan')){
	function pengaturan() {
		$CI =& get_instance();
		
		$query = $CI->db->select('id_pengaturan, field, label, deskripsi, dibuat_oleh, tipe, foto, grup, tanggal_buat, tanggal_ubah')
				->from('pengaturan ks')
				->get();      

		if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row)
            {
                $data[$row['field']] = $row['deskripsi'];
			}

			return (object) $data;
		}

		return false;
	}
}

if ( ! function_exists('unggah')){
	function unggah($config) {
		$CI =& get_instance();

		$CI->load->library('upload', $config);

		if (! $CI->upload->do_upload()) {
			return false;
		}else{
			$upload = $CI->upload->data();
			return $upload['file_name'];
		}
	}
}
