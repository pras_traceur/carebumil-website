<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Fusion - Bootstrap 4 Template</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?php echo base_url('assets/frontpage/'); ?>/css/bootstrap.min.css" >
    <!-- Icon -->
    <link rel="stylesheet" href="<?php echo base_url('assets/frontpage/'); ?>/fonts/line-icons.css">
    <!-- Owl carousel -->
    <link rel="stylesheet" href="<?php echo base_url('assets/frontpage/'); ?>/css/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/frontpage/'); ?>/css/owl.theme.css">
    
    <link rel="stylesheet" href="<?php echo base_url('assets/frontpage/'); ?>/css/magnific-popup.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/frontpage/'); ?>/css/nivo-lightbox.css">
    <!-- Animate -->
    <link rel="stylesheet" href="<?php echo base_url('assets/frontpage/'); ?>/css/animate.css">
    <!-- Main Style -->
    <link rel="stylesheet" href="<?php echo base_url('assets/frontpage/'); ?>/css/main.css">
    <!-- Responsive Style -->
    <link rel="stylesheet" href="<?php echo base_url('assets/frontpage/'); ?>/css/responsive.css">

  </head>
  <body>

	{header}
	{content}
	{footer}

    <!-- Go to Top Link -->
    <a href="#" class="back-to-top">
    	<i class="lni-arrow-up"></i>
    </a>
    
    <!-- Preloader -->
    <div id="preloader">
      <div class="loader" id="loader-1"></div>
    </div>
    <!-- End Preloader -->
    
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="<?php echo base_url('assets/frontpage/'); ?>/js/jquery-min.js"></script>
    <script src="<?php echo base_url('assets/frontpage/'); ?>/js/popper.min.js"></script>
    <script src="<?php echo base_url('assets/frontpage/'); ?>/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url('assets/frontpage/'); ?>/js/owl.carousel.min.js"></script>
    <script src="<?php echo base_url('assets/frontpage/'); ?>/js/wow.js"></script>
    <script src="<?php echo base_url('assets/frontpage/'); ?>/js/jquery.nav.js"></script>
    <script src="<?php echo base_url('assets/frontpage/'); ?>/js/scrolling-nav.js"></script>
    <script src="<?php echo base_url('assets/frontpage/'); ?>/js/jquery.easing.min.js"></script>
    <script src="<?php echo base_url('assets/frontpage/'); ?>/js/jquery.counterup.min.js"></script>      
    <script src="<?php echo base_url('assets/frontpage/'); ?>/js/waypoints.min.js"></script>   
    <script src="<?php echo base_url('assets/frontpage/'); ?>/js/main.js"></script>
      
  </body>
</html>
			