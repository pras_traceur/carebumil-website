<?php if (validation_errors()): ?>
	<div class="alert alert-warning alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		<h4><i class="icon fa fa-warning"></i> Alert!</h4>
		Pastikan form input yang wajib diisi telah terisi semuanya.
	</div>
<?php endif; ?>
<div class='row'>
	<div class="col-md-12">
		<div class="form-group">
			<label>Nama Pengguna :</label>
			<input type="text" class="form-control" name="nama_pengguna" placeholder="Enter ..." value="<?php echo set_value('nama_pengguna', ($item) ? $item['nama_pengguna'] : ''); ?>" disabled>
			<div class="text-orange"><?php echo form_error('nama_pengguna'); ?></div>
		</div>
		<div class="form-group">
			<label>Username :</label>
			<input type="text" name="username" class="form-control" placeholder="Enter ..." value="<?php echo set_value('username', ($item) ? $item['username'] : ''); ?>" disabled>
			<div class="text-orange"><?php echo form_error('username'); ?></div>
		</div>
		<div class="form-group">
			<label>Email :</label>
			<input type="email" name="email_pengguna"  class="form-control" placeholder="Enter ..." value="<?php echo set_value('email_pengguna', ($item) ? $item['email_pengguna'] : ''); ?>" disabled>
			<div class="text-orange"><?php echo form_error('email_pengguna'); ?></div>
		</div>
		<div class="form-group">
			<label for="password">Password :</label>
			<input type="password" name="password" class="form-control" id="password" placeholder="Enter ...">
			<div class="text-orange"><?php echo form_error('password'); ?></div>
		</div>
		<div class="form-group">
			<label for="repassword">re-Password :</label>
			<input type="password" name="repassword" class="form-control" id="repassword" placeholder="Enter ...">
			<div class="text-orange"><?php echo form_error('repassword'); ?></div>
		</div>
	</div>
</div>