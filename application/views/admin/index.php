<!DOCTYPE html>
<html class="loading" lang="en" xmlns:fb="http://www.facebook.com/2008/fbml">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="icon" href="<?php echo base_url('assets/img/logobumil.png'); ?>">  
		
		<title>{meta_title} | <?php echo pengaturan()->WEBSITE_NAMA; ?></title>

		<!-- Bootstrap core CSS -->
		<link  rel="stylesheet" href="<?php echo base_url() ?>assets/bootstrap/css/bootstrap.min.css">
		<link  rel="stylesheet" href="<?php echo base_url() ?>assets/datatables/css/dataTables.bootstrap.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
		<!-- Ionicons -->
		<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">

		<link rel="stylesheet" href="<?php echo base_url('assets/jquery/jquery-ui.css'); ?>">
		
		<!-- Select2 -->
		<link rel="stylesheet" href="<?php echo base_url('assets/plugins/select2/select2.min.css'); ?>">
	
		<!-- Theme style -->
		<link  rel="stylesheet" href="<?php echo base_url('assets/dist/css/AdminLTE.min.css'); ?>">
		<!-- AdminLTE Skins. Choose a skin from the css/skins folder instead of downloading all of them to reduce the load. -->
		<link  rel="stylesheet" href="<?php echo base_url('assets/dist/css/skins/_all-skins.min.css'); ?>">
		<link rel="stylesheet" href="<?php echo base_url('assets/jquery/jquery.fileupload.css'); ?>">
		<style>
			.table caption{ margin-bottom: 10px; }
			th.dt-center, td.dt-center { text-align: center; }
			.title_field_edit, .title_field_delete, .title-btn{ width: 50px; }
			.title_field_id{ width: 20px; }
			.text-orange{ color: orange; }
		</style>
		
		<!-- jQuery 2.1.4 -->
		<script src="<?php echo base_url('assets/plugins/jQuery/jQuery-2.1.4.min.js')?>"></script>
		<script src="<?php echo base_url('assets/jquery/jquery-ui.js') ?>"></script>

		<!-- Bootstrap 3.3.5 -->
		<script src="<?php echo base_url('assets/bootstrap/js/bootstrap.min.js')?>"></script>
		<script src="<?php echo base_url('assets/datatables/js/jquery.dataTables.min.js')?>"></script>
		<script src="<?php echo base_url('assets/datatables/js/dataTables.bootstrap.js')?>"></script>
		
		<!-- Select2 -->
		<script src="<?php echo base_url('assets/plugins/select2/select2.full.min.js') ?>"></script>
		
		<!-- InputMask -->
		<script src="<?php echo base_url('assets/plugins/input-mask/jquery.inputmask.js') ?>"></script>
		<script src="<?php echo base_url('assets/plugins/input-mask/jquery.inputmask.date.extensions.js') ?>"></script>
		<script src="<?php echo base_url('assets/plugins/input-mask/jquery.inputmask.extensions.js') ?>"></script>
	
		<!-- SlimScroll -->
		<script src="<?php echo base_url('assets/plugins/slimScroll/jquery.slimscroll.min.js')?>"></script>
		
		<!-- FastClick -->
		<script src="<?php echo base_url('assets/plugins/fastclick/fastclick.min.js')?>"></script>
		
		<!-- AdminLTE App -->
		<script src="<?php echo base_url('assets/dist/js/app.min.js')?>"></script>
		
		<!-- AdminLTE for demo purposes -->
		<script src="<?php echo base_url('assets/dist/js/demo.js')?>"></script>	
		
		<script src="<?php echo base_url('assets/jquery/jquery.fileupload.js') ?>"></script>


	</head>
	<body class="hold-transition skin-red sidebar-mini">
		<div class="wrapper">
		
			{header}
		
			{sidebar}
			
			<div class="content-wrapper">
				{content}
			</div>
			
			{footer}
			
			<div class="control-sidebar-bg"></div>
		</div>
		<script>
			$(function () {
				//Initialize Select2 Elements
				$(".select2").select2();

				//Datemask dd/mm/yyyy
				$("#datemask").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});

				//Money Euro
				$("[data-mask]").inputmask();
			});
		</script>
	</body>
</html>
