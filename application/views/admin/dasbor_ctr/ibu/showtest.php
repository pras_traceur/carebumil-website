<section class="content-header">
	<h1>
    <?=$meta_title?>
		<small>Preview</small>
	</h1>
	<ol class="breadcrumb">
        <li><a href="<?php echo base_url('admin'); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li><a href="<?php echo base_url('admin/test'); ?>"><i class="fa fa-dashboard"></i> Test</a></li>
		<li class="active"><?=$meta_title?></li>
	</ol>
</section>
<section class="content">

    <div class="row">
		<div class="col-md-6">
            <div class="info-box bg-green">
                <span class="info-box-icon"><i class="ion ion-ios-heart-outline"></i></span>

                <div class="info-box-content">
                <span class="info-box-text"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Hasil Test Menyebutkan Ibu </font></font></span>
                <span class="info-box-number"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"><?=$result['hasil']?></font></font></span>

                <div class="progress">
                    <div class="progress-bar" style="width: 20%"></div>
                </div>
                <span class="progress-description"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
                        <?php
                            if($result['hasil'] == 'Berisiko mengalami depresi'){
                                echo 'Silakan lanjutkan ke sesi konsultasi langsung dengan bidan';
                            }else{
                                echo 'Silakan isi kuesioner deteksi depresi kembali di trimester selanjutnya';
                            }
                        ?>
                    </font></font></span>
                </div>
                <!-- /.info-box-content -->
            </div>
        </div>
        <div class="col-md-6">
            <div class="info-box bg-aqua">
                <span class="info-box-icon"><i class="ion-ios-chatbubble-outline"></i></span>

                <div class="info-box-content">
                <span class="info-box-text"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Score Kuisioner</font></font></span>
                <span class="info-box-number"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"><?=$result['score']?></font></font></span>

                <div class="progress">
                    <div class="progress-bar" style="width: 40%"></div>
                </div>
                <span class="progress-description"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
                        Hasil Score Kuisioner
                    </font></font></span>
                </div>
                <!-- /.info-box-content -->
            </div>
        </div>
    </div>


	<div class="row">
		<div class="col-md-12">
			<!-- general form elements disabled -->
			<div class="box box-warning">
				<div class="box-header with-border">
					
					<h3 class="box-title">Hasil Test Tanggal <?=tanggalid($result['tanggal'])?></h3>
				</div><!-- /.box-header -->
				
				<div class="box-body">
                    <div class="form-group">
                        <div class="col-sm-12">
							<table class="table table-bordered">
								<tbody>
									<tr>
										<th style="width: 10px">#</th>
										<th>Pertanyaan</th>
										<th>Pilihan Jawaban</th>
									</tr>
									<tr>
										<td>1.</td>
										<td>Dalam 7 hari terakhir, Saya mampu tertawa dan merasakan hal-hal yang menyenangkan</td>
										<td>
                                            <input type="radio" name="k1" value="0" style="transform:translateY(2px)" <?=($result['k1'] == '0')?'checked':''?>>Sebanyak yang saya bisa<br>
                                            <input type="radio" name="k1" value="1" style="transform:translateY(2px)" <?=($result['k1'] == '1')?'checked':''?>>Tidak terlalu banyak<br>
                                            <input type="radio" name="k1" value="2" style="transform:translateY(2px)" <?=($result['k1'] == '2')?'checked':''?>>Tidak banyak<br>
                                            <input type="radio" name="k1" value="3" style="transform:translateY(2px)" <?=($result['k1'] == '3')?'checked':''?>>Tidak sama sekali<br>
                                        </td>
									</tr>
                                    <tr>
										<td>2.</td>
										<td>Dalam 7 hari terakhir, Saya melihat segala sesuatunya kedepan sangat menyenangkan</td>
										<td>
                                            <input type="radio" name="k2" value="0" style="transform:translateY(2px)" <?=($result['k2'] == '0')?'checked':''?>>Sebanyak sebelumya<br>
                                            <input type="radio" name="k2" value="1" style="transform:translateY(2px)" <?=($result['k2'] == '1')?'checked':''?>>Agak sedikit kurang dibandingkan dengan sebelumnya<br>
                                            <input type="radio" name="k2" value="2" style="transform:translateY(2px)" <?=($result['k2'] == '2')?'checked':''?>>Kurang dibandingkan dengan sebelumnya<br>
                                            <input type="radio" name="k2" value="3" style="transform:translateY(2px)" <?=($result['k2'] == '3')?'checked':''?>>Tidak pernah sama sekali<br>
                                        </td>
									</tr>
                                    <tr>
										<td>3.</td>
										<td>Dalam 7 hari terakhir, Saya menyalahkan diri saya sendiri saat sesuatu terjadi tidak sebagaimana mestinya</td>
										<td>
                                            <input type="radio" name="k3" value="3" style="transform:translateY(2px)" <?=($result['k3'] == '3')?'checked':''?>>Ya, setiap saat<br>
                                            <input type="radio" name="k3" value="2" style="transform:translateY(2px)" <?=($result['k3'] == '2')?'checked':''?>>Ya, kadang-kadang<br>
                                            <input type="radio" name="k3" value="1" style="transform:translateY(2px)" <?=($result['k3'] == '1')?'checked':''?>>Tidak terlalu sering<br>
                                            <input type="radio" name="k3" value="0" style="transform:translateY(2px)" <?=($result['k3'] == '0')?'checked':''?>>Tidak pernah sama sekali<br>
                                        </td>
									</tr>
									<tr>
										<td>4.</td>
										<td>Dalam 7 hari terakhir, Saya merasa cemas atau merasa kuatir tanpa alasan yang jelas</td>
										<td>
                                            <input type="radio" name="k4" value="0" style="transform:translateY(2px)" <?=($result['k4'] == '0')?'checked':''?>>Tidak pernah sama sekali<br>
                                            <input type="radio" name="k4" value="1" style="transform:translateY(2px)" <?=($result['k4'] == '1')?'checked':''?>>Jarang-jarang<br>
                                            <input type="radio" name="k4" value="2" style="transform:translateY(2px)" <?=($result['k4'] == '2')?'checked':''?>>Ya, kadang-kadang<br>
                                            <input type="radio" name="k4" value="3" style="transform:translateY(2px)" <?=($result['k4'] == '3')?'checked':''?>>Ya, sering sekali<br>
                                        </td>
									</tr>
									<tr>
										<td>5.</td>
										<td>Dalam 7 hari terakhir, Saya merasa takut atau panik tanpa alasan yang jelas</td>
										<td>
                                            <input type="radio" name="k5" value="3" style="transform:translateY(2px)" <?=($result['k5'] == '3')?'checked':''?>>Ya, cukup sering<br>
                                            <input type="radio" name="k5" value="2" style="transform:translateY(2px)" <?=($result['k5'] == '2')?'checked':''?>>Ya, kadang-kadang<br>
                                            <input type="radio" name="k5" value="1" style="transform:translateY(2px)" <?=($result['k5'] == '1')?'checked':''?>>Tidak terlalu sering<br>
                                            <input type="radio" name="k5" value="0" style="transform:translateY(2px)" <?=($result['k5'] == '0')?'checked':''?>>Tidak pernah sama sekali<br>
                                        </td>
									</tr>
									<tr>
										<td>6.</td>
										<td>Dalam 7 hari terakhir, Segala sesuatunya terasa sulit untuk dikerjakan</td>
										<td>
                                            <input type="radio" name="k6" value="3" style="transform:translateY(2px)" <?=($result['k6'] == '3')?'checked':''?>>Ya, hampir setiap saat saya tidak mampu menanganinya<br>
                                            <input type="radio" name="k6" value="2" style="transform:translateY(2px)" <?=($result['k6'] == '2')?'checked':''?>>Ya, kadang-kadang saya tidak mampu menangani seperti biasanya<br>
                                            <input type="radio" name="k6" value="1" style="transform:translateY(2px)" <?=($result['k6'] == '1')?'checked':''?>>Tidak terlalu, sebagian besar berhasil saya tangani<br>
                                            <input type="radio" name="k6" value="0" style="transform:translateY(2px)" <?=($result['k6'] == '0')?'checked':''?>>Tidak pernah, saya mampu mengerjakan segala sesuatu dengan baik<br>
                                        </td>
									</tr>
									<tr>
										<td>7.</td>
										<td>Dalam 7 hari terakhir, Saya merasa tidak bahagia sehingga mengalami kesulitan untuk tidur</td>
										<td>
                                            <input type="radio" name="k7" value="3" style="transform:translateY(2px)" <?=($result['k7'] == '3')?'checked':''?>>Ya, setiap saat<br>
                                            <input type="radio" name="k7" value="2" style="transform:translateY(2px)" <?=($result['k7'] == '2')?'checked':''?>>Ya, kadang-kadang<br>
                                            <input type="radio" name="k7" value="1" style="transform:translateY(2px)" <?=($result['k7'] == '1')?'checked':''?>>Tidak terlalu sering<br>
                                            <input type="radio" name="k7" value="0" style="transform:translateY(2px)" <?=($result['k7'] == '0')?'checked':''?>>Tidak pernah sama sekali<br>
                                        </td>
									</tr>
									<tr>
										<td>8.</td>
										<td>Dalam 7 hari terakhir, Saya merasa sedih dan merasa diri saya menyedihkan</td>
										<td>
                                            <input type="radio" name="k8" value="3" style="transform:translateY(2px)" <?=($result['k8'] == '3')?'checked':''?>>Ya, setiap saat<br>
                                            <input type="radio" name="k8" value="2" style="transform:translateY(2px)" <?=($result['k8'] == '2')?'checked':''?>>Ya, cukup sering<br>
                                            <input type="radio" name="k8" value="1" style="transform:translateY(2px)" <?=($result['k8'] == '1')?'checked':''?>>Tidak terlalu sering<br>
                                            <input type="radio" name="k8" value="0" style="transform:translateY(2px)" <?=($result['k8'] == '0')?'checked':''?>>Tidak pernah sama sekali<br>
                                        </td>
									</tr>
									<tr>
										<td>9.</td>
										<td>Dalam 7 hari terakhir, Saya merasa tidak bahagia sehingga menyebabkan saya menangis</td>
										<td>
                                            <input type="radio" name="k9" value="3" style="transform:translateY(2px)" <?=($result['k9'] == '3')?'checked':''?>>Ya, setiap saat<br>
                                            <input type="radio" name="k9" value="2" style="transform:translateY(2px)" <?=($result['k9'] == '2')?'checked':''?>>Ya, cukup sering<br>
                                            <input type="radio" name="k9" value="1" style="transform:translateY(2px)" <?=($result['k9'] == '1')?'checked':''?>>Disaat tertentu saja<br>
                                            <input type="radio" name="k9" value="0" style="transform:translateY(2px)" <?=($result['k9'] == '0')?'checked':''?>>Tidak pernah sama sekali<br>
                                        </td>
									</tr>
									<tr>
										<td>10.</td>
										<td>Dalam 7 hari terakhir, Muncul pikiran untuk menyakiti diri saya sendiri</td>
										<td>
                                            <input type="radio" name="k10" value="3" style="transform:translateY(2px)" <?=($result['k10'] == '3')?'checked':''?>>Ya, cukup sering<br>
                                            <input type="radio" name="k10" value="2" style="transform:translateY(2px)" <?=($result['k10'] == '2')?'checked':''?>>Kadang-kadang<br>
                                            <input type="radio" name="k10" value="1" style="transform:translateY(2px)" <?=($result['k10'] == '1')?'checked':''?>>Jarang sekali<br>
                                            <input type="radio" name="k10" value="0" style="transform:translateY(2px)" <?=($result['k10'] == '0')?'checked':''?>>Tidak pernah sama sekali<br>
                                        </td>
									</tr>
									

								</tbody>
			  				</table>
                        </div>
                    </div>
                	<br>
				</div>
				<div class="box-footer">
					<a href="javascript:javascript:history.go(-1)" class="btn btn-default">Kembali</a>
				</div>
				
			</div><!-- /.box -->
		</div>
	</div>
</section>