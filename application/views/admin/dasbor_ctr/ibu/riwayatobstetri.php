<section class="content-header">
	<h1>
    <?=$meta_title?>
		<small>Preview</small>
	</h1>
	<ol class="breadcrumb">
        <li><a href="<?php echo base_url('admin'); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="<?php echo base_url('admin/tentang'); ?>"><i class="fa fa-user"></i> Tentang Saya</a></li>
		<li class="active"><?=$meta_title?></li>
	</ol>
</section>
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<!-- general form elements disabled -->
			<div class="box box-warning">
				<div class="box-header with-border">
                    <?php if ($this->session->flashdata('pesan')) { ?>
					<div class="alert alert-success alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<h4><i class="icon fa fa-check"></i> Alert!</h4>
						<?php echo $this->session->flashdata('pesan'); ?>
					</div>
					<?php }else if ($this->session->flashdata('error')) { ?>
                    <div class="alert alert-danger alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<h4><i class="icon fa fa-check"></i> Alert!</h4>
						<?php echo $this->session->flashdata('error'); ?>
					</div>
                    <?php } ?>
					
					<h3 class="box-title">Riwayat Obsetri</h3>
				</div><!-- /.box-header -->
				
				<?php echo form_open_multipart('admin/submitriwayatobstetri/' . $this->session->userdata('id_pengguna')); ?>
				<div class="box-body">
					<div class="form-group">
                        <label class="col-sm-3 control-label" style="transform:translateY(8px)">Pernah depresi sebelumnya selama hamil/setelah persalinan ?</label>
                        <div class="col-sm-9">
                            <div class="row" style="transform:translateY(7px);">
                                <div class="col-sm-1">
                                    <input type="radio" name="k1" value="ya" <?=($result['k1'] == 'ya')?'checked':''?> required>Ya
                                </div>
                                <div class="col-sm-2">
                                    <input type="radio" name="k1" value="tidak" <?=($result['k1'] == 'tidak')?'checked':''?> required>Tidak
                                </div>
                                <div class="col-sm-9">
                                    <?php if($result['k1'] == 'ya'){ ?>
										<img src="<?=base_url('/assets/img/atention/atentionred.png')?>" style="width:40px;transform:translate(-80px,-9px);">
									<?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br><br>
					<div class="form-group">
                        <label class="col-sm-3 control-label" style="transform:translateY(8px)">Pernah keguguran ?</label>
                        <div class="col-sm-9">
                            <div class="row" style="transform:translateY(7px);">
                                <div class="col-sm-1">
                                    <input type="radio" name="k2" value="ya" <?=($result['k2'] == 'ya')?'checked':''?> required>Ya
                                </div>
                                <div class="col-sm-2">
                                    <input type="radio" name="k2" value="tidak" <?=($result['k2'] == 'tidak')?'checked':''?> required>Tidak
                                </div>
                                <div class="col-sm-9">
                                    <?php if($result['k2'] == 'ya'){ ?>
										<img src="<?=base_url('/assets/img/atention/atentionred.png')?>" style="width:40px;transform:translate(-80px,-9px);">
									<?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br><br>
					<div class="form-group">
                        <label class="col-sm-3 control-label" style="transform:translateY(8px)">Pernah bersalin caesar/dengan tindakan ?</label>
                        <div class="col-sm-9">
                            <div class="row" style="transform:translateY(7px);">
                                <div class="col-sm-1">
                                    <input type="radio" name="k3" value="ya" <?=($result['k3'] == 'ya')?'checked':''?> required onChange="lihat('tindakan')">Ya
                                </div>
                                <div class="col-sm-2">
                                    <input type="radio" name="k3" value="tidak" <?=($result['k3'] == 'tidak')?'checked':''?> required onChange="sembunyi('tindakan')">Tidak
                                </div>
                                <div class="col-sm-9">
                                    <?php if($result['k3'] == 'ya'){ ?>
										<img src="<?=base_url('/assets/img/atention/atentionred.png')?>" style="width:40px;transform:translate(-80px,-9px);">
									<?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br>
					<div class="form-group" id="tindakan" <?=($result['k3'] == 'tidak')?'style="display:none;"':''?>>
                    <br>
                        <label class="col-sm-3 control-label" style="transform:translateY(8px)">Jika ya apakah direncanakan ?</label>
                        <div class="col-sm-9">
                            <div class="row" style="transform:translateY(7px);">
                                <div class="col-sm-1">
                                    <input type="radio" name="k4" value="ya" <?=($result['k4'] == 'ya')?'checked':''?> required>Ya
                                </div>
                                <div class="col-sm-2">
                                    <input type="radio" name="k4" value="tidak" <?=($result['k4'] == 'tidak')?'checked':''?> required>Tidak
                                </div>
                                <div class="col-sm-9"></div>
                            </div>
                        </div>
                    </div>
                    <br><br>

					<div class="form-group">
                        <label class="col-sm-3 control-label" style="transform:translateY(8px)">Pernah mengalami komplikasi/masalah kehamilan/persalinan/nifas terdahulu ?</label>
                        <div class="col-sm-9">
                            <div class="row" style="transform:translateY(7px);">
                                <div class="col-sm-1">
                                    <input type="radio" name="k5" value="ya" <?=($result['k5'] == 'ya')?'checked':''?> required onChange="lihat('sebut')">Ya
                                </div>
                                <div class="col-sm-2">
                                    <input type="radio" name="k5" value="tidak" <?=($result['k5'] == 'tidak')?'checked':''?> required onChange="sembunyi('sebut')">Tidak
                                </div>
                                <div class="col-sm-9">
                                    <?php if($result['k5'] == 'ya'){ ?>
										<img src="<?=base_url('/assets/img/atention/atentionred.png')?>" style="width:40px;transform:translate(-80px,-9px);">
									<?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br><br>
					<div class="form-group" id="sebut" <?=($result['k5'] == 'tidak')?'style="display:none;"':''?>>
                        <label class="col-sm-3 control-label" style="transform:translateY(8px)">Jika Ya Sebutkan:</label>
                        <div class="col-sm-9">
                            <input type="text" name="jika_ya" class="form-control" value="<?=$result['jika_ya']?>" placeholder="Isi Keluhan/Masalah">
                        </div>
                    </div>
                    <br>

				</div>
				<div class="box-footer">
					<input class="btn btn-primary" type="submit" value="Simpan"/>
					<a href="<?php echo base_url('admin/tentang'); ?>" class="btn btn-default">Kembali</a>
				</div>
				
				<?php echo form_close(); ?>
				
			</div><!-- /.box -->
		</div>
	</div>
</section>

<script>
	function lihat(id){
		$('#'+id).show();
	}
	function sembunyi(id){
		$('#'+id).hide();
	}
</script>