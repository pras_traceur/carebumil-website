<link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
<style>
.zoom-hover:hover{
	transform:scale(1.1)!important;
}
</style>
<section class="content-header">
	<h1>
    <?=$meta_title?>
		<small>Preview</small>
	</h1>
	<ol class="breadcrumb">
        <li><a href="<?php echo base_url('admin'); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li class="active"><?=$meta_title?></li>
	</ol>
</section>
<section class="content">
    
	<br><br><br><br><br>
    <div class="row">
        <div class="col-md-1"></div>
		<div class="col-md-2">
			<a href="<?php echo base_url('admin/datadiri').'/'.$this->session->userdata('id_pengguna'); ?>">
				<img class="zoom-hover" data-aos="fade-up" src="<?php echo base_url('assets/img/ibu/tentang/logo_data_diri.png'); ?>">
			</a>
		</div>
		<div class="col-md-2">
			<a href="<?php echo base_url('admin/kehamilanini').'/'.$this->session->userdata('id_pengguna'); ?>">
				<img class="zoom-hover" data-aos="fade-up" src="<?php echo base_url('assets/img/ibu/tentang/logo_kehamilan_ini.png'); ?>">
			</a>
		</div>
		<div class="col-md-2">
			<a href="<?php echo base_url('admin/riwayatobstetri').'/'.$this->session->userdata('id_pengguna'); ?>">
				<img class="zoom-hover" data-aos="fade-up" src="<?php echo base_url('assets/img/ibu/tentang/logo_riwayat_obstetri.png'); ?>">
			</a>
		</div>
        <div class="col-md-2">
			<a href="<?php echo base_url('admin/dukungansosial').'/'.$this->session->userdata('id_pengguna'); ?>">
				<img class="zoom-hover" data-aos="fade-up" src="<?php echo base_url('assets/img/ibu/tentang/logo_dukungan_sosial.png'); ?>">
			</a>
		</div>
        <div class="col-md-2">
			<a href="<?php echo base_url('admin/persiapanpersalinan').'/'.$this->session->userdata('id_pengguna'); ?>">
				<img class="zoom-hover" data-aos="fade-up" src="<?php echo base_url('assets/img/ibu/tentang/logo_persiapan_persalinan.png'); ?>">
			</a>
		</div>
        <div class="col-md-1"></div>
    </div>
	<br><br><br><br><br>
	<a href="<?php echo base_url('admin'); ?>">
        <img class="zoom-hover" data-aos="fade-up" src="<?php echo base_url('assets/img/kembali.png'); ?>" width="10%">
    </a>
</section>
<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
<script>
  AOS.init();
</script>