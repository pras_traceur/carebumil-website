<section class="content-header">
	<h1>
    <?=$meta_title?>
		<small>Preview</small>
	</h1>
	<ol class="breadcrumb">
        <li><a href="<?php echo base_url('admin'); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="<?php echo base_url('admin/tentang'); ?>"><i class="fa fa-user"></i> Tentang Saya</a></li>
		<li class="active"><?=$meta_title?></li>
	</ol>
</section>
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<!-- general form elements disabled -->
			<div class="box box-warning">
				<div class="box-header with-border">
                    <?php if ($this->session->flashdata('pesan')) { ?>
					<div class="alert alert-success alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<h4><i class="icon fa fa-check"></i> Alert!</h4>
						<?php echo $this->session->flashdata('pesan'); ?>
					</div>
					<?php }else if ($this->session->flashdata('error')) { ?>
                    <div class="alert alert-danger alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<h4><i class="icon fa fa-check"></i> Alert!</h4>
						<?php echo $this->session->flashdata('error'); ?>
					</div>
                    <?php } ?>
					
					<h3 class="box-title">Persiapan Persalinan</h3>
				</div><!-- /.box-header -->
				
				<?php echo form_open_multipart('admin/submitpersiapanpersalinan/' . $this->session->userdata('id_pengguna')); ?>
				<div class="box-body">
					<div class="form-group">
                        <label class="col-sm-3 control-label" style="transform:translateY(8px)">Selama hamil, pernahkah mengikuti kelas hamil ?</label>
                        <div class="col-sm-9">
                            <div class="row" style="transform:translateY(7px);">
                                <div class="col-sm-2">
                                    <input type="radio" name="kelas_hamil" value="pernah" <?=($result['kelas_hamil'] == 'pernah')?'checked':''?> required>Ya
                                </div>
                                <div class="col-sm-2">
                                    <input type="radio" name="kelas_hamil" value="tidak" <?=($result['kelas_hamil'] == 'tidak')?'checked':''?> required>Tidak
                                </div>
                                <div class="col-sm-9"></div>
                            </div>
                        </div>
                    </div>
                    <br><br>
					<div class="form-group">
                        <label class="col-sm-3 control-label" style="transform:translateY(8px)">Rencana persalinan ingin ditolong :</label>
                        <div class="col-sm-9">
                            <input type="text" name="rencana_persalinan" class="form-control" value="<?=$result['rencana_persalinan']?>" placeholder="Dokter/Bidan/Lainnya, sebutkan" required>
                        </div>
                    </div>
                    <br>
					<div class="form-group">
                        <label class="col-sm-3 control-label" style="transform:translateY(8px)">Rencana tempat persalinan :</label>
                        <div class="col-sm-9">
                            <input type="text" name="di" class="form-control" value="<?=$result['di']?>" placeholder="Rumah sakit/Klinik Bidan/Lainnya, sebutkan" required>
                        </div>
                    </div>
                    <br><br>
					<div class="form-group">
                        <label class="col-sm-3 control-label" style="transform:translateY(8px)">Keinginan bersalin secara</label>
                        <div class="col-sm-9">
                            <div class="row" style="transform:translateY(7px);">
                                <div class="col-sm-2">
                                    <input type="radio" name="keinginan_bersalin" value="normal" <?=($result['keinginan_bersalin'] == 'normal')?'checked':''?> required>Normal
                                </div>
                                <div class="col-sm-2">
                                    <input type="radio" name="keinginan_bersalin" value="caesar" <?=($result['keinginan_bersalin'] == 'caesar')?'checked':''?> required>Caesar
                                </div>
                                <div class="col-sm-9"></div>
                            </div>
                        </div>
                    </div>
					<br><br>
					<div class="form-group">
                        <label class="col-sm-3 control-label" style="transform:translateY(8px)">Apakah ada asuransi untuk pembiayaan persalinan ?</label>
                        <div class="col-sm-9">
                            <div class="row" style="transform:translateY(7px);">
                                <div class="col-sm-2">
                                    <input type="radio" name="asuransi" value="ada" <?=($result['asuransi'] == 'ada')?'checked':''?> required>Ada
                                </div>
                                <div class="col-sm-2">
                                    <input type="radio" name="asuransi" value="belum" <?=($result['asuransi'] == 'belum')?'checked':''?> required>Belum Ada
                                </div>
                                <div class="col-sm-8">
                                    <?php if($result['asuransi'] == 'belum'){ ?>
										<img src="<?=base_url('/assets/img/atention/atentionred.png')?>" style="width:40px;transform:translate(-50px,-9px);">
									<?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br><br>
					<div class="form-group">
                        <label class="col-sm-3 control-label" style="transform:translateY(8px)">Apakah sudah ada persiapan dana untuk persalinan ?</label>
                        <div class="col-sm-9">
                            <div class="row" style="transform:translateY(7px);">
							    <div class="col-sm-2">
                                    <input type="radio" name="persiapan_dana" value="ada" <?=($result['persiapan_dana'] == 'ada')?'checked':''?> required>Ada
                                </div>
                                <div class="col-sm-2">
                                    <input type="radio" name="persiapan_dana" value="belum" <?=($result['persiapan_dana'] == 'belum')?'checked':''?> required>Belum Ada
                                </div>
                                <div class="col-sm-8">
                                    <?php if($result['persiapan_dana'] == 'belum'){ ?>
										<img src="<?=base_url('/assets/img/atention/atentionred.png')?>" style="width:40px;transform:translate(-50px,-9px);">
									<?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br><br>
					
				</div>
				<div class="box-footer">
					<input class="btn btn-primary" type="submit" value="Simpan"/>
					<a href="<?php echo base_url('admin/tentang'); ?>" class="btn btn-default">Kembali</a>
				</div>
				
				<?php echo form_close(); ?>
				
			</div><!-- /.box -->
		</div>
	</div>
</section>