
<!-- jquery -->
<script src="<?php echo base_url('assets/tables/jquery-3.5.1.js')?>"></script>

<!-- datatables -->
<link  rel="stylesheet" href="<?php echo base_url() ?>assets/tables/jquery.dataTables.min.css">
<script src="<?php echo base_url('assets/tables/jquery.dataTables.min.js')?>"></script>	
<style>
.btnz {
  padding: 12px 24px;
  color: white;
  display: inline-block;
  opacity: 1;
  border-radius: 4px;
}

.btnz:hover {
  opacity: 0.8;
}

.message {
  padding: 15px 70px;
  background-color: #21ba45;
  cursor: pointer;
  box-shadow: 0 7px #1a7940;
}

.message:active {
  box-shadow: none;
  position: relative;
  top: 7px;
}
</style>

<section class="content-header">
	<h1>
    <?=$meta_title?>
		<small>Preview</small>
	</h1>
	<ol class="breadcrumb">
        <li><a href="<?php echo base_url('admin'); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li class="active"><?=$meta_title?></li>
	</ol>
</section>
<section class="content">
	<div class="row">
		<div class="col-md-12">
            <div class="box box-success">
				<div class="box-header with-border">
                <h3 class="box-title">Test Baru</h3>
                </div>
                <div class="box-body">
                    <center>
                        <h4>
                            Questioner Edinburgh Postnatal Depression Scale (EPDS) yaitu Test depresi perinatal yang telah digunakan secara umum untuk mendeteksi depresi pada kehamilan.
                        </h4>
                        <?php if($aktif){ ?>
                            <a href="<?=base_url('admin/newtest')?>"><span class="btnz message">TEST SEKARANG</span></a>
                        <?php }else{ ?>
                            <a data-toggle="modal" data-target="#modal-info"><span class="btnz message">TEST SEKARANG</span></a>
                        <?php } ?>
                    </center>
                    <br>
                </div>
            </div>
			<!-- general form elements disabled -->
			<div class="box box-warning">
				<div class="box-header with-border">
                    <?php if ($this->session->flashdata('pesan')) { ?>
					<div class="alert alert-success alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<h4><i class="icon fa fa-check"></i> Alert!</h4>
						<?php echo $this->session->flashdata('pesan'); ?>
					</div>
					<?php }else if ($this->session->flashdata('error')) { ?>
                    <div class="alert alert-danger alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<h4><i class="icon fa fa-check"></i> Alert!</h4>
						<?php echo $this->session->flashdata('error'); ?>
					</div>
                    <?php } ?>
					
					<h3 class="box-title">Data Test Ibu</h3>
				</div><!-- /.box-header -->
				
				<div class="box-body">
                    <table id="example" class="table table-striped table-bordered" style="width:100%">
                        <thead>
                            <tr>
                                <th style="width:10px;">No</th>
                                <th>Tanggal Test</th>
                                <th>Score</th>
                                <th>Hasil Test</th>
                                <th style="width:150px;">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                if($result){
                                    $no=1;
                                    foreach($result as $items){
                                ?>
                                    <tr>
                                        <td><?=$no?></td>
                                        <td><?=tanggalid($items['tanggal'])?></td>
                                        <td><?=$items['score']?></td>
                                        <td><?=$items['hasil']?></td>
                                        <td><a href="<?=base_url('admin/showtest/'.$items['id'])?>" class="btn btn-sm btn-success"><i class="glyphicon glyphicon-eye-open"></i>&nbsp;Lihat</a></td>
                                    </tr>
                                <?php $no++;}} ?>
                        </tbody>
                    </table>
                </div>
				
			</div><!-- /.box -->
		</div>
	</div>
    <a href="<?php echo base_url('admin'); ?>">
        <img class="zoom-hover" data-aos="fade-up" src="<?php echo base_url('assets/img/kembali.png'); ?>" width="10%">
    </a>
</section>

<div class="modal modal-info fade" id="modal-info" style="display: none;">
    <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span></button>
        <h4 class="modal-title">Informasi</h4>
        </div>
        <div class="modal-body">
        <p>Anda Telah Melakukan Test Hari ini datang lagi di Hari yang lain</p>
        </div>
        <div class="modal-footer">
        <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
        </div>
    </div>
    <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<script>
$(document).ready(function() {
    $('#example').DataTable( {
        columnDefs: [ {
            targets: [ 0 ],
            orderData: [ 0, 1 ]
        }, {
            targets: [ 1 ],
            orderData: [ 1, 0 ]
        }]
    } );
} );
</script>