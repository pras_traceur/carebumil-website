<section class="content-header">
	<h1>
    <?=$meta_title?>
		<small>Preview</small>
	</h1>
	<ol class="breadcrumb">
        <li><a href="<?php echo base_url('admin'); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="<?php echo base_url('admin/tentang'); ?>"><i class="fa fa-user"></i> Tentang Saya</a></li>
		<li class="active"><?=$meta_title?></li>
	</ol>
</section>
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<!-- general form elements disabled -->
			<div class="box box-warning">
				<div class="box-header with-border">
                    <?php if ($this->session->flashdata('pesan')) { ?>
					<div class="alert alert-success alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<h4><i class="icon fa fa-check"></i> Alert!</h4>
						<?php echo $this->session->flashdata('pesan'); ?>
					</div>
					<?php }else if ($this->session->flashdata('error')) { ?>
                    <div class="alert alert-danger alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<h4><i class="icon fa fa-check"></i> Alert!</h4>
						<?php echo $this->session->flashdata('error'); ?>
					</div>
                    <?php } ?>
					
					<h3 class="box-title">Kehamilan ini</h3>
				</div><!-- /.box-header -->
				
				<?php echo form_open_multipart('admin/submitkehamilan/' . $this->session->userdata('id_pengguna')); ?>
				<div class="box-body">
                    <div class="form-group">
                        <label class="col-sm-3 control-label" style="transform:translateY(8px)">Hamil ke ?</label>
						<?php if($result['hamil_ke'] == 1){ ?>
							<div class="col-sm-8">
								<input type="number" name="hamil_ke" class="form-control" value="<?=$result['hamil_ke']?>" required>
							</div>
							<div class="col-sm-1">
								<img src="<?=base_url('/assets/img/atention/atentionred.png')?>" style="width:40px;margin-top: -1px;">
							</div>
						<?php }else{ ?>
							<div class="col-sm-9">
								<input type="number" name="hamil_ke" class="form-control" value="<?=$result['hamil_ke']?>" required>
							</div>
						<?php } ?>
                    </div>
                    <br>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" style="transform:translateY(8px)">Hari Pertama Haid Terakhir</label>
                        <div class="col-sm-4">
                            <div class="input-group date">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="date" name="tanggal_pertama_haid" class="form-control" value="<?=$result['tanggal_pertama_haid']?>" required>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <center style="transform:translateY(5px)">Hari Perkiraan Lahir</center>
                        </div>
                        <div class="col-sm-3">
                            <input type="text" id="HPL" class="form-control" disabled>
                        </div>
                    </div>
                    <br>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" style="transform:translateY(8px)">Apakah hamil ini direncanakan ?</label>
                        <div class="col-sm-9">
                            <div class="row" style="transform:translateY(7px);">
                                <div class="col-sm-1">
                                    <input type="radio" name="hamil_direncanakan" value="ya" <?=($result['hamil_direncanakan'] == 'ya')?'checked':''?> required>Ya
                                </div>
                                <div class="col-sm-2">
                                    <input type="radio" name="hamil_direncanakan" value="tidak" <?=($result['hamil_direncanakan'] == 'tidak')?'checked':''?> required>Tidak
                                </div>
                                <div class="col-sm-9">
									<?php if($result['hamil_direncanakan'] == 'tidak'){ ?>
										<img src="<?=base_url('/assets/img/atention/atentionred.png')?>" style="width:40px;transform:translate(-80px,-9px);">
									<?php } ?>
								</div>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" style="transform:translateY(8px)">Harapan keluarga untuk jenis kelamin bayi</label>
                        <div class="col-sm-9">
                            <div class="row" style="transform:translateY(7px);">
                                <div class="col-sm-1">
                                    <input type="radio" name="harapan_jenis_kelamin" value="ada" <?=($result['harapan_jenis_kelamin'] == 'ada')?'checked':''?> required onChange="lihat('kalau')">Ada
                                </div>
                                <div class="col-sm-2">
                                    <input type="radio" name="harapan_jenis_kelamin" value="tidak" <?=($result['harapan_jenis_kelamin'] == 'tidak')?'checked':''?> required onChange="sembunyi('kalau')">Tidak
                                </div>
								<div class="col-sm-9">
									<?php if($result['harapan_jenis_kelamin'] == 'ada'){ ?>
										<img src="<?=base_url('/assets/img/atention/atentionred.png')?>" style="width:40px;transform:translate(-80px,-9px);">
									<?php } ?>
								</div>
                            </div>
                        </div>
                    </div>
                    <br><br>
                    <div class="form-group" id="kalau" <?=($result['harapan_jenis_kelamin'] == 'tidak')?'style="display:none;"':''?>>
                        <label class="col-sm-3 control-label" style="transform:translateY(8px)">Kalau ada:</label>
                        <div class="col-sm-9">
                            <input type="text" name="kalau_ada" class="form-control" value="<?=$result['kalau_ada']?>" placeholder="Laki-laki/Perempuan">
                        </div>
                    </div>
                    <br>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" style="transform:translateY(8px)">Apakah sedang mengkonsumsi obat- obatan antidepresi ?</label>
                        <div class="col-sm-9">
                            <div class="row" style="transform:translateY(7px);">
                                <div class="col-sm-1">
                                    <input type="radio" name="antidepresi" value="ya" <?=($result['antidepresi'] == 'ya')?'checked':''?> required>Ya
                                </div>
                                <div class="col-sm-2">
                                    <input type="radio" name="antidepresi" value="tidak" <?=($result['antidepresi'] == 'tidak')?'checked':''?> required>Tidak
                                </div>
								<div class="col-sm-9">
									<?php if($result['antidepresi'] == 'ya'){ ?>
										<img src="<?=base_url('/assets/img/atention/atentionred.png')?>" style="width:40px;transform:translate(-80px,-9px);">
									<?php } ?>
								</div>
                            </div>
                        </div>
                    </div>
                    <br><br>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" style="transform:translateY(8px)">Apakah selama hamil ini mengalami keluhan/masalah ?</label>
                        <div class="col-sm-9">
                            <div class="row" style="transform:translateY(7px);">
                                <div class="col-sm-1">
                                    <input type="radio" name="keluhan" value="ya" <?=($result['keluhan'] == 'ya')?'checked':''?> required onChange="lihat('sebut')">Ya
                                </div>
                                <div class="col-sm-2">
                                    <input type="radio" name="keluhan" value="tidak" <?=($result['keluhan'] == 'tidak')?'checked':''?> required onChange="sembunyi('sebut')">Tidak
                                </div>
								<div class="col-sm-9">
									<?php if($result['keluhan'] == 'ya'){ ?>
										<img src="<?=base_url('/assets/img/atention/atentionred.png')?>" style="width:40px;transform:translate(-80px,-9px);">
									<?php } ?>
								</div>
                            </div>
                        </div>
                    </div>
                    <br><br>
                    <div class="form-group" id="sebut" <?=($result['keluhan'] == 'tidak')?'style="display:none;"':''?>>
                        <label class="col-sm-3 control-label" style="transform:translateY(8px)">Jika Ya Sebutkan:</label>
                        <div class="col-sm-9">
                            <input type="text" name="jika_ada" class="form-control" value="<?=$result['jika_ada']?>" placeholder="Isi Keluhan/Masalah">
                        </div>
                    </div>
                    <br>
                </div>
				<div class="box-footer">
					<input class="btn btn-primary" type="submit" value="Simpan"/>
					<a href="<?php echo base_url('admin/tentang'); ?>" class="btn btn-default">Kembali</a>
				</div>
				
				<?php echo form_close(); ?>
				
			</div><!-- /.box -->
		</div>
	</div>
</section>
<script type="text/javascript">

	function hplhitung(){
		var HHT   = <?=date_format(date_create($result['tanggal_pertama_haid']),"d")?>;
		var bulan = <?=date_format(date_create($result['tanggal_pertama_haid']),"m")?>;
		var tahun = <?=date_format(date_create($result['tanggal_pertama_haid']),"Y")?>;

		// HPL naigale
		if(bulan >=1 && bulan <= 3){
			HHT   = +HHT +7;
			bulan = +bulan + 9;
			tahun = +tahun + 0;
		} else if (bulan >= 4 && bulan <= 12){
			HHT   = +HHT +7;
			bulan = bulan - 3;
			tahun = +tahun + 1;	
		} 

		// validasi bulan & tahun
		if(bulan == 2){
			if(tahun%2==0){
				if(HHT>=29){
					HHT   = HHT - 29;
					bulan = bulan - 1;
				} 
			} else {
				if(HHT>=28){
					HHT = HHT - 28;
					bulan = bulan - 1;
				}
			}
		}else if(bulan%2==0){
			if(bulan==8){
				if(HHT>31){
					HHT   = HHT - 31;
					bulan = +bulan + 1;
				}
			} else if(bulan>12){
				if(HHT>30){
					HHT   = HHT - 30;
					bulan = bulan - 12;
					tahun = +tahun + 1;
				}else{
					bulan = bulan - 12;
					tahun = +tahun + 1;	
				}
			}else{
				if(HHT>30){
					HHT   = HHT - 30;
					if(bulan>=12){
						bulan = bulan - 11;			
					} else {
						bulan = +bulan + 1;	
					}
				}
			}

		} else if(bulan%2!=0){
			if(bulan>12){
				if(HHT>31){
					HHT   = HHT - 31;
					bulan = bulan - 12;
					tahun = +tahun + 1;
				}else{
					bulan = bulan - 11;
					tahun = +tahun + 1;	
				}
			}else if(HHT>31){
				HHT   = HHT - 31;
				bulan = +bulan + 1;	
			}
		}
		
		// konversi bulan
		if(bulan==1){
			bulan = "Januari";
		} else if(bulan==2){
			bulan = "Februari";
		} else if (bulan==3){
			bulan = "Maret";
		} else if (bulan==4){
			bulan = "April";
		} else if (bulan==5){
			bulan = "Mei";
		} else if (bulan==6){
			bulan = "Juni";
		} else if (bulan==7){
			bulan = "Juli";
		} else if (bulan==8){
			bulan = "Agutus";
		} else if (bulan==9){
			bulan = "September";
		} else if (bulan==10){
			bulan = "Oktober";
		} else if (bulan==11){
			bulan = "November";
		} else if (bulan==12){
			bulan = "Desember";
		} 

		HPL = HHT+' - '+bulan+' - '+tahun;

		$('#HPL').val(HPL);
	};

    window.onload = function() {
        hplhitung();
    };
    
    $(function() {
        $('input[name="tanggal_pertama_haid"]').daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            minYear: 1901,
            maxYear: parseInt(moment().format('YYYY'),10)
        }, function(start, end, label) {
            var years = moment().diff(start, 'years');
        });
    });

	function lihat(id){
		$('#'+id).show();
	}
	function sembunyi(id){
		$('#'+id).hide();
	}
</script>