<section class="content-header">
	<h1>
    <?=$meta_title?>
		<small>Preview</small>
	</h1>
	<ol class="breadcrumb">
        <li><a href="<?php echo base_url('admin'); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li><a href="<?php echo base_url('admin/test'); ?>"><i class="fa fa-dashboard"></i> Test</a></li>
		<li class="active"><?=$meta_title?></li>
	</ol>
</section>
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<!-- general form elements disabled -->
			<div class="box box-warning">
				<div class="box-header with-border">
					<?php if ($this->session->flashdata('pesan')) { ?>
					<div class="alert alert-success alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<h4><i class="icon fa fa-check"></i> Alert!</h4>
						<?php echo $this->session->flashdata('pesan'); ?>
					</div>
					<?php }else if ($this->session->flashdata('error')) { ?>
                    <div class="alert alert-danger alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<h4><i class="icon fa fa-check"></i> Alert!</h4>
						<?php echo $this->session->flashdata('error'); ?>
					</div>
                    <?php } ?>
					
					<h3 class="box-title">Test</h3>
				</div><!-- /.box-header -->
				
				<?php echo form_open_multipart('admin/submittest/' . $this->session->userdata('id_pengguna')); ?>
				<div class="box-body">
                    <div class="form-group">
                        <div class="col-sm-12">
                        - Ibu hamil mengisi questioner Edinburgh Postnatal Depression Scale (EPDS)<br>
                        - Mohon memilih jawaban yang paling mendekati keadaan perasaan ibu DALAM 7 HARI TERAKHIR, bukan hanya perasaan ibu hari ini. Tidak ada jawaban benar atau salah dalam pertanyaan ini.
                        <br><br>
							<table class="table table-bordered">
								<tbody>
									<tr>
										<th style="width: 10px">#</th>
										<th>Pertanyaan</th>
										<th>Pilihan Jawaban</th>
									</tr>
									<tr>
										<td>1.</td>
										<td>Dalam 7 hari terakhir, Saya mampu tertawa dan merasakan hal-hal yang menyenangkan</td>
										<td>
                                            <input type="radio" name="k1" value="0" style="transform:translateY(2px)" required>Sebanyak yang saya bisa<br>
                                            <input type="radio" name="k1" value="1" style="transform:translateY(2px)" required>Tidak terlalu banyak<br>
                                            <input type="radio" name="k1" value="2" style="transform:translateY(2px)" required>Tidak banyak<br>
                                            <input type="radio" name="k1" value="3" style="transform:translateY(2px)" required>Tidak sama sekali<br>
                                        </td>
									</tr>
                                    <tr>
										<td>2.</td>
										<td>Dalam 7 hari terakhir, Saya melihat segala sesuatunya kedepan sangat menyenangkan</td>
										<td>
                                            <input type="radio" name="k2" value="0" style="transform:translateY(2px)" required>Sebanyak sebelumya<br>
                                            <input type="radio" name="k2" value="1" style="transform:translateY(2px)" required>Agak sedikit kurang dibandingkan dengan sebelumnya<br>
                                            <input type="radio" name="k2" value="2" style="transform:translateY(2px)" required>Kurang dibandingkan dengan sebelumnya<br>
                                            <input type="radio" name="k2" value="3" style="transform:translateY(2px)" required>Tidak pernah sama sekali<br>
                                        </td>
									</tr>
                                    <tr>
										<td>3.</td>
										<td>Dalam 7 hari terakhir, Saya menyalahkan diri saya sendiri saat sesuatu terjadi tidak sebagaimana mestinya</td>
										<td>
                                            <input type="radio" name="k3" value="3" style="transform:translateY(2px)" required>Ya, setiap saat<br>
                                            <input type="radio" name="k3" value="2" style="transform:translateY(2px)" required>Ya, kadang-kadang<br>
                                            <input type="radio" name="k3" value="1" style="transform:translateY(2px)" required>Tidak terlalu sering<br>
                                            <input type="radio" name="k3" value="0" style="transform:translateY(2px)" required>Tidak pernah sama sekali<br>
                                        </td>
									</tr>
									<tr>
										<td>4.</td>
										<td>Dalam 7 hari terakhir, Saya merasa cemas atau merasa kuatir tanpa alasan yang jelas</td>
										<td>
                                            <input type="radio" name="k4" value="0" style="transform:translateY(2px)" required>Tidak pernah sama sekali<br>
                                            <input type="radio" name="k4" value="1" style="transform:translateY(2px)" required>Jarang-jarang<br>
                                            <input type="radio" name="k4" value="2" style="transform:translateY(2px)" required>Ya, kadang-kadang<br>
                                            <input type="radio" name="k4" value="3" style="transform:translateY(2px)" required>Ya, sering sekali<br>
                                        </td>
									</tr>
									<tr>
										<td>5.</td>
										<td>Dalam 7 hari terakhir, Saya merasa takut atau panik tanpa alasan yang jelas</td>
										<td>
                                            <input type="radio" name="k5" value="3" style="transform:translateY(2px)" required>Ya, cukup sering<br>
                                            <input type="radio" name="k5" value="2" style="transform:translateY(2px)" required>Ya, kadang-kadang<br>
                                            <input type="radio" name="k5" value="1" style="transform:translateY(2px)" required>Tidak terlalu sering<br>
                                            <input type="radio" name="k5" value="0" style="transform:translateY(2px)" required>Tidak pernah sama sekali<br>
                                        </td>
									</tr>
									<tr>
										<td>6.</td>
										<td>Dalam 7 hari terakhir, Segala sesuatunya terasa sulit untuk dikerjakan</td>
										<td>
                                            <input type="radio" name="k6" value="3" style="transform:translateY(2px)" required>Ya, hampir setiap saat saya tidak mampu menanganinya<br>
                                            <input type="radio" name="k6" value="2" style="transform:translateY(2px)" required>Ya, kadang-kadang saya tidak mampu menangani seperti biasanya<br>
                                            <input type="radio" name="k6" value="1" style="transform:translateY(2px)" required>Tidak terlalu, sebagian besar berhasil saya tangani<br>
                                            <input type="radio" name="k6" value="0" style="transform:translateY(2px)" required>Tidak pernah, saya mampu mengerjakan segala sesuatu dengan baik<br>
                                        </td>
									</tr>
									<tr>
										<td>7.</td>
										<td>Dalam 7 hari terakhir, Saya merasa tidak bahagia sehingga mengalami kesulitan untuk tidur</td>
										<td>
                                            <input type="radio" name="k7" value="3" style="transform:translateY(2px)" required>Ya, setiap saat<br>
                                            <input type="radio" name="k7" value="2" style="transform:translateY(2px)" required>Ya, kadang-kadang<br>
                                            <input type="radio" name="k7" value="1" style="transform:translateY(2px)" required>Tidak terlalu sering<br>
                                            <input type="radio" name="k7" value="0" style="transform:translateY(2px)" required>Tidak pernah sama sekali<br>
                                        </td>
									</tr>
									<tr>
										<td>8.</td>
										<td>Dalam 7 hari terakhir, Saya merasa sedih dan merasa diri saya menyedihkan</td>
										<td>
                                            <input type="radio" name="k8" value="3" style="transform:translateY(2px)" required>Ya, setiap saat<br>
                                            <input type="radio" name="k8" value="2" style="transform:translateY(2px)" required>Ya, cukup sering<br>
                                            <input type="radio" name="k8" value="1" style="transform:translateY(2px)" required>Tidak terlalu sering<br>
                                            <input type="radio" name="k8" value="0" style="transform:translateY(2px)" required>Tidak pernah sama sekali<br>
                                        </td>
									</tr>
									<tr>
										<td>9.</td>
										<td>Dalam 7 hari terakhir, Saya merasa tidak bahagia sehingga menyebabkan saya menangis</td>
										<td>
                                            <input type="radio" name="k9" value="3" style="transform:translateY(2px)" required>Ya, setiap saat<br>
                                            <input type="radio" name="k9" value="2" style="transform:translateY(2px)" required>Ya, cukup sering<br>
                                            <input type="radio" name="k9" value="1" style="transform:translateY(2px)" required>Disaat tertentu saja<br>
                                            <input type="radio" name="k9" value="0" style="transform:translateY(2px)" required>Tidak pernah sama sekali<br>
                                        </td>
									</tr>
									<tr>
										<td>10.</td>
										<td>Dalam 7 hari terakhir, Muncul pikiran untuk menyakiti diri saya sendiri</td>
										<td>
                                            <input type="radio" name="k10" value="3" style="transform:translateY(2px)" required>Ya, cukup sering<br>
                                            <input type="radio" name="k10" value="2" style="transform:translateY(2px)" required>Kadang-kadang<br>
                                            <input type="radio" name="k10" value="1" style="transform:translateY(2px)" required>Jarang sekali<br>
                                            <input type="radio" name="k10" value="0" style="transform:translateY(2px)" required>Tidak pernah sama sekali<br>
                                        </td>
									</tr>
									

								</tbody>
			  				</table>
                        </div>
                    </div>
                	<br>
				</div>
				<div class="box-footer">
					<input class="btn btn-primary" type="submit" value="Simpan"/>
					<a href="<?php echo base_url('admin/test/'.$this->session->userdata('id_pengguna')); ?>" class="btn btn-default">Kembali</a>
				</div>
				
				<?php echo form_close(); ?>
				
			</div><!-- /.box -->
		</div>
	</div>
</section>