<section class="content-header">
	<h1>
    <?=$meta_title?>
		<small>Preview</small>
	</h1>
	<ol class="breadcrumb">
        <li><a href="<?php echo base_url('admin'); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="<?php echo base_url('admin/tentang'); ?>"><i class="fa fa-user"></i> Tentang Saya</a></li>
		<li class="active"><?=$meta_title?></li>
	</ol>
</section>
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<!-- general form elements disabled -->
			<div class="box box-warning">
				<div class="box-header with-border">
					<?php if ($this->session->flashdata('pesan')) { ?>
					<div class="alert alert-success alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<h4><i class="icon fa fa-check"></i> Alert!</h4>
						<?php echo $this->session->flashdata('pesan'); ?>
					</div>
					<?php }else if ($this->session->flashdata('error')) { ?>
                    <div class="alert alert-danger alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<h4><i class="icon fa fa-check"></i> Alert!</h4>
						<?php echo $this->session->flashdata('error'); ?>
					</div>
                    <?php } ?>
					
					<h3 class="box-title">Dukungan Sosial</h3>
				</div><!-- /.box-header -->
				
				<?php echo form_open_multipart('admin/submitdukungansosial/' . $this->session->userdata('id_pengguna')); ?>
				<div class="box-body">
					<div class="form-group">
                        <label class="col-sm-3 control-label" style="transform:translateY(8px)">Pernahkah mengalami kekerasan dalam rumah tangga ?</label>
                        <div class="col-sm-9">
                            <div class="row" style="transform:translateY(7px);">
                                <div class="col-sm-1">
                                    <input type="radio" name="kekerasan" value="ya" <?=($result['kekerasan'] == 'ya')?'checked':''?> required>Ya
                                </div>
                                <div class="col-sm-2">
                                    <input type="radio" name="kekerasan" value="tidak" <?=($result['kekerasan'] == 'tidak')?'checked':''?> required>Tidak
                                </div>
                                <div class="col-sm-9">
									<?php if($result['kekerasan'] == 'ya'){ ?>
										<img src="<?=base_url('/assets/img/atention/atentionred.png')?>" style="width:40px;transform:translate(-80px,-9px);">
									<?php } ?>
								</div>
                            </div>
                        </div>
                    </div>
                	<br><br>
					<div class="form-group">
                        <label class="col-sm-3 control-label" style="transform:translateY(8px)">Kuesioner dukungan sosial</label>
                        <div class="col-sm-9">
							Untuk masing-masing pernyataan, centang salah satu kotak pilihan jawaban yang paling sesuai dengan yang ibu rasakan saat ini.
							<table class="table table-bordered">
								<tbody>
									<tr>
										<th style="width: 10px">#</th>
										<th>Pernyataan</th>
										<th>Selalu</th>
										<th>Sering</th>
										<th>Kadang</th>
										<th>Jarang</th>
										<th>Tidak Pernah</th>
									</tr>
									<tr>
										<td>1.</td>
										<td>Saya memiliki teman yang sangat mendukung saya</td>
										<td><input type="radio" name="k1" value="5" <?=($result['k1'] == '5')?'checked':''?> style="transform:translateY(2px)" required>5</td>
										<td><input type="radio" name="k1" value="4" <?=($result['k1'] == '4')?'checked':''?> style="transform:translateY(2px)" required>4</td>
										<td><input type="radio" name="k1" value="3" <?=($result['k1'] == '3')?'checked':''?> style="transform:translateY(2px)" required>3</td>
										<td><input type="radio" name="k1" value="2" <?=($result['k1'] == '2')?'checked':''?> style="transform:translateY(2px)" required>2</td>
										<td><input type="radio" name="k1" value="1" <?=($result['k1'] == '1')?'checked':''?> style="transform:translateY(2px)" required>1</td>
									</tr>
									<tr>
										<td>2.</td>
										<td>Keluarga sata selalu ada untuk saya</td>
										<td><input type="radio" name="k2" value="5" <?=($result['k2'] == '5')?'checked':''?> style="transform:translateY(2px)" required>5</td>
										<td><input type="radio" name="k2" value="4" <?=($result['k2'] == '4')?'checked':''?> style="transform:translateY(2px)" required>4</td>
										<td><input type="radio" name="k2" value="3" <?=($result['k2'] == '3')?'checked':''?> style="transform:translateY(2px)" required>3</td>
										<td><input type="radio" name="k2" value="2" <?=($result['k2'] == '2')?'checked':''?> style="transform:translateY(2px)" required>2</td>
										<td><input type="radio" name="k2" value="1" <?=($result['k2'] == '1')?'checked':''?> style="transform:translateY(2px)" required>1</td>
									</tr>
									<tr>
										<td>3.</td>
										<td>Suami saya sangat membantu</td>
										<td><input type="radio" name="k3" value="5" <?=($result['k3'] == '5')?'checked':''?> style="transform:translateY(2px)" required>5</td>
										<td><input type="radio" name="k3" value="4" <?=($result['k3'] == '4')?'checked':''?> style="transform:translateY(2px)" required>4</td>
										<td><input type="radio" name="k3" value="3" <?=($result['k3'] == '3')?'checked':''?> style="transform:translateY(2px)" required>3</td>
										<td><input type="radio" name="k3" value="2" <?=($result['k3'] == '2')?'checked':''?> style="transform:translateY(2px)" required>2</td>
										<td><input type="radio" name="k3" value="1" <?=($result['k3'] == '1')?'checked':''?> style="transform:translateY(2px)" required>1</td>
									</tr>
									<tr>
										<td>4.</td>
										<td>Saya memiliki konflik dengan suami</td>
										<td><input type="radio" name="k4" value="5" <?=($result['k4'] == '5')?'checked':''?> style="transform:translateY(2px)" required>5</td>
										<td><input type="radio" name="k4" value="4" <?=($result['k4'] == '4')?'checked':''?> style="transform:translateY(2px)" required>4</td>
										<td><input type="radio" name="k4" value="3" <?=($result['k4'] == '3')?'checked':''?> style="transform:translateY(2px)" required>3</td>
										<td><input type="radio" name="k4" value="2" <?=($result['k4'] == '2')?'checked':''?> style="transform:translateY(2px)" required>2</td>
										<td><input type="radio" name="k4" value="1" <?=($result['k4'] == '1')?'checked':''?> style="transform:translateY(2px)" required>1</td>
									</tr>
									<tr>
										<td>5.</td>
										<td>Saya merasa dikendalikan oleh suami</td>
										<td><input type="radio" name="k5" value="5" <?=($result['k5'] == '5')?'checked':''?> style="transform:translateY(2px)" required>5</td>
										<td><input type="radio" name="k5" value="4" <?=($result['k5'] == '4')?'checked':''?> style="transform:translateY(2px)" required>4</td>
										<td><input type="radio" name="k5" value="3" <?=($result['k5'] == '3')?'checked':''?> style="transform:translateY(2px)" required>3</td>
										<td><input type="radio" name="k5" value="2" <?=($result['k5'] == '2')?'checked':''?> style="transform:translateY(2px)" required>2</td>
										<td><input type="radio" name="k5" value="1" <?=($result['k5'] == '1')?'checked':''?> style="transform:translateY(2px)" required>1</td>
									</tr>
									<tr>
										<td>6.</td>
										<td>Saya merasa dicintai oleh suami</td>
										<td><input type="radio" name="k6" value="5" <?=($result['k6'] == '5')?'checked':''?> style="transform:translateY(2px)" required>5</td>
										<td><input type="radio" name="k6" value="4" <?=($result['k6'] == '4')?'checked':''?> style="transform:translateY(2px)" required>4</td>
										<td><input type="radio" name="k6" value="3" <?=($result['k6'] == '3')?'checked':''?> style="transform:translateY(2px)" required>3</td>
										<td><input type="radio" name="k6" value="2" <?=($result['k6'] == '2')?'checked':''?> style="transform:translateY(2px)" required>2</td>
										<td><input type="radio" name="k6" value="1" <?=($result['k6'] == '1')?'checked':''?> style="transform:translateY(2px)" required>1</td>
									</tr>
								</tbody>
			  				</table>
                        </div>
                    </div>
                	<br><br>

				</div>
				<div class="box-footer">
					<input class="btn btn-primary" type="submit" value="Simpan"/>
					<a href="<?php echo base_url('admin/tentang'); ?>" class="btn btn-default">Kembali</a>
				</div>
				
				<?php echo form_close(); ?>
				
			</div><!-- /.box -->
		</div>
	</div>
</section>