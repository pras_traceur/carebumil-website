<section class="content-header">
	<h1>
    <?=$meta_title?>
		<small>Preview</small>
	</h1>
	<ol class="breadcrumb">
        <li><a href="<?php echo base_url('admin'); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="<?php echo base_url('admin/tentang'); ?>"><i class="fa fa-user"></i> Tentang Saya</a></li>
		<li class="active"><?=$meta_title?></li>
	</ol>
</section>
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<!-- general form elements disabled -->
			<div class="box box-warning">
				<div class="box-header with-border">
					<?php if ($this->session->flashdata('pesan')) { ?>
					<div class="alert alert-success alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<h4><i class="icon fa fa-check"></i> Alert!</h4>
						<?php echo $this->session->flashdata('pesan'); ?>
					</div>
					<?php }else if ($this->session->flashdata('error')) { ?>
                    <div class="alert alert-danger alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<h4><i class="icon fa fa-check"></i> Alert!</h4>
						<?php echo $this->session->flashdata('error'); ?>
					</div>
                    <?php } ?>
					
					<h3 class="box-title">Data Diri</h3>
				</div><!-- /.box-header -->
				
				<?php echo form_open_multipart('admin/submitdatadiri/' . $this->session->userdata('id_pengguna')); ?>
				
				<div class="box-body">
                    <div class="form-group">
                        <label class="col-sm-3 control-label" style="transform:translateY(8px)">Tanggal Lahir</label>
                        <div class="col-sm-4">
                        <div class="input-group date">
                            <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="date" name="tanggal_lahir" class="form-control" value="<?=$result['tanggal_lahir']?>" required>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <center style="transform:translateY(5px)">Usia Anda Saat ini </center>
                        </div>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" value="<?=$umur?>" disabled>
                        </div>
                    </div>
                    <br>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" style="transform:translateY(8px)">Pendidikan terakhir</label>
                        <div class="col-sm-9">
                            <?php echo form_dropdown('pendidikan', $pendidikan, $result['pendidikan'], 'class="form-control select2" id="input-status" required'); ?>
                        </div>
                    </div>
                    <br>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" style="transform:translateY(8px)">Pekerjaan</label>
                        <div class="col-sm-9">
                            <?php echo form_dropdown('pekerjaan', $pekerjaan, $result['pekerjaan'], 'class="form-control select2" id="input-status" required'); ?>
                        </div>
                    </div>
                    <br>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" style="transform:translateY(8px)">Status pernikahan</label>
                        <div class="col-sm-9">
                            <?php echo form_dropdown('pernikahan', $pernikahan, $result['pernikahan'], 'class="form-control select2" id="input-status" required'); ?>
                        </div>
                    </div>
                    <br>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" style="transform:translateY(8px)">Tipe Keluarga</label>
                        <div class="col-sm-9">
                            <?php echo form_dropdown('tipe_keluarga', $tipe_keluarga, $result['tipe_keluarga'], 'class="form-control select2" id="input-status" required'); ?>
                        </div>
                    </div>
                    <br>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Apakah penghasilan keluarga cukup untuk memenuhi kebutuhan ?</label>
                        <div class="col-sm-9">
                            <?php echo form_dropdown('penghasilan', $kecukupan, $result['penghasilan'], 'class="form-control select2" id="input-status" required'); ?>
                        </div>
                    </div>
                    
				</div><!-- /.box-body -->
				
				<div class="box-footer">
					<input class="btn btn-primary" type="submit" value="Simpan"/>
					<a href="<?php echo base_url('admin/tentang'); ?>" class="btn btn-default">Kembali</a>
				</div>
				
				<?php echo form_close(); ?>
				
			</div><!-- /.box -->
		</div>
	</div>
</section>