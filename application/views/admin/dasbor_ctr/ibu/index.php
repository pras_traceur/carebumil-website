<link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
<style>
.zoom-hover:hover{
	transform:scale(1.1)!important;
}
</style>
<section class="content-header">
	<h1>
		Dashboard
		<small>Preview</small>
	</h1>
	<ol class="breadcrumb">
		<li class="active">Dashboard</li>
	</ol>
</section>
<section class="content">
	<br><br><br>
	<center>
	<h2>Selamat Datang <b>Ibu <?=$this->session->userdata('nama_pengguna')?></b></h2><br>
	Nomer Rekam Medik - <b><?=$this->session->userdata('rekam_medik')?></b>
	<br>
	Untuk memulai Silahkan Lengkapi Menu dibawah ini dari <b>Tentang Saya</b> - <b>Test</b> - dan terakhir melihat <b>Hasil Test</b>
	</center>
	<br>
    <div class="row">
		<div class="col-md-3"></div>
		<div class="col-md-2">
			<a href="<?php echo base_url('admin/tentang'); ?>">
				<img class="zoom-hover" data-aos="fade-up" src="<?php echo base_url('assets/img/ibu/logo_tentang_saya.png'); ?>">
			</a>
		</div>
		<div class="col-md-2">
			<a href="<?php echo base_url('admin/test/'.$this->session->userdata('id_pengguna')); ?>">
				<img class="zoom-hover" data-aos="fade-up" src="<?php echo base_url('assets/img/ibu/logo_test.png'); ?>">
			</a>
		</div>
		<div class="col-md-2">
			<a href="<?php echo base_url('admin/hasil_test/'.$this->session->userdata('id_pengguna')); ?>">
				<img class="zoom-hover" data-aos="fade-up" src="<?php echo base_url('assets/img/ibu/logo_hasil_test.png'); ?>">
			</a>
		</div>
		<div class="col-md-3"></div>
    </div>
</section>
<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
<script>
  AOS.init();
</script>