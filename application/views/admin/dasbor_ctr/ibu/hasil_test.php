<script src="<?php echo base_url() ?>assets/chartjs/Chart.min.js"></script>
<script src="<?php echo base_url() ?>assets/chartjs/utils.js"></script>
<style>
canvas{
    -moz-user-select: none;
    -webkit-user-select: none;
    -ms-user-select: none;
}
</style>

<section class="content-header">
	<h1>
    <?=$meta_title?>
		<small>Preview</small>
	</h1>
	<ol class="breadcrumb">
        <li><a href="<?php echo base_url('admin'); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li><a href="<?php echo base_url('admin/test'); ?>"><i class="fa fa-dashboard"></i> Test</a></li>
		<li class="active"><?=$meta_title?></li>
	</ol>
</section>
<section class="content">

    <div class="row">
		<div class="col-md-6">
            <div class="info-box bg-green">
                <span class="info-box-icon"><i class="ion ion-ios-heart-outline"></i></span>

                <div class="info-box-content">
                <span class="info-box-text"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Hasil Test Terbaru Menyebutkan Ibu </font></font></span>
                <span class="info-box-number"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"><?=$result['hasil']?></font></font></span>

                <div class="progress">
                    <div class="progress-bar" style="width: 20%"></div>
                </div>
                <span class="progress-description"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
                        <?php
                            if($result['hasil'] == 'Berisiko mengalami depresi'){
                                echo 'Silakan lanjutkan ke sesi konsultasi langsung dengan bidan';
                            }else{
                                echo 'Silakan isi kuesioner deteksi depresi kembali di trimester selanjutnya';
                            }
                        ?>
                    </font></font></span>
                </div>
                <!-- /.info-box-content -->
            </div>
        </div>
        <div class="col-md-6">
            <div class="info-box bg-aqua">
                <span class="info-box-icon"><i class="ion-ios-chatbubble-outline"></i></span>

                <div class="info-box-content">
                <span class="info-box-text"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Score Kuisioner Terbaru</font></font></span>
                <span class="info-box-number"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"><?=$result['score']?></font></font></span>

                <div class="progress">
                    <div class="progress-bar" style="width: 40%"></div>
                </div>
                <span class="progress-description"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
                        Hasil Score Kuisioner
                    </font></font></span>
                </div>
                <!-- /.info-box-content -->
            </div>
        </div>
    </div>

    <div class="row">
		<div class="col-md-12">
			<!-- general form elements disabled -->
			<div class="box box-warning">
				<div class="box-header with-border">
					
					<h3 class="box-title">Laporan Hasil Test Ibu <?=$nama_ibu?></h3>
				</div><!-- /.box-header -->
				
				<div class="box-body">
                    <center>
                        <div style="width:75%;">
                            <canvas id="canvas"></canvas>
                        </div>
                    </center>
                </div>
            </div>
        </div>
    </div>

    <?php if($this->session->userdata('id_peran_pengguna') == '2'){ ?>
        <div class="row">
            <div class="col-md-12">
                            
                <div class="box box-success">
                    <div class="box-header with-border">
                        <h3 class="box-title">Data Test Ibu <?=$nama_ibu?></h3>
                    </div><!-- /.box-header -->
                    
                    <div class="box-body">
                        <table id="example" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th style="width:10px;">No</th>
                                    <th>Tanggal Test</th>
                                    <th>Score</th>
                                    <th>Hasil Test</th>
                                    <th style="width:150px;">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    if($resultss){
                                        $no=1;
                                        foreach($resultss as $item){
                                    ?>
                                        <tr>
                                            <td><?=$no?></td>
                                            <td><?=tanggalid($item['tanggal'])?></td>
                                            <td><?=$item['score']?></td>
                                            <td><?=$item['hasil']?></td>
                                            <td><a href="<?=base_url('admin/showtest/'.$item['id'])?>" class="btn btn-sm btn-success"><i class="glyphicon glyphicon-eye-open"></i>&nbsp;Lihat</a></td>
                                        </tr>
                                    <?php $no++;}} ?>
                            </tbody>
                        </table>
                    </div>
                    
                </div><!-- /.box -->
                
            </div>
        </div>
    <?php } ?>

    <a href="<?php echo base_url('admin'); ?>">
        <img class="zoom-hover" data-aos="fade-up" src="<?php echo base_url('assets/img/kembali.png'); ?>" width="10%">
    </a>
</section>

<script>

    function format_tanggal(tanggal){
        var mydate = new Date(tanggal);
        var month = ["Januari", "Februari", "Maret", "April", "Mei", "Juni",
        "Juli", "Agustus", "September", "Oktober", "November", "Desember"][mydate.getMonth()];
        var str = mydate.getDate()+' '+month + ' ' + mydate.getFullYear();
        return str;
    }

    var tanggal= [];
    var score = [];
    var jArray= <?php echo json_encode($results ); ?>;
    for(var i = 0;i < jArray.length; i++){
        tanggal[i] = format_tanggal(jArray[i]['tanggal']);
        score[i] = jArray[i]['score'];
    }
    
    var config = {
        type: 'line',
        data: {
            labels: tanggal,
            datasets: [{
                label: 'Hasil Test',
                backgroundColor: window.chartColors.red,
                borderColor: window.chartColors.red,
                data: score,
                fill: false,
            }]
        },
        options: {
            responsive: true,
            title: {
                display: true,
                text: 'Reporting Hasil Test Ibu <?=$nama_ibu?>'
            },
            tooltips: {
                mode: 'index',
                intersect: false,
            },
            hover: {
                mode: 'nearest',
                intersect: true
            },
            scales: {
                xAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Tanggal'
                    }
                }],
                yAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Score'
                    }
                }]
            }
        }
    };

    window.onload = function() {
        var ctx = document.getElementById('canvas').getContext('2d');
        window.myLine = new Chart(ctx, config);
    };
</script>