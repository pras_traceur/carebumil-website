<section class="content-header">
	<h1>
    <?=$meta_title?>
		<small>Preview</small>
	</h1>
	<ol class="breadcrumb">
        <li><a href="<?php echo base_url('admin'); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li class="active"><?=$meta_title?></li>
	</ol>
</section>
<section class="content">
	<div class="row">
		<div class="col-md-12">
            <!-- Data Diri -->
            <div class="box box-success">
				<div class="box-header with-border">
					<h3 class="box-title">Data Diri</h3>
				</div>
				
				<div class="box-body">
                    <div class="form-group">
                            <label class="col-sm-3 control-label" style="transform:translateY(8px)">Nama </label>
                            <div class="col-sm-9">
                                <b><?php echo $nama_ibu;?></b>
                            </div>
                        </div>
                        <br>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" style="transform:translateY(8px)">Tanggal Lahir</label>
                        <div class="col-sm-4">
                        <div class="input-group date">
                            <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="date" name="tanggal_lahir" class="form-control" value="<?=$result1['tanggal_lahir']?>" required>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <center style="transform:translateY(5px)">Usia Anda Saat ini </center>
                        </div>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" value="<?=$umur1?>" disabled>
                        </div>
                    </div>
                    <br>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" style="transform:translateY(8px)">Pendidikan terakhir</label>
                        <div class="col-sm-9">
                            <?php echo form_dropdown('pendidikan', $pendidikan, $result1['pendidikan'], 'class="form-control select2" id="input-status" required'); ?>
                        </div>
                    </div>
                    <br>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" style="transform:translateY(8px)">Pekerjaan</label>
                        <div class="col-sm-9">
                            <?php echo form_dropdown('pekerjaan', $pekerjaan, $result1['pekerjaan'], 'class="form-control select2" id="input-status" required'); ?>
                        </div>
                    </div>
                    <br>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" style="transform:translateY(8px)">Status pernikahan</label>
                        <div class="col-sm-9">
                            <?php echo form_dropdown('pernikahan', $pernikahan, $result1['pernikahan'], 'class="form-control select2" id="input-status" required'); ?>
                        </div>
                    </div>
                    <br>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" style="transform:translateY(8px)">Tipe Keluarga</label>
                        <div class="col-sm-9">
                            <?php echo form_dropdown('tipe_keluarga', $tipe_keluarga, $result1['tipe_keluarga'], 'class="form-control select2" id="input-status" required'); ?>
                        </div>
                    </div>
                    <br>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Apakah penghasilan keluarga cukup untuk memenuhi kebutuhan ?</label>
                        <div class="col-sm-9">
                            <?php echo form_dropdown('penghasilan', $kecukupan, $result1['penghasilan'], 'class="form-control select2" id="input-status" required'); ?>
                        </div>
                    </div>
                    
				</div><!-- /.box-body -->

                <div class="box-footer">
					<a href="<?php echo base_url('admin'); ?>" class="btn btn-default">Kembali</a>
				</div>
			</div>
            <!-- End Data Diri -->

            <!-- Kehamilan -->
            <div class="box box-warning">
				<div class="box-header with-border">
					<h3 class="box-title">Kehamilan ini</h3>
				</div><!-- /.box-header -->
				<div class="box-body">
                    <div class="form-group">
                        <label class="col-sm-3 control-label" style="transform:translateY(8px)">Hamil ke ?</label>
                        <?php if($result2['hamil_ke'] == 1){ ?>
							<div class="col-sm-8">
								<input type="number" name="hamil_ke" class="form-control" value="<?=$result2['hamil_ke']?>" required>
							</div>
							<div class="col-sm-1">
								<img src="<?=base_url('/assets/img/atention/atentionred.png')?>" style="width:40px;margin-top: -1px;">
							</div>
						<?php }else{ ?>
							<div class="col-sm-9">
								<input type="number" name="hamil_ke" class="form-control" value="<?=$result2['hamil_ke']?>" required>
							</div>
						<?php } ?>
                    </div>
                    <br>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" style="transform:translateY(8px)">Hari Pertama Haid Terakhir</label>
                        <div class="col-sm-4">
                            <div class="input-group date">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="date" name="tanggal_pertama_haid" class="form-control" value="<?=$result2['tanggal_pertama_haid']?>" required>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <center style="transform:translateY(5px)">Hari Perkiraan Lahir</center>
                        </div>
                        <div class="col-sm-3">
                            <input type="text" id="HPL" class="form-control" disabled>
                        </div>
                    </div>
                    <br>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" style="transform:translateY(8px)">Apakah hamil ini direncanakan ?</label>
                        <div class="col-sm-9">
                            <div class="row" style="transform:translateY(7px);">
                                <div class="col-sm-1">
                                    <input type="radio" name="hamil_direncanakan" value="ya" <?=($result2['hamil_direncanakan'] == 'ya')?'checked':''?> required>Ya
                                </div>
                                <div class="col-sm-2">
                                    <input type="radio" name="hamil_direncanakan" value="tidak" <?=($result2['hamil_direncanakan'] == 'tidak')?'checked':''?> required>Tidak
                                </div>
                                <div class="col-sm-9">
                                    <?php if($result2['hamil_direncanakan'] == 'tidak'){ ?>
										<img src="<?=base_url('/assets/img/atention/atentionred.png')?>" style="width:40px;transform:translate(-80px,-9px);">
									<?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" style="transform:translateY(8px)">Harapan keluarga untuk jenis kelamin bayi</label>
                        <div class="col-sm-9">
                            <div class="row" style="transform:translateY(7px);">
                                <div class="col-sm-1">
                                    <input type="radio" name="harapan_jenis_kelamin" value="ada" <?=($result2['harapan_jenis_kelamin'] == 'ada')?'checked':''?> required>Ada
                                </div>
                                <div class="col-sm-2">
                                    <input type="radio" name="harapan_jenis_kelamin" value="tidak" <?=($result2['harapan_jenis_kelamin'] == 'tidak')?'checked':''?> required>Tidak
                                </div>
                                <div class="col-sm-9">
									<?php if($result2['harapan_jenis_kelamin'] == 'ada'){ ?>
										<img src="<?=base_url('/assets/img/atention/atentionred.png')?>" style="width:40px;transform:translate(-80px,-9px);">
									<?php } ?>
								</div>
                            </div>
                        </div>
                    </div>
                    <br><br>
                    <div class="form-group" <?=($result2['harapan_jenis_kelamin'] == 'tidak')?'style="display:none;"':''?>>
                        <label class="col-sm-3 control-label" style="transform:translateY(8px)">Kalau ada:</label>
                        <div class="col-sm-9">
                            <input type="text" name="kalau_ada" class="form-control" value="<?=$result2['kalau_ada']?>" placeholder="Laki-laki/Perempuan">
                        </div>
                    </div>
                    <br>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" style="transform:translateY(8px)">Apakah sedang mengkonsumsi obat- obatan antidepresi ?</label>
                        <div class="col-sm-9">
                            <div class="row" style="transform:translateY(7px);">
                                <div class="col-sm-1">
                                    <input type="radio" name="antidepresi" value="ya" <?=($result2['antidepresi'] == 'ya')?'checked':''?> required>Ya
                                </div>
                                <div class="col-sm-2">
                                    <input type="radio" name="antidepresi" value="tidak" <?=($result2['antidepresi'] == 'tidak')?'checked':''?> required>Tidak
                                </div>
                                <div class="col-sm-9">
									<?php if($result2['antidepresi'] == 'ya'){ ?>
										<img src="<?=base_url('/assets/img/atention/atentionred.png')?>" style="width:40px;transform:translate(-80px,-9px);">
									<?php } ?>
								</div>
                            </div>
                        </div>
                    </div>
                    <br><br>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" style="transform:translateY(8px)">Apakah selama hamil ini mengalami keluhan/masalah ?</label>
                        <div class="col-sm-9">
                            <div class="row" style="transform:translateY(7px);">
                                <div class="col-sm-1">
                                    <input type="radio" name="keluhan" value="ya" <?=($result2['keluhan'] == 'ya')?'checked':''?> required>Ya
                                </div>
                                <div class="col-sm-2">
                                    <input type="radio" name="keluhan" value="tidak" <?=($result2['keluhan'] == 'tidak')?'checked':''?> required>Tidak
                                </div>
                                <div class="col-sm-9">
									<?php if($result2['keluhan'] == 'ya'){ ?>
										<img src="<?=base_url('/assets/img/atention/atentionred.png')?>" style="width:40px;transform:translate(-80px,-9px);">
									<?php } ?>
								</div>
                            </div>
                        </div>
                    </div>
                    <br><br>
                    <div class="form-group" <?=($result2['keluhan'] == 'tidak')?'style="display:none;"':''?>>
                        <label class="col-sm-3 control-label" style="transform:translateY(8px)">Jika Ya Sebutkan:</label>
                        <div class="col-sm-9">
                            <input type="text" name="jika_ada" class="form-control" value="<?=$result2['jika_ada']?>" placeholder="Isi Keluhan/Masalah">
                        </div>
                    </div>
                    <br>
                </div>

                <div class="box-footer">
					<a href="<?php echo base_url('admin'); ?>" class="btn btn-default">Kembali</a>
				</div>
				
			</div><!-- /.box -->
            <!-- End Kehamilan -->

            <!-- Riwayat -->
            <div class="box box-info">
				<div class="box-header with-border">
					<h3 class="box-title">Riwayat Obsetri</h3>
				</div><!-- /.box-header -->
				
				<div class="box-body">
					<div class="form-group">
                        <label class="col-sm-3 control-label" style="transform:translateY(8px)">Pernah depresi sebelumnya selama hamil/setelah persalinan ?</label>
                        <div class="col-sm-9">
                            <div class="row" style="transform:translateY(7px);">
                                <div class="col-sm-1">
                                    <input type="radio" value="ya" <?=($result3['k1'] == 'ya')?'checked':''?> required>Ya
                                </div>
                                <div class="col-sm-2">
                                    <input type="radio" value="tidak" <?=($result3['k1'] == 'tidak')?'checked':''?> required>Tidak
                                </div>
                                <div class="col-sm-9">
                                    <?php if($result3['k1'] == 'ya'){ ?>
										<img src="<?=base_url('/assets/img/atention/atentionred.png')?>" style="width:40px;transform:translate(-80px,-9px);">
									<?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br><br>
					<div class="form-group">
                        <label class="col-sm-3 control-label" style="transform:translateY(8px)">Pernah keguguran ?</label>
                        <div class="col-sm-9">
                            <div class="row" style="transform:translateY(7px);">
                                <div class="col-sm-1">
                                    <input type="radio" value="ya" <?=($result3['k2'] == 'ya')?'checked':''?> required>Ya
                                </div>
                                <div class="col-sm-2">
                                    <input type="radio" value="tidak" <?=($result3['k2'] == 'tidak')?'checked':''?> required>Tidak
                                </div>
                                <div class="col-sm-9">
                                    <?php if($result3['k2'] == 'ya'){ ?>
										<img src="<?=base_url('/assets/img/atention/atentionred.png')?>" style="width:40px;transform:translate(-80px,-9px);">
									<?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br><br>
					<div class="form-group">
                        <label class="col-sm-3 control-label" style="transform:translateY(8px)">Pernah bersalin caesar/dengan tindakan ?</label>
                        <div class="col-sm-9">
                            <div class="row" style="transform:translateY(7px);">
                                <div class="col-sm-1">
                                    <input type="radio" value="ya" <?=($result3['k3'] == 'ya')?'checked':''?> required>Ya
                                </div>
                                <div class="col-sm-2">
                                    <input type="radio" value="tidak" <?=($result3['k3'] == 'tidak')?'checked':''?> required>Tidak
                                </div>
                                <div class="col-sm-9">
                                    <?php if($result3['k3'] == 'ya'){ ?>
										<img src="<?=base_url('/assets/img/atention/atentionred.png')?>" style="width:40px;transform:translate(-80px,-9px);">
									<?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br>
					<div class="form-group" id="tindakan" <?=($result3['k3'] == 'tidak')?'style="display:none;"':''?>>
                    <br>
                        <label class="col-sm-3 control-label" style="transform:translateY(8px)">Jika ya apakah direncanakan ?</label>
                        <div class="col-sm-9">
                            <div class="row" style="transform:translateY(7px);">
                                <div class="col-sm-1">
                                    <input type="radio" value="ya" <?=($result3['k4'] == 'ya')?'checked':''?> required>Ya
                                </div>
                                <div class="col-sm-2">
                                    <input type="radio" value="tidak" <?=($result3['k4'] == 'tidak')?'checked':''?> required>Tidak
                                </div>
                                <div class="col-sm-9"></div>
                            </div>
                        </div>
                    </div>
                    <br><br>

					<div class="form-group">
                        <label class="col-sm-3 control-label" style="transform:translateY(8px)">Pernah mengalami komplikasi/masalah kehamilan/persalinan/nifas terdahulu ?</label>
                        <div class="col-sm-9">
                            <div class="row" style="transform:translateY(7px);">
                                <div class="col-sm-1">
                                    <input type="radio" value="ya" <?=($result3['k5'] == 'ya')?'checked':''?> required>Ya
                                </div>
                                <div class="col-sm-2">
                                    <input type="radio" value="tidak" <?=($result3['k5'] == 'tidak')?'checked':''?> required>Tidak
                                </div>
                                <div class="col-sm-9">
                                    <?php if($result3['k5'] == 'ya'){ ?>
										<img src="<?=base_url('/assets/img/atention/atentionred.png')?>" style="width:40px;transform:translate(-80px,-9px);">
									<?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br><br>
					<div class="form-group" <?=($result3['k5'] == 'tidak')?'style="display:none;"':''?>>
                        <label class="col-sm-3 control-label" style="transform:translateY(8px)">Jika Ya Sebutkan:</label>
                        <div class="col-sm-9">
                            <input type="text" name="jika_ya" class="form-control" value="<?=$result3['jika_ya']?>" placeholder="Isi Keluhan/Masalah">
                        </div>
                    </div>
                    <br>

				</div>

                <div class="box-footer">
					<a href="<?php echo base_url('admin'); ?>" class="btn btn-default">Kembali</a>
				</div>
			</div><!-- /.box -->
            <!-- End Riwayat -->

            <!-- Sosial -->
            <div class="box box-default">
				<div class="box-header with-border">
					
					<h3 class="box-title">Dukungan Sosial</h3>
				</div><!-- /.box-header -->
				
				<div class="box-body">
					<div class="form-group">
                        <label class="col-sm-3 control-label" style="transform:translateY(8px)">Pernahkah mengalami kekerasan dalam rumah tangga ?</label>
                        <div class="col-sm-9">
                            <div class="row" style="transform:translateY(7px);">
                                <div class="col-sm-1">
                                    <input type="radio" name="kekerasan" value="ya" <?=($result4['kekerasan'] == 'ya')?'checked':''?> required>Ya
                                </div>
                                <div class="col-sm-2">
                                    <input type="radio" name="kekerasan" value="tidak" <?=($result4['kekerasan'] == 'tidak')?'checked':''?> required>Tidak
                                </div>
                                <div class="col-sm-9">
                                    <?php if($result4['kekerasan'] == 'ya'){ ?>
										<img src="<?=base_url('/assets/img/atention/atentionred.png')?>" style="width:40px;transform:translate(-80px,-9px);">
									<?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                	<br><br>
					<div class="form-group">
                        <label class="col-sm-3 control-label" style="transform:translateY(8px)">Kuesioner dukungan sosial</label>
                        <div class="col-sm-9">
							Untuk masing-masing pernyataan, centang salah satu kotak pilihan jawaban yang paling sesuai dengan yang ibu rasakan saat ini.
							<table class="table table-bordered">
								<tbody>
									<tr>
										<th style="width: 10px">#</th>
										<th>Pernyataan</th>
										<th>Selalu</th>
										<th>Sering</th>
										<th>Kadang</th>
										<th>Jarang</th>
										<th>Tidak Pernah</th>
									</tr>
									<tr>
										<td>1.</td>
										<td>Saya memiliki teman yang sangat mendukung saya</td>
										<td><input type="radio" name="k1" value="5" <?=($result4['k1'] == '5')?'checked':''?> style="transform:translateY(2px)" required>5</td>
										<td><input type="radio" name="k1" value="4" <?=($result4['k1'] == '4')?'checked':''?> style="transform:translateY(2px)" required>4</td>
										<td><input type="radio" name="k1" value="3" <?=($result4['k1'] == '3')?'checked':''?> style="transform:translateY(2px)" required>3</td>
										<td><input type="radio" name="k1" value="2" <?=($result4['k1'] == '2')?'checked':''?> style="transform:translateY(2px)" required>2</td>
										<td><input type="radio" name="k1" value="1" <?=($result4['k1'] == '1')?'checked':''?> style="transform:translateY(2px)" required>1</td>
									</tr>
									<tr>
										<td>2.</td>
										<td>Keluarga sata selalu ada untuk saya</td>
										<td><input type="radio" name="k2" value="5" <?=($result4['k2'] == '5')?'checked':''?> style="transform:translateY(2px)" required>5</td>
										<td><input type="radio" name="k2" value="4" <?=($result4['k2'] == '4')?'checked':''?> style="transform:translateY(2px)" required>4</td>
										<td><input type="radio" name="k2" value="3" <?=($result4['k2'] == '3')?'checked':''?> style="transform:translateY(2px)" required>3</td>
										<td><input type="radio" name="k2" value="2" <?=($result4['k2'] == '2')?'checked':''?> style="transform:translateY(2px)" required>2</td>
										<td><input type="radio" name="k2" value="1" <?=($result4['k2'] == '1')?'checked':''?> style="transform:translateY(2px)" required>1</td>
									</tr>
									<tr>
										<td>3.</td>
										<td>Suami saya sangat membantu</td>
										<td><input type="radio" name="k3" value="5" <?=($result4['k3'] == '5')?'checked':''?> style="transform:translateY(2px)" required>5</td>
										<td><input type="radio" name="k3" value="4" <?=($result4['k3'] == '4')?'checked':''?> style="transform:translateY(2px)" required>4</td>
										<td><input type="radio" name="k3" value="3" <?=($result4['k3'] == '3')?'checked':''?> style="transform:translateY(2px)" required>3</td>
										<td><input type="radio" name="k3" value="2" <?=($result4['k3'] == '2')?'checked':''?> style="transform:translateY(2px)" required>2</td>
										<td><input type="radio" name="k3" value="1" <?=($result4['k3'] == '1')?'checked':''?> style="transform:translateY(2px)" required>1</td>
									</tr>
									<tr>
										<td>4.</td>
										<td>Saya memiliki konflik dengan suami</td>
										<td><input type="radio" name="k4" value="5" <?=($result4['k4'] == '5')?'checked':''?> style="transform:translateY(2px)" required>5</td>
										<td><input type="radio" name="k4" value="4" <?=($result4['k4'] == '4')?'checked':''?> style="transform:translateY(2px)" required>4</td>
										<td><input type="radio" name="k4" value="3" <?=($result4['k4'] == '3')?'checked':''?> style="transform:translateY(2px)" required>3</td>
										<td><input type="radio" name="k4" value="2" <?=($result4['k4'] == '2')?'checked':''?> style="transform:translateY(2px)" required>2</td>
										<td><input type="radio" name="k4" value="1" <?=($result4['k4'] == '1')?'checked':''?> style="transform:translateY(2px)" required>1</td>
									</tr>
									<tr>
										<td>5.</td>
										<td>Saya merasa dikendalikan oleh suami</td>
										<td><input type="radio" name="k5" value="5" <?=($result4['k5'] == '5')?'checked':''?> style="transform:translateY(2px)" required>5</td>
										<td><input type="radio" name="k5" value="4" <?=($result4['k5'] == '4')?'checked':''?> style="transform:translateY(2px)" required>4</td>
										<td><input type="radio" name="k5" value="3" <?=($result4['k5'] == '3')?'checked':''?> style="transform:translateY(2px)" required>3</td>
										<td><input type="radio" name="k5" value="2" <?=($result4['k5'] == '2')?'checked':''?> style="transform:translateY(2px)" required>2</td>
										<td><input type="radio" name="k5" value="1" <?=($result4['k5'] == '1')?'checked':''?> style="transform:translateY(2px)" required>1</td>
									</tr>
									<tr>
										<td>6.</td>
										<td>Saya merasa dicintai oleh suami</td>
										<td><input type="radio" name="k6" value="5" <?=($result4['k6'] == '5')?'checked':''?> style="transform:translateY(2px)" required>5</td>
										<td><input type="radio" name="k6" value="4" <?=($result4['k6'] == '4')?'checked':''?> style="transform:translateY(2px)" required>4</td>
										<td><input type="radio" name="k6" value="3" <?=($result4['k6'] == '3')?'checked':''?> style="transform:translateY(2px)" required>3</td>
										<td><input type="radio" name="k6" value="2" <?=($result4['k6'] == '2')?'checked':''?> style="transform:translateY(2px)" required>2</td>
										<td><input type="radio" name="k6" value="1" <?=($result4['k6'] == '1')?'checked':''?> style="transform:translateY(2px)" required>1</td>
									</tr>
								</tbody>
			  				</table>
                        </div>
                    </div>
                	<br><br>

				</div>

                <div class="box-footer">
					<a href="<?php echo base_url('admin'); ?>" class="btn btn-default">Kembali</a>
				</div>
			</div><!-- /.box -->
            <!-- End Sosial -->

            <!-- Persiapan -->
            <div class="box box-danger">
				<div class="box-header with-border">
					<h3 class="box-title">Persiapan Persalinan</h3>
				</div><!-- /.box-header -->
				
				<div class="box-body">
					<div class="form-group">
                        <label class="col-sm-3 control-label" style="transform:translateY(8px)">Selama hamil, pernahkah mengikuti kelas hamil ?</label>
                        <div class="col-sm-9">
                            <div class="row" style="transform:translateY(7px);">
                                <div class="col-sm-1">
                                    <input type="radio" name="kelas_hamil" value="pernah" <?=($result5['kelas_hamil'] == 'pernah')?'checked':''?> required>Ya
                                </div>
                                <div class="col-sm-2">
                                    <input type="radio" name="kelas_hamil" value="tidak" <?=($result5['kelas_hamil'] == 'tidak')?'checked':''?> required>Tidak
                                </div>
                                <div class="col-sm-9"></div>
                            </div>
                        </div>
                    </div>
                    <br><br>
					<div class="form-group">
                        <label class="col-sm-3 control-label" style="transform:translateY(8px)">Rencana persalinan ingin ditolong :</label>
                        <div class="col-sm-9">
                            <input type="text" name="rencana_persalinan" class="form-control" value="<?=$result5['rencana_persalinan']?>" placeholder="Dokter/Bidan/Lainnya, sebutkan" required>
                        </div>
                    </div>
                    <br>
					<div class="form-group">
                        <label class="col-sm-3 control-label" style="transform:translateY(8px)">Rencana tempat persalinan :</label>
                        <div class="col-sm-9">
                            <input type="text" name="di" class="form-control" value="<?=$result5['di']?>" placeholder="Rumah sakit/Klinik Bidan/Lainnya, sebutkan" required>
                        </div>
                    </div>
                    <br><br>
					<div class="form-group">
                        <label class="col-sm-3 control-label" style="transform:translateY(8px)">Keinginan bersalin secara :</label>
                        <div class="col-sm-9">
                            <div class="row" style="transform:translateY(7px);">
                                <div class="col-sm-1">
                                    <input type="radio" name="keinginan_bersalin" value="normal" <?=($result5['keinginan_bersalin'] == 'normal')?'checked':''?> required>Normal
                                </div>
                                <div class="col-sm-2">
                                    <input type="radio" name="keinginan_bersalin" value="caesar" <?=($result5['keinginan_bersalin'] == 'caesar')?'checked':''?> required>Caesar
                                </div>
                                <div class="col-sm-9"></div>
                            </div>
                        </div>
                    </div>
					<br><br>
					<div class="form-group">
                        <label class="col-sm-3 control-label" style="transform:translateY(8px)">Apakah ada asuransi untuk pembiayaan persalinan ?</label>
                        <div class="col-sm-9">
                            <div class="row" style="transform:translateY(7px);">
                                <div class="col-sm-1">
                                    <input type="radio" name="asuransi" value="ada" <?=($result5['asuransi'] == 'ada')?'checked':''?> required>Ada
                                </div>
                                <div class="col-sm-2">
                                    <input type="radio" name="asuransi" value="belum" <?=($result5['asuransi'] == 'belum')?'checked':''?> required>Belum Ada
                                </div>
                                <div class="col-sm-8">
                                    <?php if($result5['asuransi'] == 'belum'){ ?>
										<img src="<?=base_url('/assets/img/atention/atentionred.png')?>" style="width:40px;transform:translate(-50px,-9px);">
									<?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br><br>
					<div class="form-group">
                        <label class="col-sm-3 control-label" style="transform:translateY(8px)">Apakah sudah ada persiapan dana untuk persalinan ?</label>
                        <div class="col-sm-9">
                            <div class="row" style="transform:translateY(7px);">
							<div class="col-sm-1">
                                    <input type="radio" name="persiapan_dana" value="ada" <?=($result5['persiapan_dana'] == 'ada')?'checked':''?> required>Ada
                                </div>
                                <div class="col-sm-2">
                                    <input type="radio" name="persiapan_dana" value="belum" <?=($result5['persiapan_dana'] == 'belum')?'checked':''?> required>Belum Ada
                                </div>
                                <div class="col-sm-8">
                                    <?php if($result5['persiapan_dana'] == 'belum'){ ?>
										<img src="<?=base_url('/assets/img/atention/atentionred.png')?>" style="width:40px;transform:translate(-50px,-9px);">
									<?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br><br>
					
				</div>
                <div class="box-footer">
					<a href="<?php echo base_url('admin'); ?>" class="btn btn-default">Kembali</a>
				</div>
				
			</div><!-- /.box -->
            <!-- End Persiapan -->

        </div>
    </div>
</section>


<script type="text/javascript">

	function hplhitung(){
		var HHT   = <?=date_format(date_create($result2['tanggal_pertama_haid']),"d")?>;
		var bulan = <?=date_format(date_create($result2['tanggal_pertama_haid']),"m")?>;
		var tahun = <?=date_format(date_create($result2['tanggal_pertama_haid']),"Y")?>;

		// HPL naigale
		if(bulan >=1 && bulan <= 3){
			HHT   = +HHT +7;
			bulan = +bulan + 9;
			tahun = +tahun + 0;
		} else if (bulan >= 4 && bulan <= 12){
			HHT   = +HHT +7;
			bulan = bulan - 3;
			tahun = +tahun + 1;	
		} 

		// validasi bulan & tahun
		if(bulan == 2){
			if(tahun%2==0){
				if(HHT>=29){
					HHT   = HHT - 29;
					bulan = bulan - 1;
				} 
			} else {
				if(HHT>=28){
					HHT = HHT - 28;
					bulan = bulan - 1;
				}
			}
		}else if(bulan%2==0){
			if(bulan==8){
				if(HHT>31){
					HHT   = HHT - 31;
					bulan = +bulan + 1;
				}
			} else if(bulan>12){
				if(HHT>30){
					HHT   = HHT - 30;
					bulan = bulan - 12;
					tahun = +tahun + 1;
				}else{
					bulan = bulan - 12;
					tahun = +tahun + 1;	
				}
			}else{
				if(HHT>30){
					HHT   = HHT - 30;
					if(bulan>=12){
						bulan = bulan - 11;			
					} else {
						bulan = +bulan + 1;	
					}
				}
			}

		} else if(bulan%2!=0){
			if(bulan>12){
				if(HHT>31){
					HHT   = HHT - 31;
					bulan = bulan - 12;
					tahun = +tahun + 1;
				}else{
					bulan = bulan - 11;
					tahun = +tahun + 1;	
				}
			}else if(HHT>31){
				HHT   = HHT - 31;
				bulan = +bulan + 1;	
			}
		}
		
		// konversi bulan
		if(bulan==1){
			bulan = "Januari";
		} else if(bulan==2){
			bulan = "Februari";
		} else if (bulan==3){
			bulan = "Maret";
		} else if (bulan==4){
			bulan = "April";
		} else if (bulan==5){
			bulan = "Mei";
		} else if (bulan==6){
			bulan = "Juni";
		} else if (bulan==7){
			bulan = "Juli";
		} else if (bulan==8){
			bulan = "Agutus";
		} else if (bulan==9){
			bulan = "September";
		} else if (bulan==10){
			bulan = "Oktober";
		} else if (bulan==11){
			bulan = "November";
		} else if (bulan==12){
			bulan = "Desember";
		} 

		HPL = HHT+' - '+bulan+' - '+tahun;

		$('#HPL').val(HPL);
	};

    window.onload = function() {
        hplhitung();
    };
    
    $(function() {
        $('input[name="tanggal_pertama_haid"]').daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            minYear: 1901,
            maxYear: parseInt(moment().format('YYYY'),10)
        }, function(start, end, label) {
            var years = moment().diff(start, 'years');
        });
    });
</script>