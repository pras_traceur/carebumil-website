<section class="content-header">
	<h1>
		Dashboard
		<small>Preview</small>
	</h1>
	<ol class="breadcrumb">
		<li class="active">Dashboard</li>
	</ol>
</section>
<section class="content">
	<center>
	<h2>Selamat Datang <b>Bidan <?=$this->session->userdata('nama_pengguna')?></b></h2><br>
	Anda dapat melihat Riwayat dari Ibu Hamil melalui Tabel di Bawah ini.
	</center>
	<br>
	<div class="row">
		<div class="col-md-12">
			<!-- general form elements disabled -->
			<div class="box box-success">
				<div class="box-header with-border">
                    <?php if ($this->session->flashdata('pesan')) { ?>
					<div class="alert alert-success alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<h4><i class="icon fa fa-check"></i> Alert!</h4>
						<?php echo $this->session->flashdata('pesan'); ?>
					</div>
					<?php }else if ($this->session->flashdata('error')) { ?>
                    <div class="alert alert-danger alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<h4><i class="icon fa fa-check"></i> Alert!</h4>
						<?php echo $this->session->flashdata('error'); ?>
					</div>
                    <?php } ?>
					
					<h3 class="box-title">Data Ibu</h3>
				</div><!-- /.box-header -->
				
				<div class="box-body">
                    Tombol <button class="btn btn-sm btn-danger"><i class="glyphicon glyphicon-bullhorn"></i>&nbsp;Tata Laksana</button> akan muncul saat skor screening depresinya bernilai ≥ 13 dan berisi panduan interview pertanyaan awal yang harus ditanyakan kepada ibu hamil yang memiliki risiko depresi kehamilan.
                    <br><br>
                    <table id="example" class="table table-striped table-bordered" style="width:100%">
                        <thead>
                            <tr>
                                <th style="width:10px;">No</th>
                                <th>Nama Ibu</th>
                                <th>Rekam Medik</th>
                                <th>Tanggal Test Terbaru</th>
                                <th>Score</th>
                                <th>Hasil Test Terbaru</th>
                                <th style="width:300px;">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                if($result){
                                    $no=1;
                                    foreach($result as $items){
                                ?>
                                    <tr>
                                        <td><?=$no?></td>
                                        <td><?=$items['nama_pengguna']?></td>
                                        <td><?=$items['rekam_medik']?></td>
                                        <td><?=($items['tanggal'])?tanggalid($items['tanggal']):'Belum Melakukan Test'?></td>
                                        <td><?=($items['score'])?$items['score']:'-'?></td>
                                        <td><?=($items['hasil'])?$items['hasil']:'-'?></td>
                                        <td>
                                            <center>
                                            <?php if($items['score'] >= 13){ ?>
                                                <a href="<?=base_url('admin/tata_laksana/')?>" class="btn btn-sm btn-danger"><i class="glyphicon glyphicon-bullhorn"></i>&nbsp;Tata Laksana</a>
                                            <?php } ?>
                                            <a href="<?=base_url('admin/hasil_test/'.$items['id_pengguna'])?>" class="btn btn-sm btn-success"><i class="glyphicon glyphicon-eye-open"></i>&nbsp;Riwayat Test</a>
                                            <a href="<?=base_url('admin/detail/'.$items['id_pengguna'])?>" class="btn btn-sm btn-info"><i class="glyphicon glyphicon-info-sign"></i>&nbsp;Detail</a>
                                            </center>
                                        </td>
                                    </tr>
                                <?php $no++;}} ?>
                        </tbody>
                    </table>
                </div>
				
			</div><!-- /.box -->
		</div>
	</div>
</section>

<script>
$(document).ready(function() {
    $('#example').DataTable( {
        columnDefs: [ {
            targets: [ 0 ],
            orderData: [ 0, 1 ]
        }, {
            targets: [ 1 ],
            orderData: [ 1, 0 ]
        }]
    } );
} );
</script>