<section class="content-header">
	<h1>
    <?=$meta_title?>
		<small>Preview</small>
	</h1>
	<ol class="breadcrumb">
        <li><a href="<?php echo base_url('admin'); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li class="active"><?=$meta_title?></li>
	</ol>
</section>
<section class="content">
	<div class="row">
		<div class="col-md-12">
<pre>
Apabila skor EPDS screening ibu hamil bernilai ≥ 13, bidan harus meninjau kembali jawaban ibu hamil pada pengkajian faktor risiko depresi dan kuesi<br>oner EPDS sebagai dasar dalam melakukan intervensi konseling.<br>
Penatalaksanaan depresi antenatal dapat dilakukan dengan:
1. Tatalaksana Psikologis
   Intervensi psikologis diberikan kepada keluarga/suami ibu hamil dengan melakukan konseling untuk meningkatkan pengetahuan dan pemahaman tentang <br>   gangguan kesehatan mental, mengurangi stigmatisasi, dan membekali keluarga dengan keterampilan sederhana untuk menolong ibu hamil yang mengalami <br>   masalah kesehatan mental.
2. Tatalaksana Psikososial
   Pendekatan psikososial dilakukan dengan memberikan dukungan psikologis dan sosial pada ibu hamil yang mengalami berbagai macam perubahan fisik dan<br>   psikologis selama hamil dan perubahan-perubahan tersebut dipengaruhi oleh interaksi wanita hamil dengan lingkungan sosial.
<br>   Pendekatan bidan dalam memberikan konseling terkait masalah psikososial ibu hamil antara lain:
    1. Menjalin komunikasi yang baik dengan ibu hamil sehingga ibu merasa diperhatikan dan dapat terbuka dalam mengkomunikasikan permasalahan yang<br>       sedang dialaminya.
    2. Memberitahu tahu ibu tentang perubahan-perubahan psikologis yang mungkin terjadi selama kehamilan.
    3. Memberitahu ibu dampak masalah psikologis yang dihadapi oleh ibu terhadap janin yang dikandungnya.
    4. Menganjurkan ibu untuk mencari pihak ketiga sebagai penengah apabila sedang ada permasalahan dengan orang lain.
    5. Menganjurkan ibu untuk mengkomunikasikan permasalahan yang dialami dalam rumah tangga nya dengan suami secara baik dan terbuka,sehingga<br>       diharapkan suami dapat memahami dan memberikan dukungan psikologis terhadap kehamilan ibu.
    6. Menganjurkan ibu untuk mengikutsertakan suami ketika melakukan kunjungan antenatal sehingga bidan lebih mudah dalam menyampaikan konseling.
    7. Memberikan pendampingan psikologis dan pelayanan pengobatan fisik korban jika diketahui ibu mengalami kekerasan fisik.
    8. Jika terjadi permasalahan yang lebih berat pada ibu sampai menyebabkan depresi berat pada ibu segera fasilitasi ibu untuk berkonsultasi<br>       dengan psikiater, karena sudah tidak menjadi wewenang seorang bidan lagi.

3. Tatalaksana Psikoedukasi
   Tatalaksana psikoedukasi diberikan untuk memperkuat koping ibu dalam menghadapi stress atau gangguan psikologis dengan melibatkan partisipasi <br>   aktif keluarga. Pendekatan yang diberikan oleh bidan dalam bentuk pendidikan dan latihan agar ibu memiliki pengetahuan dan keterampilan dalam<br>   mengembangkan koping efektif. Media yang digunakan dapat berbentuk booklet. Metode penyampaian dapat berupa eksplorasi, asesmen, diskusi, bermain<br>   peran, dan demonstrasi.
4. Tatalaksana Farmakologi
   Tatalaksana farmakologi diberikan jika risiko depresi ibu hamil tidak dapat ditangani dengan tatalaksana psikologis, psikososial, dan psikoedukasi.<br>   Bidan melakukan kolaborasi/rujukan ke psikolog/psikiater.
</pre>
<a href="<?php echo base_url('admin'); ?>">
        <img class="zoom-hover" data-aos="fade-up" src="<?php echo base_url('assets/img/kembali.png'); ?>" width="10%">
    </a>
		</div>
	</div>
</section>