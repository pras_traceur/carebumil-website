<header class="main-header">
        <!-- Logo -->
			<a href="<?php echo base_url(); ?>" class="logo">
			<!-- mini logo for sidebar mini 50x50 pixels -->
			<span class="logo-mini"><img src="<?php echo base_url('assets/img/logobumil.png'); ?>" style="width:30px;border-radius: 50%;"/></span>
			<!-- logo for regular state and mobile devices -->
			<span class="logo-lg"><b><?php echo pengaturan()->WEBSITE_NAMA; ?></b></span>
		</a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
						<span class="logo-lg"></span>
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
			  <!-- User Account Menu -->
			  <li class="dropdown user user-menu">
				<!-- Menu Toggle Button -->
				<a href="#" class="dropdown-toggle" data-toggle="dropdown">
				  <!-- The user image in the navbar-->
				  <img src="<?php echo $this->simpleimage->resize_image($this->session->userdata('foto'), 'pengguna', 200); ?>" class="user-image" alt="User Image">
				  <!-- hidden-xs hides the username on small devices so only the image appears. -->
				  <span class="hidden-xs"><?php echo $this->session->userdata('nama_pengguna'); ?></span>
				</a>
				<ul class="dropdown-menu">
				  <!-- The user image in the menu -->
				  <li class="user-header">
					<img src="<?php echo $this->simpleimage->resize_image($this->session->userdata('foto'), 'pengguna', 200); ?>" class="img-circle text-center" alt="User Image">
					<p>
					  <?php echo $this->session->userdata('nama_pengguna'); ?> - <?php echo $this->session->userdata('nama_peran_pengguna'); ?>
					  <small>Member since <?php echo $this->session->userdata('tanggal_buat'); ?></small>
					</p>
				  </li>
				  <!-- Menu Footer-->
				  <li class="user-footer">
					<div class="pull-left">
					  <a href="<?php echo base_url('admin/pengguna/ubah/' . $this->session->userdata('id_pengguna')); ?>" class="btn btn-default btn-flat">Profile</a>
					</div>
					<div class="pull-right">
					  <a href="<?php echo base_url('admin/login/logout'); ?>" class="btn btn-default btn-flat">Sign out</a>
					</div>
				  </li>
				</ul>
			  </li>
              <!-- Control Sidebar Toggle Button -->
            </ul>
          </div>
		</nav>
      </header>