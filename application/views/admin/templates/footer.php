<footer class="main-footer">
	<div class="container">
	  <div class="pull-right hidden-xs">
		<?php if(pengaturan()->WEBISTE_VERSI){ ?>
			<b>Version</b> <?php echo pengaturan()->WEBISTE_VERSI; ?>
		<?php } ?>
	  </div>
		<?php if(pengaturan()->WEBSITE_FOOTER){ ?>
			<?php echo pengaturan()->WEBSITE_FOOTER; ?>
		<?php } ?>
	</div><!-- /.container -->
</footer>
