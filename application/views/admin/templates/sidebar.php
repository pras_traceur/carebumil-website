<aside class="main-sidebar">
	<section class="sidebar">
		<!-- Sidebar user panel -->
		<div class="user-panel">
			<div class="pull-left image">
				<img src="<?php echo $this->simpleimage->resize_image($this->session->userdata('foto'), 'pengguna', 200); ?>" class="img-circle" alt="User Image">
			</div>
			<div class="pull-left info">
				<p><?php echo $this->session->userdata('nama_pengguna'); ?></p>
				<a href="#"><i class="fa fa-circle text-success"></i> Online</a>
			</div>
		</div>
		<ul class="sidebar-menu">
			<li class="header">MENU</li>
			<?php if(permission('DasborMengakses')) { ?>
			<li class="<?php if (active_menu('')) { echo 'active'; } ?>"><a href="<?php echo base_url('admin'); ?>"><i class="fa fa-home"></i> <span>Dashboard</span></a></li>
			<?php } ?>
			<?php if(permission('PenggunaMengakses,PeranPenggunaMengakses,PengaturanMengakses')) { ?>
			<li class="<?php if (active_menu('pengguna,peran-pengguna,pengaturan')) { echo 'active'; } ?>">
				<a href="#"><i class="fa fa-cog"></i> Pengaturan <i class="fa fa-angle-left pull-right"></i></a>
				<ul class="treeview-menu">
					<?php if(permission('PenggunaMengakses')) { ?>
					<li class="<?php if (active_menu('pengguna')) { echo 'active'; } ?>"><a href="<?php echo base_url('admin/pengguna'); ?>"><i class="fa fa-user"></i> Pengguna</a></li>
					<?php } ?>
					<?php if(permission('PeranPenggunaMengakses')) { ?>
					<li class="<?php if (active_menu('peran-pengguna')) { echo 'active'; } ?>"><a href="<?php echo base_url('admin/peran-pengguna'); ?>"><i class="fa fa-cogs"></i> Peran Pengguna</a></li>
					<?php } ?>
					<?php if(permission('PengaturanMengakses')) { ?>
					<li class="<?php if (active_menu('pengaturan')) { echo 'active'; } ?>"><a href="<?php echo base_url('admin/pengaturan'); ?>"><i class="fa fa-wrench"></i> <span>Website</span></a></li>
					<?php } ?>
					<?php if(permission('ResetPasswordMengakses')) { ?>
					<li class="<?php if (active_menu('reset-password')) { echo 'active'; } ?>"><a href="<?php echo base_url('admin/reset-password'); ?>"><i class="fa fa-cog"></i> <span>Reset Password</span></a></li>
					<?php } ?>
				</ul>
			</li>
			<?php } ?>
		</ul>
	</section>
</aside>