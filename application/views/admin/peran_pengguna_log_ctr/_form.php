<?php if (validation_errors()): ?>
	<div class="alert alert-warning alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		<h4><i class="icon fa fa-warning"></i> Alert!</h4>
		Pastikan form input yang wajib diisi telah terisi semuanya.
	</div>
<?php endif; ?>
<div class="row">
	<div class="col-md-6">
		<div class="form-group">
			<label>Label :</label>
			<input type="text" class="form-control" name="label" placeholder="Enter ..." value="<?php echo set_value('label', ($item) ? $item['label_peran_pengguna_log'] : ''); ?>">
			<div class="text-orange"><?php echo form_error('label'); ?></div>
		</div>
		<div class="form-group">
			<label>Deskripsi :</label>
			<textarea class="form-control" name="deskripsi" rows="20" placeholder="Enter ..."><?php echo set_value('deskripsi', ($item) ? $item['deskripsi'] : ''); ?></textarea>
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group">
			<label>Tanggal Mulai :</label>
			<input type="text" name="tanggal_mulai"  class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask placeholder="Enter ..."  value="<?php echo set_value('tanggal_mulai', ($item) ? $item['tanggal_mulai'] : ''); ?>">
			<div class="text-orange"><?php echo form_error('tanggal_mulai'); ?></div>
		</div>
		<div class="form-group">
			<label>Tanggal Selesai :</label>
			<input type="text" name="tanggal_selesai"  class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask placeholder="Enter ..."  value="<?php echo set_value('tanggal_selesai', ($item) ? $item['tanggal_selesai'] : ''); ?>">
			<div class="text-orange"><?php echo form_error('tanggal_selesai'); ?></div>
		</div>
		<?php if(permission('PeranPenggunaLogFlag')){ ?>
		<div class="form-group">
			<label>Flag <span class="text-red">*</span> :</label>
			<?php echo form_dropdown('flag', $flag_options, set_value('flag', ($item) ? $item['flag'] : ''), 'class="form-control select2" id="input-flag"'); ?>
			<div class="text-orange"><?php echo form_error('flag'); ?></div>
		</div>
		<?php } ?>
	</div>
</div>