<section class="content-header">
	<h1>
		Form Log Peran Pengguna
		<small>Preview</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="<?php echo base_url('admin'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
		<li><a href="<?php echo base_url('admin/log-peran-penggun?id_pengguna={id_pengguna}'); ?>">Daftar Log Peran Pengguna</a></li>
		<li class="active">Form Log Peran Penggunai</li>
	</ol>
</section>
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<!-- general form elements disabled -->
			<div class="box box-warning">
				<div class="box-header with-border">
					<?php if ($this->session->flashdata('pesan')) { ?>
					<div class="alert alert-success alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<h4><i class="icon fa fa-check"></i> Alert!</h4>
						<?php echo $this->session->flashdata('pesan'); ?>
					</div>
					<?php } ?>
					
					<h3 class="box-title">Form Log Peran Penggunai</h3>
				</div><!-- /.box-header -->
				
				<?php echo form_open_multipart('admin/log-peran-penggun/ubah/' . $item['id_peran_pengguna_log'] . '?id_pengguna={id_pengguna}'); ?>
					
				<div class="box-body">
					<?php $this->view('admin/peran_pengguna_log_ctr/_form'); ?>
				</div><!-- /.box-body -->
				
				<div class="box-footer">
					<input class="btn btn-primary" type="submit" value="Ubah"/>
					<a href="<?php echo URLBACK . '/log-peran-penggun?id_pengguna={id_pengguna}'; ?>" class="btn btn-default">Kembali ke Daftar</a>
				</div>
			
				<?php echo form_close(); ?>
			</div><!-- /.box -->
		</div>
	</div>
</section>