<section class="content-header">
	<h1>
		Pengaturan
		<small>Preview</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="<?php echo base_url('admin'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active">Pengaturan</li>
	</ol>
</section>
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box">
				<div class="box-body">
					<?php if ($this->session->flashdata('pesan')) { ?>
					<div class="alert alert-success alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<h4><i class="icon fa fa-check"></i> Alert!</h4>
						<?php echo $this->session->flashdata('pesan'); ?>
					</div>
					<?php } ?>
                    <?php echo form_open_multipart('admin/pengaturan', 'id="form-pengaturan" method="POST"'); ?>

                    {get_pengaturan}
                        {pengaturan_tabs_opened}
                        <tr class="prop">
                            <td width="23%" valign="top"><label for="{pengaturan_field}">{pengaturan_label} </label></td>
                            <td valign="top">
                                {pengaturan_input}
                                <span class="error"></span>
                            </td>
                        </tr>
                        {pengaturan_tabs_closed}
                    {/get_pengaturan}

                    <?php echo form_close(); ?>
				</div><!-- /.box-body -->
			</div>
			<div>
                <?php if(permission('PengaturanEdit')) { ?>
				<a class="btn btn-primary" href="#" id="add-pengaturan">Ubah</a>
				<?php } ?>
				<?php if(permission('PengaturanEkspor')) { ?>
				<button class="btn btn-success"><i class="fa fa-download"></i> Download</button>
				<?php } ?>
			</div>
		</div>
	</div>
</section>

<script>
    $(document).ready(function() {
        $("#add-pengaturan").click(function(){
            $("#form-pengaturan").submit();
            return false;
        });
    });

    /*
    $('#form-pengaturan').submit(function(e){
		e.preventDefault();
		$.ajax({
            url : "<?php echo site_url('admin//pengaturan_ctr')?>",
            type: "POST",
            data: $('#form-pengaturan').serialize(),
            success: function(data)
            {
				if(data.status == false){	
                    alert('error_message');
				}
				else{
                    alert('success_message');
				}
            },
			error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error adding / update data');
            }
        });
		return false;
    });
    */
</script>