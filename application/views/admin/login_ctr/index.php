<!DOCTYPE html>
<html class="loading" lang="en" xmlns:fb="http://www.facebook.com/2008/fbml">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="icon" href="">

	<title>{meta_title} | <?php echo pengaturan()->WEBSITE_NAMA; ?></title>

	<!-- Bootstrap core CSS -->
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/datatables/css/dataTables.bootstrap.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
	<!-- Ionicons -->
	<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">

	<link rel="stylesheet" href="<?php echo base_url('assets/jquery/jquery-ui.css'); ?>">

	<!-- Select2 -->
	<link rel="stylesheet" href="<?php echo base_url('assets/plugins/select2/select2.min.css'); ?>">

	<!-- Theme style -->
	<link rel="stylesheet" href="<?php echo base_url('assets/dist/css/AdminLTE.min.css'); ?>">
	<!-- AdminLTE Skins. Choose a skin from the css/skins folder instead of downloading all of them to reduce the load. -->
	<link rel="stylesheet" href="<?php echo base_url('assets/dist/css/skins/_all-skins.min.css'); ?>">

	<style>
		.table caption{ margin-bottom: 10px; }
		th.dt-center, td.dt-center { text-align: center; }
		.title_field_edit, .title_field_delete{ width: 50px; }
		.title_field_id{ width: 20px; }
		.text-orange{ color: orange; }
		body {
			background-image: url("<?php echo base_url('uploads/background.png'); ?>");
			background-repeat: no-repeat;
			background-attachment: fixed;
			background-position: center;
			background-size: 100% 100%;
		}
	</style>

	<!-- jQuery 2.1.4 -->
	<script src="<?php echo base_url('assets/plugins/jQuery/jQuery-2.1.4.min.js')?>"></script>
	<script src="<?php echo base_url('assets/jquery/jquery-ui.js') ?>"></script>

	<!-- Bootstrap 3.3.5 -->
	<script src="<?php echo base_url('assets/bootstrap/js/bootstrap.min.js')?>"></script>
	<script src="<?php echo base_url('assets/datatables/js/jquery.dataTables.min.js')?>"></script>
	<script src="<?php echo base_url('assets/datatables/js/dataTables.bootstrap.js')?>"></script>

	<!-- Select2 -->
	<script src="<?php echo base_url('assets/plugins/select2/select2.full.min.js') ?>"></script>

	<!-- InputMask -->
	<script src="<?php echo base_url('assets/plugins/input-mask/jquery.inputmask.js') ?>"></script>
	<script src="<?php echo base_url('assets/plugins/input-mask/jquery.inputmask.date.extensions.js') ?>"></script>
	<script src="<?php echo base_url('assets/plugins/input-mask/jquery.inputmask.extensions.js') ?>"></script>

	<!-- SlimScroll -->
	<script src="<?php echo base_url('assets/plugins/slimScroll/jquery.slimscroll.min.js')?>"></script>

	<!-- FastClick -->
	<script src="<?php echo base_url('assets/plugins/fastclick/fastclick.min.js')?>"></script>

	<!-- AdminLTE App -->
	<script src="<?php echo base_url('assets/dist/js/app.min.js')?>"></script>

	<!-- AdminLTE for demo purposes -->
	<script src="<?php echo base_url('assets/dist/js/demo.js')?>"></script>

	<style>
		.btn-primary {
			color: #fff;
			background-color: #098E65;
			border-color: #046446;
		}
		.btn-primary:focus,
		.btn-primary.focus {
			color: #fff;
			background-color: #046446;
			border-color: #046446;
		}
		.btn-primary:hover {
			color: #fff;
			background-color: #046446;
			border-color: #046446;
		}
	</style>
</head>
<?php /* <body class="hold-transition login-page" style="background-image:url('<?php echo base_url('assets/img/bg.jpg'); ?>');background-repeat: no-repeat;background-attachment: fixed;background-position: center;background-size: cover;"> */ ?>
<body style="background-color: black;">
<div class="login-box">
	<div class="login-box-body" style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);border-radius: 5px !important;">
		<div class="login-logo">
			<a href="#"><img class="img img-responsive" style="padding-bottom:7px;margin:auto;" src="<?php echo base_url('assets/img/logo.png'); ?>" alt=" <?php echo pengaturan()->WEBSITE_NAMA; ?>"></a>
		</div>

		<p class="login-box-msg">Masukan <?php echo (empty($this->session->userdata('username_logged_in')))? "Email" : "password"; ?> untuk memulai sesi Anda</p>
		
		<?php if ($this->session->flashdata('pesan')) { ?>
		<div class="alert alert-success alert-dismissable">
			<h4><i class="icon fa fa-check"></i> Warning !</h4>
			<?php echo $this->session->flashdata('pesan'); ?>
		</div>
		<?php } ?>
		<?php if ($this->session->flashdata('error')) { ?>
		<div class="alert alert-danger alert-dismissable">
			<h4><i class="icon fa fa-check"></i> Warning !</h4>
			<?php echo $this->session->flashdata('error'); ?>
		</div>
		<?php } ?>

		<?php echo form_open('admin/login'); ?>
		<?php if($this->session->userdata('username_logged_in')){ ?>
			<div class="form-group has-feedback text-center">
				<!--
				<div><img style="border: 3px solid rgba(255, 255, 255, 0.2); height: 120px; width: 120px; z-index: 5; margin-bottom: 1em;" src="<?php echo $this->simpleimage->resize_image($this->session->userdata('foto'), 'pengguna', 200); ?>" class="img-circle" alt="<?php echo $this->session->userdata('nama_pengguna'); ?>"></div>
				-->
				<div><b style="color:#26AEB2;"><?php echo $this->session->userdata('nama_pengguna'); ?></b></div>
			</div>
			<div class="form-group has-feedback">
				<input type="password" class="form-control" name="password" placeholder="Password" value="">
				<span class="glyphicon glyphicon-lock form-control-feedback"></span>
				<div class="text-red"><?php echo form_error('password'); ?></div>
			</div>
		<?php }else{ ?>
			<div class="form-group has-feedback">
				<input type="text" class="form-control" name="username" placeholder="Email" value="">
				<span class="glyphicon glyphicon-user form-control-feedback"></span>
				<div class="text-red"><?php echo form_error('username'); ?></div>
			</div>
		<?php } ?>
		<div class="row">
			<div class="col-xs-12 text-right">
				<div class="btn-group btn-group-lg">
					<?php if($this->session->userdata('username_logged_in')){ ?>
						<button type="submit" value="true" name="ubahuser" class="btn btn-danger">Kembali</button>
						<button type="submit" class="btn btn-danger">Masuk</button>
					<?php }else{ ?>
						<a href="<?=base_url('admin/daftar/')?>" class="btn btn-info">Daftar</a>
						<button type="submit" class="btn btn-danger">Masuk</button>
					<?php } ?>
				</div>
			</div>
		</div>
		<?php echo form_close(); ?>
		<br>
		<div><input type="checkbox"> Ingat Saya</div>
		<!--<div><a href="<?php echo base_url('admin/login/lupa_password'); ?>"> Lupa Password ? </a></div>-->
	</div>
</div>
</html>