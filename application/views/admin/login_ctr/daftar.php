<!DOCTYPE html>
<html class="loading" lang="en" xmlns:fb="http://www.facebook.com/2008/fbml">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="icon" href="">

	<title>Daftar | <?php echo pengaturan()->WEBSITE_NAMA; ?></title>

	<!-- Bootstrap core CSS -->
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/datatables/css/dataTables.bootstrap.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
	<!-- Ionicons -->
	<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">

	<link rel="stylesheet" href="<?php echo base_url('assets/jquery/jquery-ui.css'); ?>">

	<!-- Select2 -->
	<link rel="stylesheet" href="<?php echo base_url('assets/plugins/select2/select2.min.css'); ?>">

	<!-- Theme style -->
	<link rel="stylesheet" href="<?php echo base_url('assets/dist/css/AdminLTE.min.css'); ?>">
	<!-- AdminLTE Skins. Choose a skin from the css/skins folder instead of downloading all of them to reduce the load. -->
	<link rel="stylesheet" href="<?php echo base_url('assets/dist/css/skins/_all-skins.min.css'); ?>">

	<style>
		.table caption{ margin-bottom: 10px; }
		th.dt-center, td.dt-center { text-align: center; }
		.title_field_edit, .title_field_delete{ width: 50px; }
		.title_field_id{ width: 20px; }
		.text-orange{ color: orange; }
		body {
			background-image: url("<?php echo base_url('uploads/background.png'); ?>");
			background-repeat: no-repeat;
			background-attachment: fixed;
			background-position: center;
			background-size: 100% 100%;
		}
	</style>

	<!-- jQuery 2.1.4 -->
	<script src="<?php echo base_url('assets/plugins/jQuery/jQuery-2.1.4.min.js')?>"></script>
	<script src="<?php echo base_url('assets/jquery/jquery-ui.js') ?>"></script>

	<!-- Bootstrap 3.3.5 -->
	<script src="<?php echo base_url('assets/bootstrap/js/bootstrap.min.js')?>"></script>
	<script src="<?php echo base_url('assets/datatables/js/jquery.dataTables.min.js')?>"></script>
	<script src="<?php echo base_url('assets/datatables/js/dataTables.bootstrap.js')?>"></script>

	<!-- Select2 -->
	<script src="<?php echo base_url('assets/plugins/select2/select2.full.min.js') ?>"></script>

	<!-- InputMask -->
	<script src="<?php echo base_url('assets/plugins/input-mask/jquery.inputmask.js') ?>"></script>
	<script src="<?php echo base_url('assets/plugins/input-mask/jquery.inputmask.date.extensions.js') ?>"></script>
	<script src="<?php echo base_url('assets/plugins/input-mask/jquery.inputmask.extensions.js') ?>"></script>

	<!-- SlimScroll -->
	<script src="<?php echo base_url('assets/plugins/slimScroll/jquery.slimscroll.min.js')?>"></script>

	<!-- FastClick -->
	<script src="<?php echo base_url('assets/plugins/fastclick/fastclick.min.js')?>"></script>

	<!-- AdminLTE App -->
	<script src="<?php echo base_url('assets/dist/js/app.min.js')?>"></script>

	<!-- AdminLTE for demo purposes -->
	<script src="<?php echo base_url('assets/dist/js/demo.js')?>"></script>

	<style>
		.btn-primary {
			color: #fff;
			background-color: #098E65;
			border-color: #046446;
		}
		.btn-primary:focus,
		.btn-primary.focus {
			color: #fff;
			background-color: #046446;
			border-color: #046446;
		}
		.btn-primary:hover {
			color: #fff;
			background-color: #046446;
			border-color: #046446;
		}
	</style>
</head>
<?php /* <body class="hold-transition login-page" style="background-image:url('<?php echo base_url('assets/img/bg.jpg'); ?>');background-repeat: no-repeat;background-attachment: fixed;background-position: center;background-size: cover;"> */ ?>
<body style="background-color: black;">
<div class="login-box">
	<div class="login-box-body" style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);border-radius: 5px !important;">
		<div class="login-logo">
			Pendaftaran User Baru
		</div>

		<p class="login-box-msg">Selamat Datang di Carebumil.com silahkan mendaftar terlebih dahulu</p>
		<?php echo form_open('admin/submitdaftar'); ?>
        <div class="form-group">
			<input type="text" class="form-control" name="nama_pengguna" placeholder="Nama" value="">
		</div>
        <div class="form-group">
			<input type="text" class="form-control" name="username" placeholder="Email" value="">
		</div>
        <div class="form-group">
			<input type="text" class="form-control" name="rekam_medik" placeholder="Nomer Rekam Medik" value="">
		</div>
        <div class="form-group">
			<input type="password" class="form-control" name="password" placeholder="Password" value="">
		</div>
        <div class="form-group">
            <a href="<?=base_url('admin/login')?>" class="btn btn-sm btn-danger"><i class="glyphicon glyphicon-triangle-left"></i>&nbsp;Kembali</a>
            <button type="submit" class="btn btn-sm btn-success"><i class="glyphicon glyphicon-floppy-disk"></i>&nbsp;Daftar</button>
		</div>
        <?php echo form_close(); ?>
	</div>
</div>
</html>