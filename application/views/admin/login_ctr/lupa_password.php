<!DOCTYPE html>
<html class="loading" lang="en" xmlns:fb="http://www.facebook.com/2008/fbml">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="icon" href="">

  <title>Lupa Password | <?php echo pengaturan()->WEBSITE_NAMA; ?></title>

  <!-- Bootstrap core CSS -->
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/datatables/css/dataTables.bootstrap.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">

  <link rel="stylesheet" href="<?php echo base_url('assets/jquery/jquery-ui.css'); ?>">

  <!-- Select2 -->
  <link rel="stylesheet" href="<?php echo base_url('assets/plugins/select2/select2.min.css'); ?>">

  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url('assets/dist/css/AdminLTE.min.css'); ?>">
  <!-- AdminLTE Skins. Choose a skin from the css/skins folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url('assets/dist/css/skins/_all-skins.min.css'); ?>">

  <style>
    .table caption{ margin-bottom: 10px; }
    th.dt-center, td.dt-center { text-align: center; }
    .title_field_edit, .title_field_delete{ width: 50px; }
    .title_field_id{ width: 20px; }
    .text-orange{ color: orange; }
    body {
      background-image: url("<?php echo base_url('uploads/background.jpg'); ?>");
      background-repeat: no-repeat;
      background-attachment: fixed;
      background-position: center;
    }
  </style>

  <!-- jQuery 2.1.4 -->
  <script src="<?php echo base_url('assets/plugins/jQuery/jQuery-2.1.4.min.js')?>"></script>
  <script src="<?php echo base_url('assets/jquery/jquery-ui.js') ?>"></script>

  <!-- Bootstrap 3.3.5 -->
  <script src="<?php echo base_url('assets/bootstrap/js/bootstrap.min.js')?>"></script>
  <script src="<?php echo base_url('assets/datatables/js/jquery.dataTables.min.js')?>"></script>
  <script src="<?php echo base_url('assets/datatables/js/dataTables.bootstrap.js')?>"></script>

  <!-- Select2 -->
  <script src="<?php echo base_url('assets/plugins/select2/select2.full.min.js') ?>"></script>

  <!-- InputMask -->
  <script src="<?php echo base_url('assets/plugins/input-mask/jquery.inputmask.js') ?>"></script>
  <script src="<?php echo base_url('assets/plugins/input-mask/jquery.inputmask.date.extensions.js') ?>"></script>
  <script src="<?php echo base_url('assets/plugins/input-mask/jquery.inputmask.extensions.js') ?>"></script>

  <!-- SlimScroll -->
  <script src="<?php echo base_url('assets/plugins/slimScroll/jquery.slimscroll.min.js')?>"></script>

  <!-- FastClick -->
  <script src="<?php echo base_url('assets/plugins/fastclick/fastclick.min.js')?>"></script>

  <!-- AdminLTE App -->
  <script src="<?php echo base_url('assets/dist/js/app.min.js')?>"></script>

  <!-- AdminLTE for demo purposes -->
  <script src="<?php echo base_url('assets/dist/js/demo.js')?>"></script>
</head>
<?php /* <body class="hold-transition login-page" style="background-image:url('<?php echo base_url('assets/img/bg.jpg'); ?>');background-repeat: no-repeat;background-attachment: fixed;background-position: center;background-size: cover;"> */ ?>
<body style="background-color: black;">
<div class="login-box">
  <div class="login-box-body">
    <div class="login-logo">
      <a href="#"><img class="img img-responsive" style="padding-bottom:7px;margin:auto;" src="<?php echo base_url('assets/img/logo.png'); ?>" alt=" <?php echo pengaturan()->WEBSITE_NAMA; ?>"></a>
    </div>

    <p class="login-box-msg">Masukan Email Anda Untuk Memulai Proses Reset Password </p>
    
    <?php if ($this->session->flashdata('pesan')) { ?>
    <div class="alert alert-success alert-dismissable">
      <h4><i class="icon fa fa-check"></i> Warning !</h4>
      <?php echo $this->session->flashdata('pesan'); ?>
    </div>
    <?php } ?>
    <?php echo form_open('admin/login/lupa_password'); ?>
      <div class="form-group has-feedback">
        <input type="email" class="form-control" name="email" placeholder="Email" value="">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        <div class="text-red"><?php echo form_error('username'); ?></div>
      </div>
    <div class="row">
      <div class="col-xs-12 text-center">
        <div class="btn-group btn-group-lg">
          <button type="submit" class="btn btn-primary"><i class="fa fa-send"></i> Kirim Email</button>
        </div>
      </div>
    </div>
    <?php echo form_close(); ?>
    <br>
  </div>
</div>
</html>