<?php if (validation_errors()): ?>
	<div class="alert alert-warning alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		<h4><i class="icon fa fa-warning"></i> Alert!</h4>
		Pastikan form input yang wajib diisi telah terisi semuanya.
	</div>
<?php endif; ?>
<div class='row'>
	<div class="col-md-6">
		<div class="form-group">
		<?php if ($item['foto']) { ?>
			<?php
				$image = $this->simpleimage->resize_image($item['foto'], 'pengguna', 200);
			?>
			
			<p><img class="mg-responsive" src="<?php echo $image; ?>" alt="<?php echo $item['nama_pengguna']; ?>" width="200px"></p>
			<p><a href="<?php echo base_url(); ?>uploads/pengguna/<?php echo $item['foto']; ?>" target="_blank"><?php echo $item['foto']; ?></a></p>
			<?php if($this->uri->segment(3)=='ubah') { ?>
			<p><a class="btn btn-danger btn-sm" href="<?php echo URLBACK; ?>/pengguna_ctr/hapus_foto/<?php echo $item['id_pengguna'] ?>">Hapus</a></p>
			<?php } ?>
		<?php } else { ?>
			<label for="image">Foto </label>
			<input type="file" name="userfile" size="20" />
			<small style="color:red">*.jpeg,*.jpg,*.png (Max 2MB)</small>		
		<?php } ?>
			<div class="text-orange"><?php echo form_error('userfile'); ?></div>
		</div>
		<div class="form-group">
			<label>Nama Pengguna <span class="text-red">*</span> :</label>
			<input type="text" class="form-control" name="nama_pengguna" placeholder="Enter ..." value="<?php echo set_value('nama_pengguna', ($item) ? $item['nama_pengguna'] : ''); ?>">
			<div class="text-orange"><?php echo form_error('nama_pengguna'); ?></div>
		</div>
		<div class="form-group">
			<label>Username <span class="text-red">*</span> :</label>
			<input type="text" name="username" class="form-control" placeholder="Enter ..." value="<?php echo set_value('username', ($item) ? $item['username'] : ''); ?>">
			<div class="text-orange"><?php echo form_error('username'); ?></div>
		</div>
		<div class="form-group">
			<label>Email <span class="text-red">*</span> :</label>
			<input type="email" name="email_pengguna"  class="form-control" placeholder="Enter ..." value="<?php echo set_value('email_pengguna', ($item) ? $item['email_pengguna'] : ''); ?>">
			<div class="text-orange"><?php echo form_error('email_pengguna'); ?></div>
		</div>
		<!--
		<div class="form-group">
			<label>Jabatan :</label>
			<?php echo form_dropdown('jabatan', $jabatan_options, set_value('jabatan', ($item) ? $item['jabatan'] : ''), 'class="form-control select2" id="input-jabatan"'); ?>
			<div class="text-orange"><?php echo form_error('jabatan'); ?></div>
		</div>
		-->
		<div class="form-group">
			<label>Tempat Lahir <span class="text-red">*</span> :</label>
			<input type="text" name="tempat_lahir_pengguna" class="form-control" placeholder="Enter ..." value="<?php echo set_value('tempat_lahir_pengguna', ($item) ? $item['tempat_lahir_pengguna'] : ''); ?>">
			<div class="text-orange"><?php echo form_error('tempat_lahir_pengguna'); ?></div>
		</div>
		<div class="form-group">
			<label>Tanggal Lahir <span class="text-red">*</span> :</label>
			<input type="text" name="tanggal_lahir_pengguna" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask placeholder="Enter ..." value="<?php echo set_value('tanggal_lahir_pengguna', ($item) ? $item['tanggal_lahir_pengguna'] : ''); ?>">
			<div class="text-orange"><?php echo form_error('tanggal_lahir_pengguna'); ?></div>
		</div>
		<div class="form-group">
			<label>Telepon :</label>
			<input class="form-control" name="telepon_pengguna" placeholder="Enter ..." type="text" value="<?php echo set_value('telepon_pengguna', ($item) ? $item['telepon_pengguna'] : ''); ?>">
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group">
			<label for="password">Password :</label>
			<input type="password" name="password" class="form-control" id="password" placeholder="Enter ...">
			<div class="text-orange"><?php echo form_error('password'); ?></div>
		</div>
		<div class="form-group">
			<label for="repassword">re-Password :</label>
			<input type="password" name="repassword" class="form-control" id="repassword" placeholder="Enter ...">
			<div class="text-orange"><?php echo form_error('repassword'); ?></div>
		</div>
		<?php if(permission('PenggunaEdit')){ ?>
			<?php if(permission('PenggunaFlag')){ ?>
				<div class="form-group">
					<label>Flag <span class="text-red">*</span> :</label>
					<?php echo form_dropdown('flag', $flag_options, set_value('flag', ($item) ? $item['flag'] : ''), 'class="form-control select2" id="input-flag"'); ?>
					<div class="text-orange"><?php echo form_error('flag'); ?></div>
				</div>
			<?php } ?>
			<div class="form-group">
				<label>Peran Pengguna <span class="text-red">*</span> :</label>
				<?php echo form_dropdown('id_peran_pengguna', $peran_pengguna_options, set_value('id_peran_pengguna', ($item) ? $item['id_peran_pengguna'] : ''), 'class="form-control select2" id="input-peran-pengguna"'); ?>
				<div class="text-orange"><?php echo form_error('id_peran_pengguna'); ?></div>
			</div>
		<?php } ?>
		<div class="form-group">
			<label>Alamat :</label>
			<textarea class="form-control" name="alamat_pengguna" rows="3" placeholder="Enter ..."><?php echo set_value('alamat_pengguna', ($item) ? $item['alamat_pengguna'] : ''); ?></textarea>
		</div>
		<div class="form-group">
			<label>Status <span class="text-red">*</span> :</label>
			<?php echo form_dropdown('status', $status_options, set_value('status', ($item) ? $item['status'] : ''), 'class="form-control select2" id="input-status"'); ?>
			<div class="text-orange"><?php echo form_error('status'); ?></div>
		</div>
	</div>
</div>