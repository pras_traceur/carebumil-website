<section class="content-header">
	<h1>
		Daftar Pengguna
		<small>Preview</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="<?php echo base_url('admin'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active">Daftar Pengguna</li>
	</ol>
</section>
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box">
				<div class="box-header">
					<?php if ($this->session->flashdata('pesan')) { ?>
					<div class="alert alert-success alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<h4><i class="icon fa fa-check"></i> Alert!</h4>
						<?php echo $this->session->flashdata('pesan'); ?>
					</div>
					<?php } ?>
					
					<h3 class="box-title">Daftar Pengguna</h3>
				</div><!-- /.box-header -->
				<div class="box-body">
					<table id="table" class="table table-hover table-striped" cellspacing="0" width="100%">
						<thead>
							<tr>
								<th class="title_field_id">No</th>
								<th>Nama</th>
								<th>Username</th>
								<th>Email</th>
								<th>Peran Pengguna</th>
								<th>Status</th>
								<?php if(permission('PenggunaFlag')){ ?>
								<th>Flag</th>
								<?php } ?>
								<th class="title_field_edit">Log Peran</th>
								<?php if(permission('PenggunaEdit')) { ?>
								<th class="title_field_edit">Ubah</th>
								<?php } ?>
								<?php if(permission('PenggunaMenghapus')) { ?>
								<th class="title_field_delete">Hapus</th>
								<?php } ?>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div><!-- /.box-body -->
			</div>
			<div>
				<?php if(permission('PenggunaTambah')) { ?>
				<a class="btn btn-primary" href="<?php echo base_url('admin/pengguna/tambah'); ?>">Tambah</a>
				<?php } ?>
				<?php if(permission('PenggunaEkspor')) { ?>
				<button class="btn btn-success"><i class="fa fa-download"></i> Download</button>
				<?php } ?>
			</div>
		</div>
	</div>
</section>
<script type="text/javascript">
    var table;
    $(document).ready(function() {
		$.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings)
        {
            return {
                "iStart": oSettings._iDisplayStart,
                "iEnd": oSettings.fnDisplayEnd(),
                "iLength": oSettings._iDisplayLength,
                "iTotal": oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            };
        };
		
		table = $('#table').DataTable({
			"processing": true, //Feature control the processing indicator.
			"serverSide": true, //Feature control DataTables' server-side processing mode.
	 
			// Load data for the table's content from an Ajax source
			"ajax": {
				"url": "<?php echo site_url('admin/pengguna/get_list_ajax')?>",
				"type": "POST",
				"dataType": "json"
			},
	 
			//Set column definition initialisation properties.
			"columnDefs": [
				{
					"targets": [ -1, -2 ],
					"orderable": false,
					"searchable": false
				},
				{
					"targets": [ 0 ],
					"searchable": false,
					"className": "dt-center"
				}
			],
			rowCallback: function(row, data, iDisplayIndex) {
                var info = this.fnPagingInfo();
                var page = info.iPage;
                var length = info.iLength;
                var index = page * length + (iDisplayIndex + 1);
                $('td:eq(0)', row).html(index);
            }
		});
    });
	
	function reload_table(){
		table.ajax.reload(null,false); //reload datatable ajax
    }
</script>