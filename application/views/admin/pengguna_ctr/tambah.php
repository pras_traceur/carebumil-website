<section class="content-header">
	<h1>
		Form Pengguna
		<small>Preview</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="<?php echo base_url('admin'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
		<li><a href="<?php echo base_url('admin/pengguna'); ?>">Daftar Pengguna</a></li>
		<li class="active">Form Pengguna</li>
	</ol>
</section>
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<!-- general form elements disabled -->
			<div class="box box-warning">
				<div class="box-header with-border">
					<h3 class="box-title">Form Pengguna</h3>
				</div><!-- /.box-header -->
				
				<?php echo form_open_multipart('admin/pengguna/tambah'); ?>
				
				<div class="box-body">	
					<?php $this->view('admin/pengguna_ctr/_form'); ?>
				</div><!-- /.box-body -->
				
				<div class="box-footer">
					<input class="btn btn-primary" type="submit" value="Tambah"/>
					<a href="<?php echo URLBACK . '/pengguna'; ?>" class="btn btn-default">Kembali ke Daftar</a>
				</div>
				
				<?php echo form_close(); ?>
				
			</div><!-- /.box -->
		</div>
	</div>
</section>