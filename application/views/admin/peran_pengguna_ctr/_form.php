<?php if (validation_errors()): ?>
	<div class="alert alert-warning alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		<h4><i class="icon fa fa-warning"></i> Alert!</h4>
		Pastikan form input yang wajib diisi telah terisi semuanya.
	</div>
<?php endif; ?>
<div class="row">
	<div class="col-md-12">
		<div class="form-group">
			<label>Nama Peran Pengguna <span class="text-red">*</span> :</label>
			<input class="form-control" name="nama_peran_pengguna" placeholder="Enter ..." type="text" value="<?php echo set_value('nama_peran_pengguna', ($item) ? $item['nama_peran_pengguna'] : ''); ?>">
			<div class="text-orange"><?php echo form_error('nama_peran_pengguna'); ?></div>
		</div>
		<?php if(permission('PeranPenggunaFlag')){ ?>
		<div class="form-group">
			<label>Flag <span class="text-red">*</span> :</label>
			<?php echo form_dropdown('flag', $flag_options, set_value('flag', ($item) ? $item['flag'] : ''), 'class="form-control select2 input-flag"'); ?>
			<div class="text-orange"><?php echo form_error('flag'); ?></div>
		</div><!-- /.form-group -->
		<?php } ?>
		<!-- textarea -->
		<div class="form-group">
			<label>Deskripsi :</label>
			<textarea class="form-control" name="deskripsi" rows="3" placeholder="Enter ..."><?php echo set_value('deskripsi', ($item) ? $item['deskripsi'] : ''); ?></textarea>
		</div>
		<div class="form-group">
			<label>Hak Akses : </label>
			<div style="margin-bottom: 1em;"><input type="checkbox" id="id_peran_rinci"> Menandai Semua</div>
			<?php echo $checkbox_peran_pengguna_rinci; ?>
		</div>
	</div>
</div>
<script>
	$(document).ready(function () {
        $('#id_peran_rinci').click(function () {
            if ($(this).is(':checked')) {
                $('input:checkbox').prop('checked', true);
            } else {
                $('input:checkbox').prop('checked', false);
            }
        })
    });
</script>