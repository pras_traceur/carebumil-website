<section class="content-header">
	<h1>
		Form Peran Pengguna
		<small>Preview</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="<?php echo base_url('admin'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
		<li><a href="<?php echo base_url('admin/peran-pengguna'); ?>">Daftar Peran Pengguna</a></li>
		<li class="active">Form Peran Pengguna</li>
	</ol>
</section>
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<!-- general form elements disabled -->
			<div class="box box-warning">
				<div class="box-header with-border">
					<?php if ($this->session->flashdata('pesan')) { ?>
					<div class="alert alert-success alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<h4><i class="icon fa fa-check"></i> Alert!</h4>
						<?php echo $this->session->flashdata('pesan'); ?>
					</div>
					<?php } ?>
					
					<h3 class="box-title">Form Peran Pengguna</h3>
				</div><!-- /.box-header -->
				
				<?php echo form_open_multipart('admin/peran-pengguna/ubah/' . $item['id_peran_pengguna']); ?>
					
				<div class="box-body">
					<?php $this->view('admin/peran_pengguna_ctr/_form'); ?>
				</div><!-- /.box-body -->
				
				<div class="box-footer">
					<input class="btn btn-primary" type="submit" value="Ubah"/>
					<a href="<?php echo URLBACK . '/peran-pengguna'; ?>" class="btn btn-default">Kembali ke Daftar</a>
				</div>
				
				<?php echo form_close(); ?>
				
			</div><!-- /.box -->
		</div>
	</div>
</section>