<?php

class Template{
	var $CI;

	public function __construct(){
		$this->CI =& get_instance();
	}
	
	function build($view, $content = array()){
		$this->CI =& get_instance();
		
		if ( ! file_exists(APPPATH.'views/'.$view.'.php')){
			// Whoops, we don't have a page for that!
			show_404();
		}
		
		$data = array(
			'header' => $this->CI->parser->parse('templates/header', $content, TRUE),
			'content' => $this->CI->parser->parse($view, $content, TRUE),
			'footer' => $this->CI->parser->parse('templates/footer', $content, TRUE)
		);
			
		$data = array_merge($content, $data);
		
		$this->CI->parser->parse('index', $data);
	}
	
	function build_admin($view, $content = array())	{
		$this->CI =& get_instance();

		$folder = 'admin/';

		if ( ! file_exists(APPPATH.'views/' . $folder . '/'.$view.'.php')){
			// Whoops, we don't have a page for that!
			show_404();
		}
		
		$data = array(
			'header' => $this->CI->parser->parse($folder . 'templates/header', $content, TRUE),
			'sidebar' => $this->CI->parser->parse($folder . 'templates/sidebar', $content, TRUE),
			'content' => $this->CI->parser->parse($folder . $view, $content, TRUE),
			'footer' => $this->CI->parser->parse($folder . 'templates/footer', $content, TRUE)
		);
			
		$index = $folder . 'index';

		$data = array_merge($content, $data);
		
		$this->CI->parser->parse($index, $data);
	}

	function build_frontpage($view, $content = array())	{
		$this->CI =& get_instance();

		$folder = 'frontpage/';

		if ( ! file_exists(APPPATH.'views/' . $folder . '/'.$view.'.php')){
			// Whoops, we don't have a page for that!
			show_404();
		}
		
		$data = array(
			'header' => $this->CI->parser->parse($folder . 'templates/header', $content, TRUE),
			'content' => $this->CI->parser->parse($folder . $view, $content, TRUE),
			'footer' => $this->CI->parser->parse($folder . 'templates/footer', $content, TRUE)
		);
			
		$index = $folder . 'index';

		$data = array_merge($content, $data);
		
		$this->CI->parser->parse($index, $data);
	}
}