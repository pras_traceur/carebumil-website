<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Login_ctr extends CI_Controller {
	public $table = 'pengguna';
	public $select = 'id_pengguna, nama_pengguna, username, email_pengguna, password';
	public $table1 = 'tokens';
	public $select1 = 'id, token, id_pengguna';
	public $table2 = 'pengaturan';
	public $select2 = 'id_pengaturan, field, deskripsi';
	

	public function __construct() {
        parent::__construct();
		$this->load->model('Mlogin');
		$this->load->helper('form', 'url'); 
		$this->load->model('Mcommon');
    }


	public function index(){
		$data['meta_title'] = 'Login';

		if($this->input->post('ubahuser')) {
			$data['username'] = $this->session->userdata('username');
			$this->session->unset_userdata('username');
			$this->session->unset_userdata('username_logged_in');
			$this->session->unset_userdata('foto');
			$this->session->unset_userdata('nama_pengguna');
			$this->session->unset_userdata('id_peran_pengguna');
			$this->session->unset_userdata('id_pengguna');
			$this->session->unset_userdata('email_pengguna');
			$this->session->unset_userdata('password_logged_in');
			$this->session->unset_userdata('tanggal_buat');
			$this->session->unset_userdata('id_peran_rinci');
			$this->session->unset_userdata('nama_peran_pengguna');
			$this->session->unset_userdata('rekam_medik');
			$this->session->sess_destroy();
		}

		if($this->session->userdata('username_logged_in')){
			$this->form_validation->set_rules('password', 'Password', 'required|callback_login_check_password');
		}else{
			($this->input->post('ubahuser'))? "" :$this->form_validation->set_rules('username', 'Username', 'required|callback_login_check_username');
		}
        
		if ($this->form_validation->run() == FALSE) {
			if($this->session->userdata('username_logged_in') && $this->session->userdata('password_logged_in')){
				redirect(URLBACK);
			}else{
				$this->parser->parse('admin/login_ctr/index', $data);
			}
        } else {
			if($this->session->userdata('username_logged_in') && $this->session->userdata('password_logged_in')){
				redirect(URLBACK);
			}else if($this->session->userdata('username_logged_in')){
				redirect(URLBACK . '/login');
			}else{
				redirect(URLBACK . '/login');
			}
        }
    }

	public function set_session($result = array()){
		$id_peran_rinci = $this->Mlogin->auth($result['id_peran_pengguna']);
		$role = $this->Mlogin->peran_pengguna($result['id_peran_pengguna']);
		
		$newdata = array(
			'username' => $result['username'],
			'id_pengguna' => $result['id_pengguna'],
			'id_peran_pengguna' => $result['id_peran_pengguna'],
			'nama_pengguna' => $result['nama_pengguna'],
			'foto' => $result['foto'],
			'nama_peran_pengguna' => $role['nama_peran_pengguna'],
			'tanggal_buat' => date('M, Y', strtotime($result['tanggal_buat'])),
			'email_pengguna' => $result['email_pengguna'],
			'id_peran_rinci' => json_encode($id_peran_rinci),
			'username_logged_in' => TRUE,
			'rekam_medik' => $result['rekam_medik'],
		);
		
		return $newdata;
	}
	
	public function login_check_username(){
        $username = $this->input->post('username');

        $result = $this->Mlogin->peran($username, false);
        
		if ($result) {
			$newdata = $this->set_session($result);
			
			$this->session->set_userdata($newdata);
			
            return true;
        } else {
            $this->form_validation->set_message('login_check_username', 'Email Salah');
            return false;
        }
    }
	
	public function login_check_password(){
        $password = md5($this->input->post('password'));

        $result = $this->Mlogin->peran($this->session->userdata('username'), $password);
		
		if ($result) {
			$newdata = $this->set_session($result);
			$newdata['password_logged_in'] = TRUE;
			
			$this->session->set_userdata($newdata);
			
            return true;
        } else {
            $this->form_validation->set_message('login_check_password', 'Wrong password');
            return false;
        }
    }
	
	 public function logout(){
		if($this->session->userdata('username_logged_in') && $this->session->userdata('password_logged_in')){
			$this->session->sess_destroy();
		}
		
		redirect(URLBACK . '/login');
    }

    public function lupa_password() 
    {
    	$data['meta_title'] = 'Lupa Password';
    	$email_web = $this->Mcommon->find($this->select2, $this->table2, array('field' => 'KONTAK_EMAIL'));
    	
    	$this->form_validation->set_rules('email', 'Email', 'required|valid_email');   
         
		if($this->form_validation->run() == FALSE) {  
			$data['title'] = 'Halaman Reset Password';  
			$this->load->view('admin/login_ctr/lupa_password',$data);  
		}else{  
			$email = $this->input->post('email'); 
			$userInfo = $this->Mlogin->getUserInfoByEmail($email);  

			if(!$userInfo){  
				$this->session->set_flashdata('pesan', 'email address salah, silakan coba lagi.');  
				redirect(site_url('admin/login/lupa_password'),'refresh');   
			}    

			//build token   
			$token = $this->Mlogin->insertToken($userInfo->id_pengguna); 
			$url = site_url() . 'admin/login/reset_password/' . $token;  
			$link = '<a href="' . $url . '">' . $url . '</a>';   

			$message = '';             
			$message .= '<strong>Hai, anda menerima email ini karena ada permintaan untuk memperbaharui  
			password anda.</strong><br>';  
			$message .= '<strong>Silakan klik link ini:</strong> ' . $link;

			// send mail
			$ci = get_instance();
			$ci->load->library('email');
			$config['protocol'] = "smtp";
			$config['smtp_host'] = "ssl://smtp.gmail.com";
			$config['smtp_port'] = "465";
			$config['smtp_user'] = "indraccntts@gmail.com";
			$config['smtp_pass'] = "indrawulida";
			$config['charset'] = "utf-8";
			$config['mailtype'] = "html";
			$config['newline'] = "\r\n";

			$ci->email->initialize($config);

			$ci->email->from($email_web['deskripsi'], 'Reset Password Monev');
			$list = array($email);
			$ci->email->to($list);
			$ci->email->subject('Reset Password Monev');
			$ci->email->message($message);
			if ($this->email->send()) {
				echo 'Email sent.';
			} else {
				show_error($this->email->print_debugger());
			}      
			$this->session->set_flashdata('pesan', 'Cek Email Anda Untuk Reset Password !');	   
			
			redirect(URLBACK . '/login');
		}  
    }

    public function reset_password($token) 
    {

    	if($token){
			$item = $this->Mcommon->find($this->select1, $this->table1, array('token' => $token));
			$this->rules[] = array('field' => 'password', 'label' => 'Password', 'rules' => 'required');
			$this->rules[] = array('field' => 'repassword', 'label' => 're-Password', 'rules' => 'required|matches[password]');

			$this->form_validation->set_rules($this->rules);
			
			if ($this->form_validation->run() === false) {
				
				$data['item'] = $item;

				$this->load->view('admin/login_ctr/reset_password',$data);
			}else{
				$item = $this->Mcommon->find($this->select1, $this->table1, array('token' => $token));

				$data = array(
					'password' => md5($this->input->post('password'))
				);
				
				$this->Mcommon->update($this->table, $data, array('id_pengguna' => $item['id_pengguna']));
				
				$this->session->set_flashdata('pesan', 'Reset Password berhasil');
				
				redirect(URLBACK . '/login');
			}
		}else{
			redirect(URLBACK . '/login');
		}
	}
	
	public function daftar(){
		$this->load->view('admin/login_ctr/daftar'); 
	}

	public function submitdaftar(){
		$username = $this->input->post('username');
		$nama_pengguna = $this->input->post('nama_pengguna');
		$rekam_medik = $this->input->post('rekam_medik');
		$password = $this->input->post('password');

		if(empty($username) || empty($nama_pengguna) || empty($rekam_medik) || empty($password)){
			$this->session->set_flashdata('error', 'Data Tidak Lengkap, Daftar Lagi !');
			redirect(URLBACK . '/login');
		}

		$result = $this->Mcommon->find('*', 'pengguna', array("username" => $username));
		if($result){
			$this->session->set_flashdata('error', 'Error, Email sudah pernah dibuat mendaftar');
		}
		$this->db->trans_begin();

		$data = array(
			'username' => $this->input->post('username'),
			'email_pengguna' => $this->input->post('username'),
			'nama_pengguna' => $this->input->post('nama_pengguna'),
			'rekam_medik' => $this->input->post('rekam_medik'),
			'id_peran_pengguna' => 3,
			'password' => md5($this->input->post('password')),
			'status' => 1,
			'flag' => 1,
			'foto' => 'logobumil1.png'
		);

		
		$this->Mcommon->insert('pengguna', $data);
		
		
		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			$this->session->set_flashdata('error', 'Data Gagal di Simpan');
		}else{
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan', 'Data Berhasil di Simpan silahkan Login');
		}
		redirect(URLBACK . '/login');
	}

}