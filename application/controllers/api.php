<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Api extends CI_Controller {

	public function __construct() {
        parent::__construct();
        $this->load->model('Mcommon');
        $this->load->model('Mpengguna');
        $this->load->helper(array('url', 'date'));
    }

    public function loginApi(){
        $email = $this->input->post('email_pengguna');
        $password = $this->input->post('password');
        $result = $this->Mpengguna->getEmailPassword($email,md5($password));

        if($result){
            $response = array(
                'success' => 'ya',
                'info' => 'Login Sukses'
            );
            $response = array_merge($response,$result);
        }else{
            $response = array(
                'success' => 'tidak',
                'info' => 'Login Gagal'
            );
        }
        

        $this->output
        ->set_status_header(201)
        ->set_content_type('application/json', 'utf-8')
        ->set_output(json_encode($response, JSON_PRETTY_PRINT))
        ->_display();
        exit;
    }

    public function signUpApi(){
        $username = $this->input->post('email_pengguna');
        $password = $this->input->post('password');
        $nama_pengguna = $this->input->post('nama_pengguna');
        $rekam_medik = $this->input->post('rekam_medik');

		if(empty($username) || empty($nama_pengguna) || empty($rekam_medik) || empty($password)){
			$response = array(
                'success' => 'tidak',
                'info' => 'Daftar Gagal, Data Tidak Lengkap'
            );
		}else{
            $result = $this->Mcommon->find('*', 'pengguna', array("username" => $username));
            if($result){
                $response = array(
                    'success' => 'tidak',
                    'info' => 'Error, Email sudah pernah dibuat mendaftar'
                );
            }else{
                $this->db->trans_begin();

                $data = array(
                    'username' => $username,
                    'email_pengguna' => $username,
                    'nama_pengguna' => $nama_pengguna,
                    'rekam_medik' => $rekam_medik,
                    'id_peran_pengguna' => 3,
                    'password' => md5($password),
                    'status' => 1,
                    'flag' => 1,
                    'foto' => 'logobumil1.png'
                );
                
                $this->Mcommon->insert('pengguna', $data);
                
                if ($this->db->trans_status() === FALSE){
                    $this->db->trans_rollback();
                    $response = array(
                        'success' => 'tidak',
                        'info' => 'Data Gagal di Simpan'
                    );
                }else{
                    $this->db->trans_commit();
                    $response = array(
                        'success' => 'ya',
                        'info' => 'Data Berhasil di Simpan silahkan Login'
                    );
                }
            }
        }

        $this->output
        ->set_status_header(201)
        ->set_content_type('application/json', 'utf-8')
        ->set_output(json_encode($response, JSON_PRETTY_PRINT))
        ->_display();
        exit;
    }

    public function getLastTest(){
        $id = $this->input->get('id_pengguna');

        $result = $this->Mcommon->last_find('*', 'test', array("id_pengguna" => $id));

        if($result){
            $response = array(
                'success' => 'ya',
                'info' => 'Ambil Data Sukses'
            );
            $response = array_merge($response,$result);
        }else{
            $response = array(
                'success' => 'tidak',
                'info' => 'Ambil Data Gagal'
            );
        }

        $this->output
        ->set_status_header(200)
        ->set_content_type('application/json', 'utf-8')
        ->set_output(json_encode($response, JSON_PRETTY_PRINT))
        ->_display();
        exit;
    }
    
    public function getResultTest(){
        $id = $this->input->get('id_pengguna');

        $result = $this->Mcommon->list_array('*', 'test', array("id_pengguna" => $id),'tanggal asc');

        if($result){
            $response = array(
                'success' => 'ya',
                'info' => 'Ambil Data Sukses',
                'data' => $result
            );
        }else{
            $response = array(
                'success' => 'tidak',
                'info' => 'Ambil Data Gagal'
            );
        }

        $this->output
        ->set_status_header(200)
        ->set_content_type('application/json', 'utf-8')
        ->set_output(json_encode($response, JSON_PRETTY_PRINT))
        ->_display();
        exit;
    }

    public function apiSubmitTest(){
		$score = $this->input->post('k1')+$this->input->post('k2')+$this->input->post('k3')+$this->input->post('k4')+$this->input->post('k5')+$this->input->post('k6')+$this->input->post('k7')+$this->input->post('k8')+$this->input->post('k9')+$this->input->post('k10');
		if($score >= 13){
			$hasil = 'Berisiko mengalami depresi';
		}else{
			$hasil = 'Tidak berisiko mengalami depresi';
		}
		$this->db->trans_begin();
		$data = array(
			'tanggal' => date("Y-m-d"),
			'score' => $score,
			'k1' => $this->input->post('k1'),
			'k2' => $this->input->post('k2'),
			'k3' => $this->input->post('k3'),
			'k4' => $this->input->post('k4'),
			'k5' => $this->input->post('k5'),
			'k6' => $this->input->post('k6'),
			'k7' => $this->input->post('k7'),
			'k8' => $this->input->post('k8'),
			'k9' => $this->input->post('k9'),
			'k10' => $this->input->post('k10'),
			'hasil' => $hasil,
			'flag' => 1,
			'id_pengguna' => $this->input->post('id_pengguna')
		);

		$this->Mcommon->insert_single('test', $data);
		
		if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
            $response = array(
                'success' => 'tidak',
                'info' => 'Data Test gagal di Simpan'
            );
		}else{
            $this->db->trans_commit();
            $response = array(
                'success' => 'ya',
                'info' => 'Data Test berhasil di Simpan'
            );
        }

        $this->output
        ->set_status_header(201)
        ->set_content_type('application/json', 'utf-8')
        ->set_output(json_encode($response, JSON_PRETTY_PRINT))
        ->_display();
        exit;
    }
    
    public function getDukunganSosial(){
        $id = $this->input->get('id_pengguna');
        
        $result = $this->Mcommon->find('*', 'dukungan_sosial', array("id_pengguna" => $id));
        if($result){
            $response = array(
                'success' => 'ya',
                'info' => 'Ambil Data Dukungan Sosial Sukses'
            );
            $response = array_merge($response,$result);
        }else{
            $response = array(
                'success' => 'tidak',
                'info' => 'Ambil Data Dukungan Sosial Gagal'
            );
        }

        $this->output
        ->set_status_header(200)
        ->set_content_type('application/json', 'utf-8')
        ->set_output(json_encode($response, JSON_PRETTY_PRINT))
        ->_display();
        exit;
    }
    
    public function apiSubmitDukunganSosial(){
        $id = $this->input->post('id_pengguna');

		$result = $this->Mcommon->find('*', 'dukungan_sosial', array("id_pengguna" => $id));
        $this->db->trans_begin();
        
        if($this->input->post('kekerasan') == "1"){
            $kekerasan  = "ya";
        }else{
            $kekerasan = "tidak";
        }

		$data = array(
			'kekerasan' => $kekerasan,
			'k1' => $this->input->post('k1'),
			'k2' => $this->input->post('k2'),
			'k3' => $this->input->post('k3'),
			'k4' => $this->input->post('k4'),
			'k5' => $this->input->post('k5'),
			'k6' => $this->input->post('k6'),
			'id_pengguna' => $id
		);

		if($result){
			$this->Mcommon->update_single('dukungan_sosial', $data, array("id_pengguna" => $id));
		}else{
			$this->Mcommon->insert_single('dukungan_sosial', $data);
		}
		
		if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
            $response = array(
                'success' => 'tidak',
                'info' => 'Data Dukungan Sosial gagal di Simpan'
            );
		}else{
            $this->db->trans_commit();
            $response = array(
                'success' => 'ya',
                'info' => 'Data Dukungan Sosial berhasil di Simpan'
            );
        }

        $this->output
        ->set_status_header(201)
        ->set_content_type('application/json', 'utf-8')
        ->set_output(json_encode($response, JSON_PRETTY_PRINT))
        ->_display();
        exit;
    }
    

    public function getPersiapanPersalinan(){
        $id = $this->input->get('id_pengguna');
        $result = $this->Mcommon->find('*', 'persiapan_persalinan', array("id_pengguna" => $id));
        if($result){
            $response = array(
                'success' => 'ya',
                'info' => 'Ambil Data Persiapan Persalinan Sukses'
            );
            $response = array_merge($response,$result);
        }else{
            $response = array(
                'success' => 'tidak',
                'info' => 'Ambil Data Dukungan Sosial Gagal'
            );
        }

        $this->output
        ->set_status_header(200)
        ->set_content_type('application/json', 'utf-8')
        ->set_output(json_encode($response, JSON_PRETTY_PRINT))
        ->_display();
        exit;
    }
    
    public function apiSubmitPersiapanPersalinan(){
        $id = $this->input->post('id_pengguna');

        if($this->input->post('kelas_hamil') == "1"){
            $kelas_hamil = "pernah";
        }else{
            $kelas_hamil = "tidak";
        }

        if($this->input->post('keinginan_bersalin') == "1"){
            $keinginan_bersalin = "normal";
        }else{
            $keinginan_bersalin = "caesar";
        }

        if($this->input->post('asuransi') == "1"){
            $asuransi = "ada";
        }else{
            $asuransi = "belum";
        }

        if($this->input->post('persiapan_dana') == "1"){
            $persiapan_dana = "ada";
        }else{
            $persiapan_dana = "belum";
        }

		$result = $this->Mcommon->find('*', 'persiapan_persalinan', array("id_pengguna" => $id));
		$this->db->trans_begin();

		$data = array(
			'kelas_hamil' => $kelas_hamil,
			'rencana_persalinan' => $this->input->post('rencana_persalinan'),
			'di' => $this->input->post('di'),
			'keinginan_bersalin' => $keinginan_bersalin,
			'asuransi' => $asuransi,
			'persiapan_dana' => $persiapan_dana,
			'id_pengguna' => $id
		);

		if($result){
			$this->Mcommon->update_single('persiapan_persalinan', $data, array("id_pengguna" => $id));
		}else{
			$this->Mcommon->insert_single('persiapan_persalinan', $data);
		}
		
		if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
            $response = array(
                'success' => 'tidak',
                'info' => 'Data Persiapan Persalinan gagal di Simpan'
            );
		}else{
            $this->db->trans_commit();
            $response = array(
                'success' => 'ya',
                'info' => 'Data Persiapan Persalinan berhasil di Simpan'
            );
        }

        $this->output
        ->set_status_header(201)
        ->set_content_type('application/json', 'utf-8')
        ->set_output(json_encode($response, JSON_PRETTY_PRINT))
        ->_display();
        exit;
    }
    
    public function getRiwayatObstetri(){
        $id = $this->input->get('id_pengguna');
		$result = $this->Mcommon->find('*', 'obsetri', array("id_pengguna" => $id));
		if($result){
            $response = array(
                'success' => 'ya',
                'info' => 'Ambil Data Riwayat Obstetri Sukses'
            );
            $response = array_merge($response,$result);
        }else{
            $response = array(
                'success' => 'tidak',
                'info' => 'Ambil Data Riwayat Obstetri Gagal'
            );
        }

        $this->output
        ->set_status_header(200)
        ->set_content_type('application/json', 'utf-8')
        ->set_output(json_encode($response, JSON_PRETTY_PRINT))
        ->_display();
        exit;
    }
    
    public function apiSubmitRiwayatObstetri(){
        $id = $this->input->post('id_pengguna');

        if($this->input->post('k1') == "1"){
            $k1 = "ya";
        }else{
            $k1 = "tidak";
        }
        if($this->input->post('k2') == "1"){
            $k2 = "ya";
        }else{
            $k2 = "tidak";
        }
        if($this->input->post('k3') == "1"){
            $k3 = "ya";
        }else{
            $k3 = "tidak";
        }
        if($this->input->post('k4') == "1"){
            $k4 = "ya";
        }else{
            $k4 = "tidak";
        }
        if($this->input->post('k5') == "1"){
            $k5 = "ya";
        }else{
            $k5 = "tidak";
        }

		$result = $this->Mcommon->find('*', 'obsetri', array("id_pengguna" => $id));
		$this->db->trans_begin();

		$data = array(
			'k1' => $k1,
			'k2' => $k2,
			'k3' => $k3,
			'k4' => $k4,
			'k5' => $k5,
			'jika_ya' => $this->input->post('jika_ya'),
			'id_pengguna' => $id
		);

		if($result){
			$this->Mcommon->update_single('obsetri', $data, array("id_pengguna" => $id));
		}else{
			$this->Mcommon->insert_single('obsetri', $data);
		}
		
		if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
            $response = array(
                'success' => 'tidak',
                'info' => 'Data Riwayat Obsetri gagal di Simpan'
            );
		}else{
            $this->db->trans_commit();
            $response = array(
                'success' => 'ya',
                'info' => 'Data Riwayat Obsetri berhasil di Simpan'
            );
        }

        $this->output
        ->set_status_header(201)
        ->set_content_type('application/json', 'utf-8')
        ->set_output(json_encode($response, JSON_PRETTY_PRINT))
        ->_display();
        exit;
    }
    
    public function getDatadiri(){
        $id = $this->input->get('id_pengguna');
        $result = $this->Mcommon->find('*', 'datadiri', array("id_pengguna" => $id));
        
        if($result){
            $tanggal = date_format(date_create($result['tanggal_lahir']),"d-m-Y");
            $umur = $this->hitung_umur($tanggal);
            $response = array(
                'success' => 'ya',
                'info' => 'Ambil Data Diri Sukses',
                'umur' => $umur
            );
            $response = array_merge($response,$result);
        }else{
            $response = array(
                'success' => 'tidak',
                'info' => 'Ambil Data Diri Gagal'
            );
        }

        $this->output
        ->set_status_header(200)
        ->set_content_type('application/json', 'utf-8')
        ->set_output(json_encode($response, JSON_PRETTY_PRINT))
        ->_display();
        exit;
    }

    public function apiSubmitDataDiri(){
        $id = $this->input->post('id_pengguna');

        if($this->input->post('pendidikan') == "SD"){
            $pendidikan = 0;
        }else if($this->input->post('pendidikan') == "SMP"){
            $pendidikan = 1;
        }else if($this->input->post('pendidikan') == "SMA"){
            $pendidikan = 2;
        }else if($this->input->post('pendidikan') == "S1"){
            $pendidikan = 3;
        }else if($this->input->post('pendidikan') == "S2"){
            $pendidikan = 4;
        }

        if($this->input->post('pekerjaan') == "Bekerja"){
            $pekerjaan = 0;
        }else{
            $pekerjaan = 1;
        }
        if($this->input->post('pernikahan') == "Menikah"){
            $pernikahan = 0;
        }else{
            $pernikahan = 1;
        }

        if($this->input->post('tipe_keluarga') == "Keluarga inti (suami,istri, anak)"){
            $tipe_keluarga = 0;
        }else{
            $tipe_keluarga = 1;
        }

        if($this->input->post('penghasilan') == "Cukup"){
            $penghasilan = 0;
        }else{
            $penghasilan = 1;
        }

		$result = $this->Mcommon->find('*', 'datadiri', array("id_pengguna" => $id));
		$this->db->trans_begin();
		$data = array(
			'tanggal_lahir' => date("Y-m-d", strtotime($this->input->post('tanggal_lahir'))),
			'pendidikan' => $pendidikan,
			'pekerjaan' => $pekerjaan,
			'pernikahan' => $pernikahan,
			'tipe_keluarga' => $tipe_keluarga,
			'penghasilan' => $penghasilan,
			'id_pengguna' => $id
        );
        
        if($result){
			$this->Mcommon->update_single('datadiri', $data, array("id_pengguna" => $id));
		}else{
			$this->Mcommon->insert_single('datadiri', $data);
		}

		if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
            $response = array(
                'success' => 'tidak',
                'info' => 'Data Diri gagal di Simpan'
            );
		}else{
            $this->db->trans_commit();
            $response = array(
                'success' => 'ya',
                'info' => 'Data Diri berhasil di Simpan'
            );
        }

        $this->output
        ->set_status_header(201)
        ->set_content_type('application/json', 'utf-8')
        ->set_output(json_encode($response, JSON_PRETTY_PRINT))
        ->_display();
        exit;
    }
    
    public function getKehamilan(){
        $id = $this->input->get('id_pengguna');
        $result = $this->Mcommon->find('*', 'kehamilan', array("id_pengguna" => $id));
        
        if($result){
            $hpl = $this->hitunghpl($result['tanggal_pertama_haid']);
            $response = array(
                'success' => 'ya',
                'info' => 'Ambil Data Kehamilan Sukses',
                'hpl' => $hpl
            );
            $response = array_merge($response,$result);
        }else{
            $response = array(
                'success' => 'tidak',
                'info' => 'Ambil Data Kehamilan Gagal'
            );
        }

        $this->output
        ->set_status_header(200)
        ->set_content_type('application/json', 'utf-8')
        ->set_output(json_encode($response, JSON_PRETTY_PRINT))
        ->_display();
        exit;
    }
    
    public function apiSubmitKehamilan(){
        $id = $this->input->post('id_pengguna');

        if($this->input->post('hamil_direncanakan') == "1"){
            $hamil_direncanakan = "ya";
        }else{
            $hamil_direncanakan = "tidak";
        }
        if($this->input->post('harapan_jenis_kelamin') == "1"){
            $harapan_jenis_kelamin = "ada";
        }else{
            $harapan_jenis_kelamin = "tidak";
        }
        if($this->input->post('antidepresi') == "1"){
            $antidepresi = "ya";
        }else{
            $antidepresi = "tidak";
        }
        if($this->input->post('keluhan') == "1"){
            $keluhan = "ya";
        }else{
            $keluhan = "tidak";
        }

		$result = $this->Mcommon->find('*', 'kehamilan', array("id_pengguna" => $id));
		$this->db->trans_begin();

		$data = array(
			'hamil_ke' => $this->input->post('hamil_ke'),
			'tanggal_pertama_haid' => date("Y-m-d", strtotime($this->input->post('tanggal_pertama_haid'))),
			'hamil_direncanakan' => $hamil_direncanakan,
			'harapan_jenis_kelamin' => $harapan_jenis_kelamin,
			'kalau_ada' => $this->input->post('kalau_ada'),
			'antidepresi' => $antidepresi,
			'keluhan' => $keluhan,
			'jika_ada' => $this->input->post('jika_ada'),
			'id_pengguna' => $id
		);

		if($result){
			$this->Mcommon->update_single('kehamilan', $data, array("id_pengguna" => $id));
		}else{
			$this->Mcommon->insert_single('kehamilan', $data);
        }
        
		if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
            $response = array(
                'success' => 'tidak',
                'info' => 'Data Kehamilan gagal di Simpan'
            );
		}else{
            $this->db->trans_commit();
            $response = array(
                'success' => 'ya',
                'info' => 'Data Kehamilan berhasil di Simpan'
            );
        }

        $this->output
        ->set_status_header(201)
        ->set_content_type('application/json', 'utf-8')
        ->set_output(json_encode($response, JSON_PRETTY_PRINT))
        ->_display();
        exit;
	}
    
    function hitung_umur($tanggal_lahir){
		$birthDate = new DateTime($tanggal_lahir);
		$today = new DateTime("today");
		if ($birthDate > $today) { 
			exit("0 tahun 0 bulan 0 hari");
		}
		$y = $today->diff($birthDate)->y;
		$m = $today->diff($birthDate)->m;
		$d = $today->diff($birthDate)->d;
		return $y." tahun ";
    }
    
    public function hitunghpl($tanggal){
		$HHT   = date_format(date_create($tanggal),"d");
		$bulan = date_format(date_create($tanggal),"m");
		$tahun = date_format(date_create($tanggal),"Y");

		// HPL naigale
		if($bulan >=1 && $bulan <= 3){
			$HHT   = +$HHT +7;
			$bulan = +$bulan + 9;
			$tahun = +$tahun + 0;
		} else if ($bulan >= 4 && $bulan <= 12){
			$HHT   = +$HHT +7;
			$bulan = $bulan - 3;
			$tahun = +$tahun + 1;	
		} 

		// validasi $bulan & $tahun
		if($bulan == 2){
			if($tahun%2==0){
				if($HHT>=29){
					$HHT   = $HHT - 29;
					$bulan = $bulan - 1;
				} 
			} else {
				if($HHT>=28){
					$HHT = $HHT - 28;
					$bulan = $bulan - 1;
				}
			}
		}else if($bulan%2==0){
			if($bulan==8){
				if($HHT>31){
					$HHT   = $HHT - 31;
					$bulan = +$bulan + 1;
				}
			} else if($bulan>12){
				if($HHT>30){
					$HHT   = $HHT - 30;
					$bulan = $bulan - 12;
					$tahun = +$tahun + 1;
				}else{
					$bulan = $bulan - 12;
					$tahun = +$tahun + 1;	
				}
			}else{
				if($HHT>30){
					$HHT   = $HHT - 30;
					if($bulan>=12){
						$bulan = $bulan - 11;			
					} else {
						$bulan = +$bulan + 1;	
					}
				}
			}

		} else if($bulan%2!=0){
			if($bulan>12){
				if($HHT>31){
					$HHT   = $HHT - 31;
					$bulan = $bulan - 12;
					$tahun = +$tahun + 1;
				}else{
					$bulan = $bulan - 11;
					$tahun = +$tahun + 1;	
				}
			}else if($HHT>31){
				$HHT   = $HHT - 31;
				$bulan = +$bulan + 1;	
			}
		}

		$HPL = $tahun.'-'.$bulan.'-'.$HHT;
		return $HPL;
	}

}