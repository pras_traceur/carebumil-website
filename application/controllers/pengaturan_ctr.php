<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Pengaturan_ctr extends CI_Controller {
    public $tableoption1 = 'pengaturan_dropdown';
	public $labeloption1 = 'Pengaturan';
    public $option1 = array('value_dropdown','label_dropdown');
    
	public function __construct() {
        parent::__construct();
        $this->load->model('Mcommon');
        $this->load->model('Mpengaturan');
        $this->load->helper(array('url', 'date'));
    }

    public function do_upload()
        {
            $config['upload_path']          = './uploads/';
            $config['allowed_types']        = 'xls|xlsx|doc|docx|zip|rar';

            $this->load->library('upload', $config);

            if ( ! $this->upload->do_upload('userfile'))
            {
                    $error = array('error' => $this->upload->display_errors());

                    $this->load->view('upload_form', $error);
            }
            else
            {
                    $data = array('upload_data' => $this->upload->data());

                    $this->load->view('upload_success', $data);
            }
        }


    public function index(){
		if(permission('PengaturanMengakses')) {
            $this->form_validation->set_rules(
                array(
                    array('field' => 'WEBSITE_NAMA', 'label' => 'Nama', 'rules' => 'required')
                )
            );

            if ($this->form_validation->run() === false) {
                $data = array(
                    'meta_title' => 'Pengaturan',
                    'get_pengaturan' => $this->get_pengaturan()
                );

                $this->template->build_admin('pengaturan_ctr/index', $data);
            }else{
                $config['upload_path'] = 'uploads/login/';
                $config['allowed_types'] = 'jpg|jpeg|png';
                $this->load->library('upload', $config);

                // script upload Image pertama
                $this->upload->do_upload('WEBSITE_BACKGROUND_LOGIN');
                $WEBSITE_BACKGROUND_LOGIN_NAME = $this->upload->data()['file_name'];

                // script uplaod Image kedua
                $this->upload->do_upload('WEBSITE_ICON_LOGIN');
                $WEBSITE_ICON_LOGIN_NAME = $this->upload->data()['file_name'];

                if (!empty($_FILES['WEBSITE_BACKGROUND_LOGIN']['name'])) {
                    $data = array(
                        'deskripsi' => $WEBSITE_BACKGROUND_LOGIN_NAME
                        ,'dibuat_oleh' => $this->session->userdata('id_pengguna')
                    );

                    $this->db->where('field', 'WEBSITE_BACKGROUND_LOGIN');
                    $this->db->update('pengaturan', $data);
                }

                if (!empty($_FILES['WEBSITE_ICON_LOGIN']['name'])) {
                    $data = array(
                        'deskripsi' => $WEBSITE_ICON_LOGIN_NAME
                        ,'dibuat_oleh' => $this->session->userdata('id_pengguna')
                    );

                    $this->db->where('field', 'WEBSITE_ICON_LOGIN');
                    $this->db->update('pengaturan', $data);
                }

                foreach($this->input->post() as $key => $value)
                {
                    $data = array(
                        'deskripsi' => $value
                        ,'dibuat_oleh' => $this->session->userdata('id_pengguna')
                    );

                    $this->db->where('field', $key);
                    $this->db->update('pengaturan', $data);
                }

                $this->session->set_flashdata('pesan', 'Data pengaturan berhasil diubah');
				
				redirect(URLBACK . '/pengaturan');
            }
		}
    }

    function get_pengaturan(){
        $data = array();
        
        $result = $this->Mcommon->list_array('id_pengaturan, field, label, deskripsi, dibuat_oleh, tipe, tipe_param_value, foto, grup, tanggal_buat, tanggal_ubah', 'pengaturan', false, 'grup asc, field asc');
        
        if($result){
            $get_jumlah_tabs = $this->get_jumlah_tabs();

            $i = 1;

            foreach($get_jumlah_tabs as $key => $jumlah_field){
                $tag_table = 1;

                foreach ($result as $row)
                {
                    if($key == $row['grup']){
                        $id_pengaturan = !isset($row['id_pengaturan']) ? "" : $row['id_pengaturan'];
                        $field = !isset($row['field']) ? "" : $row['field'];
                        $label = !isset($row['label']) ? "" : $row['label'];
                        $deskripsi = !isset($row['deskripsi']) ? "" : $row['deskripsi'];
                        $id_pengguna = !isset($row['dibuat_oleh']) ? "" : $row['dibuat_oleh'];
                        $tipe = !isset($row['tipe']) ? "" : $row['tipe'];
                        $tipe_param_value = !isset($row['tipe_param_value']) ? "" : $row['tipe_param_value'];
                        $foto = !isset($row['foto']) ? "" : $row['foto'];
                        $grup = !isset($row['grup']) ? "" : $row['grup'];
                        $tanggal_buat = !isset($row['tanggal_buat']) ? "" : $row['tanggal_buat'];
                        $tanggal_ubah = !isset($row['tanggal_ubah']) ? "" : $row['tanggal_ubah'];

                        $data[$i] = array(
                            "pengaturan_id" => $id_pengaturan,
                            "pengaturan_field" => $field,
                            "pengaturan_label" => $label,
                            "pengaturan_deskripsi" => $deskripsi,
                            "pengaturan_id_pengguna" => $id_pengguna,
                            "pengaturan_tipe" => $tipe,
                            "pengaturan_tipe_param_value" => $tipe_param_value,
                            "pengaturan_foto" => $foto,
                            "pengaturan_grup" => $grup,
                            "pengaturan_tanggal_buat" => $tanggal_buat,
                            "pengaturan_tanggal_ubah" => $tanggal_ubah,
                            "pengaturan_tabs_opened" => ($tag_table == 1) ? '<h1 style="margin: 0 0 0.2em 0;"><small>' . strtoupper($key) . '</small></h1><table class="table table-bordered"><tbody>' : '',
                            "pengaturan_tabs_closed" => ($tag_table == $jumlah_field) ? '</tbody></table>' : ''
                        );

                        $data[$i]['pengaturan_input'] = $this->tipe_input($tipe, $tipe_param_value, $data[$i]);
                        
                        $tag_table++;
                        $i++;
                    }
                }
            }
        }

		return $data;
    }

    function get_jumlah_tabs(){
        $data = array();

        $list_grup = $this->Mpengaturan->list_grup();
        $jumlah_field = $this->Mpengaturan->jumlah_field($list_grup);
    
        foreach($list_grup as $row){
            if($row['grup'])
                $data[$row['grup']] = ($jumlah_field[$row['grup']]) ? $jumlah_field[$row['grup']] : 0;
        }

        return $data;
    }

    function tipe_input($tipe, $tipe_param_value, $data){
        if($tipe == "textarea")
            $input = '<textarea class="form-control" id="' . $data['pengaturan_field'] . '" name="' . $data['pengaturan_field'] . '" rows="4" placeholder="Enter ...">' . $data['pengaturan_deskripsi'] . '</textarea>';
        else if($tipe == "dropdown"){
            $pengaturan_dropdown_options = $this->Mcommon->get_select_options($this->tableoption1, $this->option1, $this->labeloption1, array('grup_dropdown' => $tipe_param_value));
            $input = form_dropdown($data['pengaturan_field'], $pengaturan_dropdown_options, set_value($data['pengaturan_field'], ($data) ? $data['pengaturan_deskripsi'] : ''), 'class="form-control select2" id="input-' . $data['pengaturan_field'] . '"');
        }else if($tipe == "file"){
            $input = '<img id="image" src="' . base_url('uploads/login/'.$data['pengaturan_deskripsi']) . '" alt="" style="width: 350px;"/> <br><br> <input class="form-control" id="' . $data['pengaturan_field'] . '" name="' . $data['pengaturan_field'] . '" type="' . $data['pengaturan_tipe'] . '" value="' . $data['pengaturan_deskripsi'] . '" placeholder="Enter ...">';
        }else
            $input = '<input class="form-control" id="' . $data['pengaturan_field'] . '" name="' . $data['pengaturan_field'] . '" type="' . $data['pengaturan_tipe'] . '" value="' . $data['pengaturan_deskripsi'] . '" placeholder="Enter ...">';

        return $input;
    }
}