<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Peran_pengguna_ctr extends CI_Controller {
	public $table = 'peran_pengguna';
	public $select = 'id_peran_pengguna, nama_peran_pengguna, flag, deskripsi, tanggal_buat, tanggal_ubah';

	public $rules = array(
        array('field' => 'nama_peran_pengguna', 'label' => 'Nama Peran Pengguna', 'rules' => 'required'),
        array('field' => 'flag', 'label' => 'Flag', 'rules' => '')
    );
	
	public function __construct() {
        parent::__construct();
		
		$this->load->model('Mcommon');
		$this->load->model('Mperan_pengguna');
        $this->load->helper(array('url', 'date'));
    }

	public function index(){
		if(permission('PeranPenggunaMengakses')) {
			$data['meta_title'] = 'Peran Pengguna';

			$this->template->build_admin('peran_pengguna_ctr/index', $data);
		}
	}
	
	public function get_list_ajax()
    {	
		header('Content-Type: application/json;charset=utf-8');
		
		if(permission('PeranPenggunaMengakses')) {
			if(! permission('PeranPenggunaFlag')){
				$this->datatables->select('id_peran_pengguna, nama_peran_pengguna, deskripsi');
				$this->datatables->where('flag', 1);
			}else{
				$this->datatables->select('id_peran_pengguna, nama_peran_pengguna, deskripsi, flag');
				$this->datatables->edit_column('flag', '$1', 'flag', flagOptions('label'));
			}

			$this->datatables->from('peran_pengguna ppen');
			
			if(permission('PeranPenggunaEdit')) {
				$this->datatables->add_column('ubah', '<a class="btn btn-sm btn-success" title="Ubah" href="' . base_url('admin/peran-pengguna/ubah') . '/$1"><i class="glyphicon glyphicon-pencil"></i> Ubah</a>', 'id_peran_pengguna');
			}
			
			if(permission('PeranPenggunaMenghapus')) {
				$this->datatables->add_column('hapus', '<a class="btn btn-sm btn-danger" title="Hapus" href="' . base_url('admin/peran-pengguna/hapus') . '/$1"><i class="glyphicon glyphicon-trash"></i> Hapus</a>', 'id_peran_pengguna');
			}
			
			echo $this->datatables->generate('json', 'ISO-8859-1');
		}
    }
	
	public function tambah(){
		if(permission('PeranPenggunaTambah')) {
			$data['meta_title'] = 'Tambah Peran Pengguna';

			$this->form_validation->set_rules($this->rules);

			if ($this->form_validation->run() === false) {
				$data['item'] = array();
				
				if(permission('PeranPenggunaFlag'))
					$data['flag_options'] = flagOptions();

				$checkbox_peran_pengguna_rinci = $this->checkbox_peran_pengguna_rinci();
				$data['checkbox_peran_pengguna_rinci'] = $this->nav_tabs($checkbox_peran_pengguna_rinci);

				$this->template->build_admin('peran_pengguna_ctr/tambah', $data);
			} else {

				$this->Mperan_pengguna->insert();
				
				$this->session->set_flashdata('pesan', 'Data peran pengguna berhasil ditambah');
				
				redirect(URLBACK . '/peran-pengguna');
			}
		}
	}
	
	public function ubah($id){
		if(permission('PeranPenggunaEdit')) {
			$data['meta_title'] = 'Ubah Peran Pengguna';
			
			if($id){
				$item = $this->Mcommon->find($this->select, $this->table, array('id_peran_pengguna' => $id));

				$this->form_validation->set_rules($this->rules);
				
				if ($this->form_validation->run() === false) {
					$data['item'] = $item;
					
					if(permission('PeranPenggunaFlag'))
						$data['flag_options'] = flagOptions();

					$checkbox_peran_pengguna_rinci = $this->checkbox_peran_pengguna_rinci($id);
					$data['checkbox_peran_pengguna_rinci'] = $this->nav_tabs($checkbox_peran_pengguna_rinci);
						
					$this->template->build_admin('peran_pengguna_ctr/ubah', $data);
				}else{
					
					$this->Mperan_pengguna->update($id);
					
					$this->session->set_flashdata('pesan', 'Data peran pengguna berhasil diubah');
					
					redirect(URLBACK . '/peran-pengguna/ubah/' . $id);
				}
			}else{
				redirect(URLBACK . '/peran-pengguna');
			}
		}
	}
	
	public function hapus($id)
    {
		if(permission('PeranPenggunaMenghapus')) {
			$this->Mcommon->update($this->table, array('flag' => 0), array('id_peran_pengguna' => $id));

			$this->session->set_flashdata('pesan', 'Data peran pengguna berhasil dihapus');

			redirect(URLBACK . '/peran-pengguna');
		}
    }
	
	public function checkbox_peran_pengguna_rinci($id = 0){
		if(permission('PeranPenggunaTambah, PeranPenggunaEdit')) {
			$listArrayPeranGrup = $this->Mcommon->list_array('id_peran_grup, nama_peran_grup, id_peran_grup_menu', 'peran_grup', array(), 'nama_peran_grup asc');
			$listArrayPeranRinci = $this->Mcommon->list_array('id_peran_rinci, id_peran_grup, nama_peran_rinci', 'peran_rinci', array(), 0);
			$listArrayPeranGrupMenu = $this->Mcommon->list_array('id_peran_grup_menu, nama_peran_grup_menu', 'peran_grup_menu', array(), 0);
			
			if($id){
				$listArrayPeranPenggunaRinci = $this->Mcommon->list_in_array('id_peran_rinci', 'peran_pengguna_rinci', array('id_peran_pengguna' => $id));
			}
			
			$checkbox = array();

			if($listArrayPeranGrup){
				foreach($listArrayPeranGrup as $row){
					$checkbox[$row['id_peran_grup_menu']][] = '<div class="col-md-3" style="margin-bottom: 1em;"><div style="font-size: 18px; font-weight: 300; line-height: 1.4;"><strong>' . $row['nama_peran_grup'] . '</strong></div>';
					
					foreach($listArrayPeranRinci as $row1){
						$checked = '';
						
						if($id){
							$checked = in_array($row1['id_peran_rinci'], $listArrayPeranPenggunaRinci) ? 'checked' : '';
						}
						
						if($row1['id_peran_grup'] == $row['id_peran_grup']){
							$checkbox[$row['id_peran_grup_menu']][] = '<div>';
							$checkbox[$row['id_peran_grup_menu']][] = '<input name="id_peran_rinci[]" value="' . $row1['id_peran_rinci'] . '" type="checkbox" ' . $checked . '> ' . $row1['nama_peran_rinci'];
							$checkbox[$row['id_peran_grup_menu']][] = '</div>';
						}
					}
					
					$checkbox[$row['id_peran_grup_menu']][] = '</div>';
				}
			}
			
			foreach($listArrayPeranGrupMenu as $row){
				$result[$row['nama_peran_grup_menu']] = implode('', $checkbox[$row['id_peran_grup_menu']]);
			}

			return $result;
		}
	}
	
	public function nav_tabs($checkbox_peran_pengguna_rinci = ""){
		$i = 1;
		foreach($checkbox_peran_pengguna_rinci as $key => $row){
			$active = ($i == 1) ? 'active' : '';
			
			$list[] = '<li class="' . $active . '"><a href="#' . $key . '" data-toggle="tab" aria-expanded="true">' . $key . '</a></li>';
			$content[] = '<div class="tab-pane ' . $active . '" id="' . $key . '" style="min-height: 52.5em;">' . $row . '</div>';
			
			$i++;
		}
		
		$html[] = '<div class="nav-tabs-custom">';
		$html[] = '<ul class="nav nav-tabs">';
		$html[] =  implode('', $list);
		$html[] = '</ul>';
		$html[] = '<div class="tab-content">';
		$html[] = implode('', $content);
		$html[] = '</div>';
		$html[] = '</div>';
		
		$result = implode('', $html);
		
		return $result;
	}
}