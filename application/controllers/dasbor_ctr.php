<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Dasbor_ctr extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('Mcommon');
        $this->load->helper(array('url', 'date'));
    }

	public function index(){
		if(permission('DasborMengakses')) {	
			$data['meta_title'] = 'Dashboard';

			if($this->session->userdata('id_peran_pengguna') == '1'){
				$this->template->build_admin('dasbor_ctr/index', $data);
			}else if($this->session->userdata('id_peran_pengguna') == '2'){
				$result = $this->Mcommon->result_bidan();
				$data['result'] = $result;
				$this->template->build_admin('dasbor_ctr/bidan/index', $data);
			}else if($this->session->userdata('id_peran_pengguna') == '3'){
				$this->template->build_admin('dasbor_ctr/ibu/index', $data);
			}
		}
	}

	public function detail($id){
		if(permission('DasborMengakses')) {	
			$data['pendidikan'] = array(0 => 'SD',1 => 'SMP',2 => 'SMA',3 => 'S1',4 => 'S2');
			$data['pekerjaan'] = array(0 => 'Bekerja',1 => 'Tidak Bekerja');
			$data['pernikahan'] = array(0 => 'Menikah',1 => 'Tidak Menikah');
			$data['tipe_keluarga'] = array(0 => 'Keluarga inti (suami,istri, anak)',1 => 'Keluarga inti + keluarga tambahan');
			$data['kecukupan'] = array(0 => 'Cukup',1 => 'Tidak Cukup');

			if($id){
				$data['nama_ibu'] = $this->Mcommon->find('nama_pengguna', 'pengguna', array("id_pengguna" => $id))['nama_pengguna'];
				$data['result1'] = $this->Mcommon->find('*', 'datadiri', array("id_pengguna" => $id));
				$data['tanggal1'] = date_format(date_create($data['result1']['tanggal_lahir']),"d-m-Y");
				$data['umur1'] = $this->hitung_umur($data['tanggal1']);

				$data['result2'] = $this->Mcommon->find('*', 'kehamilan', array("id_pengguna" => $id));

				$data['result3'] = $this->Mcommon->find('*', 'obsetri', array("id_pengguna" => $id));

				$data['result4'] = $this->Mcommon->find('*', 'dukungan_sosial', array("id_pengguna" => $id));

				$data['result5'] = $this->Mcommon->find('*', 'persiapan_persalinan', array("id_pengguna" => $id));

				$data['meta_title'] = 'Detail Data Ibu '.$data['nama_ibu'];
			}
			$this->template->build_admin('dasbor_ctr/bidan/detail', $data);
		}
	}

	public function tata_laksana(){
		if(permission('DasborMengakses')) {	
			$data['meta_title'] = 'Tata Laksana';
			$this->template->build_admin('dasbor_ctr/bidan/tata_laksana', $data);
		}
	}

	function hitung_umur($tanggal_lahir){
		$birthDate = new DateTime($tanggal_lahir);
		$today = new DateTime("today");
		if ($birthDate > $today) { 
			exit("0 tahun 0 bulan 0 hari");
		}
		$y = $today->diff($birthDate)->y;
		$m = $today->diff($birthDate)->m;
		$d = $today->diff($birthDate)->d;
		return $y." tahun ";
	}

	public function tentang(){
		if(permission('DasborMengakses')) {	
			$data['meta_title'] = 'Tentang Saya';
			$this->template->build_admin('dasbor_ctr/ibu/tentang', $data);
		}
	}

	public function test($id){
		if(permission('DasborMengakses')) {	
			$data['meta_title'] = 'Test';
			if($id){
				$result = $this->Mcommon->list_array('*', 'test', array("id_pengguna" => $id),'tanggal desc');
				$data['result'] = $result;

				$tanggal = $this->Mcommon->last_find('*', 'test', array("id_pengguna" => $id));
				if($tanggal['tanggal'] == date("Y-m-d")){
					$data['aktif'] = false;
				}else{
					$data['aktif'] = true;
				}
			}
			$this->template->build_admin('dasbor_ctr/ibu/test', $data);
		}
	}

	public function hasil_test($id){
		if(permission('DasborMengakses')) {	
			$data['meta_title'] = 'Hasil Test';
			if($id){
				$data['nama_ibu'] = $this->Mcommon->find('nama_pengguna', 'pengguna', array("id_pengguna" => $id))['nama_pengguna'];
				$resultss = $this->Mcommon->list_array('*', 'test', array("id_pengguna" => $id),'tanggal desc');
				$results = $this->Mcommon->list_array('*', 'test', array("id_pengguna" => $id),'tanggal asc');
				$result = $this->Mcommon->last_find('*', 'test', array("id_pengguna" => $id));
				$data['resultss'] = $resultss;
				$data['results'] = $results;
				$data['result'] = $result;
			}
			$this->template->build_admin('dasbor_ctr/ibu/hasil_test', $data);
		}
	}

	public function datadiri($id){
		if(permission('DasborMengakses')) {	
			$data['meta_title'] = 'Data Diri';
			$data['pendidikan'] = array(0 => 'SD',1 => 'SMP',2 => 'SMA',3 => 'S1',4 => 'S2');
			$data['pekerjaan'] = array(0 => 'Bekerja',1 => 'Tidak Bekerja');
			$data['pernikahan'] = array(0 => 'Menikah',1 => 'Tidak Menikah');
			$data['tipe_keluarga'] = array(0 => 'Keluarga inti (suami,istri, anak)',1 => 'Keluarga inti + keluarga tambahan');
			$data['kecukupan'] = array(0 => 'Cukup',1 => 'Tidak Cukup');
			if($id){
				$result = $this->Mcommon->find('*', 'datadiri', array("id_pengguna" => $id));
				$data['result'] = $result;
				$data['tanggal'] = date_format(date_create($result['tanggal_lahir']),"d-m-Y");
				$data['umur'] = $this->hitung_umur($data['tanggal']);
			}
			$this->template->build_admin('dasbor_ctr/ibu/datadiri', $data);
		}
	}

	public function kehamilanini($id){
		if(permission('DasborMengakses')) {	
			$data['meta_title'] = 'Kehamilan ini';
			if($id){
				$result = $this->Mcommon->find('*', 'kehamilan', array("id_pengguna" => $id));
				$data['result'] = $result;
			}
			$this->template->build_admin('dasbor_ctr/ibu/kehamilanini', $data);
		}
	}

	public function riwayatobstetri($id){
		if(permission('DasborMengakses')) {	
			$data['meta_title'] = 'Riwayat Obsetri';
			if($id){
				$result = $this->Mcommon->find('*', 'obsetri', array("id_pengguna" => $id));
				$data['result'] = $result;
			}
			$this->template->build_admin('dasbor_ctr/ibu/riwayatobstetri', $data);
		}
	}

	public function dukungansosial($id){
		if(permission('DasborMengakses')) {	
			$data['meta_title'] = 'Dukungan Sosial';
			if($id){
				$result = $this->Mcommon->find('*', 'dukungan_sosial', array("id_pengguna" => $id));
				$data['result'] = $result;
			}
			$this->template->build_admin('dasbor_ctr/ibu/dukungansosial', $data);
		}
	}

	public function persiapanpersalinan($id){
		if(permission('DasborMengakses')) {	
			$data['meta_title'] = 'Persiapan Persalinan';
			if($id){
				$result = $this->Mcommon->find('*', 'persiapan_persalinan', array("id_pengguna" => $id));
				$data['result'] = $result;
			}
			$this->template->build_admin('dasbor_ctr/ibu/persiapanpersalinan', $data);
		}
	}

	public function newtest(){
		if(permission('DasborMengakses')) {	
			$data['meta_title'] = 'Test Baru';
			$this->template->build_admin('dasbor_ctr/ibu/newtest', $data);
		}
	}

	public function showtest($id){
		if(permission('DasborMengakses')) {	
			$data['meta_title'] = 'Lihat Hasil Test';
			if($id){
				$result = $this->Mcommon->find('*', 'test', array("id" => $id));
				$data['result'] = $result;
			}
			$this->template->build_admin('dasbor_ctr/ibu/showtest', $data);
		}
	}

	public function submitdatadiri($id){
		$result = $this->Mcommon->find('*', 'datadiri', array("id_pengguna" => $id));
		$this->db->trans_begin();
		$data = array(
			'tanggal_lahir' => date("Y-m-d", strtotime($this->input->post('tanggal_lahir'))),
			'pendidikan' => $this->input->post('pendidikan'),
			'pekerjaan' => $this->input->post('pekerjaan'),
			'pernikahan' => $this->input->post('pernikahan'),
			'tipe_keluarga' => $this->input->post('tipe_keluarga'),
			'penghasilan' => $this->input->post('penghasilan'),
			'id_pengguna' => $id
		);

		if($result){
			$this->Mcommon->update_single('datadiri', $data, array("id_pengguna" => $id));
		}else{
			$this->Mcommon->insert_single('datadiri', $data);
		}
		
		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			$this->session->set_flashdata('error', 'Data Diri gagal di Simpan');
		}else{
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan', 'Data Diri berhasil di Simpan');
		}
		redirect(base_url('admin/datadiri').'/'.$id);
	}

	public function submitkehamilan($id){
		$result = $this->Mcommon->find('*', 'kehamilan', array("id_pengguna" => $id));
		$this->db->trans_begin();

		$data = array(
			'hamil_ke' => $this->input->post('hamil_ke'),
			'tanggal_pertama_haid' => date("Y-m-d", strtotime($this->input->post('tanggal_pertama_haid'))),
			'hamil_direncanakan' => $this->input->post('hamil_direncanakan'),
			'harapan_jenis_kelamin' => $this->input->post('harapan_jenis_kelamin'),
			'kalau_ada' => $this->input->post('kalau_ada'),
			'antidepresi' => $this->input->post('antidepresi'),
			'keluhan' => $this->input->post('keluhan'),
			'jika_ada' => $this->input->post('jika_ada'),
			'id_pengguna' => $id
		);

		if($result){
			$this->Mcommon->update_single('kehamilan', $data, array("id_pengguna" => $id));
		}else{
			$this->Mcommon->insert_single('kehamilan', $data);
		}
		
		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			$this->session->set_flashdata('error', 'Data Kehamilan gagal di Simpan');
		}else{
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan', 'Data Kehamilan berhasil di Simpan');
		}
		redirect(base_url('admin/kehamilanini').'/'.$id);
	}

	public function submitriwayatobstetri($id){
		$result = $this->Mcommon->find('*', 'obsetri', array("id_pengguna" => $id));
		$this->db->trans_begin();

		$data = array(
			'k1' => $this->input->post('k1'),
			'k2' => $this->input->post('k2'),
			'k3' => $this->input->post('k3'),
			'k4' => $this->input->post('k4'),
			'k5' => $this->input->post('k5'),
			'jika_ya' => $this->input->post('jika_ya'),
			'id_pengguna' => $id
		);

		if($result){
			$this->Mcommon->update_single('obsetri', $data, array("id_pengguna" => $id));
		}else{
			$this->Mcommon->insert_single('obsetri', $data);
		}
		
		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			$this->session->set_flashdata('error', 'Data Riwayat Obsetri gagal di Simpan');
		}else{
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan', 'Data Riwayat Obsetri di Simpan');
		}
		redirect(base_url('admin/riwayatobstetri').'/'.$id);
	}

	public function submitdukungansosial($id){
		$result = $this->Mcommon->find('*', 'dukungan_sosial', array("id_pengguna" => $id));
		$this->db->trans_begin();

		$data = array(
			'kekerasan' => $this->input->post('kekerasan'),
			'k1' => $this->input->post('k1'),
			'k2' => $this->input->post('k2'),
			'k3' => $this->input->post('k3'),
			'k4' => $this->input->post('k4'),
			'k5' => $this->input->post('k5'),
			'k6' => $this->input->post('k6'),
			'id_pengguna' => $id
		);

		if($result){
			$this->Mcommon->update_single('dukungan_sosial', $data, array("id_pengguna" => $id));
		}else{
			$this->Mcommon->insert_single('dukungan_sosial', $data);
		}
		
		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			$this->session->set_flashdata('error', 'Data Dukungan Sosial gagal di Simpan');
		}else{
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan', 'Data Dukungan Sosial di Simpan');
		}
		redirect(base_url('admin/dukungansosial').'/'.$id);
	}

	public function submitpersiapanpersalinan($id){
		$result = $this->Mcommon->find('*', 'persiapan_persalinan', array("id_pengguna" => $id));
		$this->db->trans_begin();

		$data = array(
			'kelas_hamil' => $this->input->post('kelas_hamil'),
			'rencana_persalinan' => $this->input->post('rencana_persalinan'),
			'di' => $this->input->post('di'),
			'keinginan_bersalin' => $this->input->post('keinginan_bersalin'),
			'asuransi' => $this->input->post('asuransi'),
			'persiapan_dana' => $this->input->post('persiapan_dana'),
			'id_pengguna' => $id
		);

		if($result){
			$this->Mcommon->update_single('persiapan_persalinan', $data, array("id_pengguna" => $id));
		}else{
			$this->Mcommon->insert_single('persiapan_persalinan', $data);
		}
		
		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			$this->session->set_flashdata('error', 'Data Persiapan Persalinan gagal di Simpan');
		}else{
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan', 'Data Persiapan Persalinan di Simpan');
		}
		redirect(base_url('admin/persiapanpersalinan').'/'.$id);
	}

	public function submittest(){
		$score = $this->input->post('k1')+$this->input->post('k2')+$this->input->post('k3')+$this->input->post('k4')+$this->input->post('k5')+$this->input->post('k6')+$this->input->post('k7')+$this->input->post('k8')+$this->input->post('k9')+$this->input->post('k10');
		if($score >= 13){
			$hasil = 'Berisiko mengalami depresi';
		}else{
			$hasil = 'Tidak berisiko mengalami depresi';
		}
		$this->db->trans_begin();
		$data = array(
			'tanggal' => date("Y-m-d"),
			'score' => $score,
			'k1' => $this->input->post('k1'),
			'k2' => $this->input->post('k2'),
			'k3' => $this->input->post('k3'),
			'k4' => $this->input->post('k4'),
			'k5' => $this->input->post('k5'),
			'k6' => $this->input->post('k6'),
			'k7' => $this->input->post('k7'),
			'k8' => $this->input->post('k8'),
			'k9' => $this->input->post('k9'),
			'k10' => $this->input->post('k10'),
			'hasil' => $hasil,
			'flag' => 1,
			'id_pengguna' => $this->session->userdata('id_pengguna')
		);

		$this->Mcommon->insert_single('test', $data);
		
		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			$this->session->set_flashdata('error', 'Data Test gagal di Simpan');
		}else{
			$this->db->trans_commit();
			$this->session->set_flashdata('pesan', 'Data Test berhasil di Simpan');
		}
		redirect(base_url('admin/test/'.$this->session->userdata('id_pengguna')));
	}

}