<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Reset_password_ctr extends CI_Controller {
	public $table = 'pengguna';
	public $select = 'id_pengguna, nama_pengguna, username, email_pengguna, password';

	public function __construct() {
        parent::__construct();
		
		$this->load->model('Mcommon');
		$this->load->model('Mperan_pengguna');
        $this->load->helper(array('url', 'date'));
    }

	public function index(){
		$data['meta_title'] = 'Reset Password';

		$this->template->build_admin('reset_password_ctr/index', $data);
	}
	
	public function get_list_ajax()
    {	
		header('Content-Type: application/json;charset=utf-8');
		
		if(permission('ResetPasswordMengakses')) {
			$this->datatables->select('id_pengguna, nama_pengguna, username, email_pengguna');
			$this->datatables->from('pengguna pen');
			$this->datatables->add_column('reset', '<a class="btn btn-sm btn-success" title="Reset Password" href="' . base_url('admin/reset-password/reset') . '/$1"><i class="fa fa-cogs"></i> Reset</a>', 'id_pengguna');
			
			echo $this->datatables->generate('json', 'ISO-8859-1');
		}
    }

    public function reset($id){
		if(permission('ResetPasswordMengakses')) {
			$data['meta_title'] = 'Reset Password';
			
			if($id){
				$item = $this->Mcommon->find($this->select, $this->table, array('id_pengguna' => $id));
				$this->rules[] = array('field' => 'password', 'label' => 'Password', 'rules' => 'required');
				$this->rules[] = array('field' => 'repassword', 'label' => 're-Password', 'rules' => 'required|matches[password]');

				$this->form_validation->set_rules($this->rules);
				
				if ($this->form_validation->run() === false) {
					$data['item'] = $item;
					$this->template->build_admin('reset_password_ctr/reset', $data);
				}else{

					$data = array(
						'password' => md5($this->input->post('password'))
					);
					
					$this->Mcommon->update($this->table, $data, array('id_pengguna' => $id));
					
					$this->session->set_flashdata('pesan', 'Reset Password berhasil');
					
					redirect(URLBACK . '/reset-password/reset/' . $id);
				}
			}else{
				redirect(URLBACK . '/reset-password');
			}
		}
	}
	
}