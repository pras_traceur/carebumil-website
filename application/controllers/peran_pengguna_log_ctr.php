<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Peran_pengguna_log_ctr extends CI_Controller {
	public $table = 'peran_pengguna_log';
	public $select = 'id_peran_pengguna_log, id_pengguna, id_peran_pengguna, label_peran_pengguna_log, deskripsi, flag, tanggal_mulai, tanggal_selesai, tanggal_buat, tanggal_ubah';
    
    public $rules = array(
        array('field' => 'judul', 'label' => 'Judul', 'rules' => ''),
		array('field' => 'deskripsi', 'label' => 'Deskripsi', 'rules' => ''),
    );

	public function __construct() {
        parent::__construct();
		$this->load->model('Mcommon');
    }

    public function index(){
		if(permission('PeranPenggunaLogMengakses')) {
            $id_pengguna = $this->input->get('id_pengguna');

			$data['meta_title'] = 'Peran Pengguna Log';
            $data['id_pengguna'] = $id_pengguna;

            $this->template->build_admin('peran_pengguna_log_ctr/index', $data);
		}
    }
    
	public function get_list_ajax($id)
    {
		header('Content-Type: application/json;charset=utf-8');
		
		if(permission('PeranPenggunaLogMengakses')) {
			if(! permission('PeranPenggunaLogFlag')){
				$this->datatables->select('peran_pengguna_log.id_peran_pengguna_log, peran_pengguna.nama_peran_pengguna, peran_pengguna_log.label_peran_pengguna_log, peran_pengguna_log.deskripsi, peran_pengguna_log.tanggal_buat');
				$this->datatables->where('peran_pengguna_log.flag', 1);
			}else{
				$this->datatables->select('peran_pengguna_log.id_peran_pengguna_log, peran_pengguna.nama_peran_pengguna, peran_pengguna_log.label_peran_pengguna_log, peran_pengguna_log.deskripsi, peran_pengguna_log.tanggal_buat, peran_pengguna_log.flag');
				$this->datatables->edit_column('flag', '$1', 'flag', flagOptions('label'));
			}
			
			$this->datatables->from('peran_pengguna_log');
            $this->datatables->join('pengguna', 'pengguna.id_pengguna = peran_pengguna_log.id_pengguna');
            $this->datatables->join('peran_pengguna', 'peran_pengguna.id_peran_pengguna = peran_pengguna_log.id_peran_pengguna');
			$this->datatables->where('peran_pengguna_log.id_pengguna', $id);

            if(permission('PeranPenggunaLogEdit')) {
                $this->datatables->add_column('ubah', '<a class="btn btn-sm btn-success" title="Ubah" href="' . base_url('admin/log-peran-pengguna/ubah') . '/$1?id_pengguna=' . $id . '"><i class="glyphicon glyphicon-pencil"></i> Ubah</a>', 'id_peran_pengguna_log');
            }

			echo $this->datatables->generate('json', 'ISO-8859-1');
		}
    }

    public function ubah($id){
		if(permission('PeranPenggunaLogEdit')) {
			$data['meta_title'] = 'Ubah Log Peran Pengguna';
            
            $id_pengguna = $this->input->get('id_pengguna');

			$item = $this->Mcommon->find($this->select, $this->table, array('id_peran_pengguna_log' => $id));

			$this->form_validation->set_rules($this->rules);
			
			if ($this->form_validation->run() === false) {
				$item['tanggal_mulai'] = dateInput($item['tanggal_mulai']);
				$item['tanggal_selesai'] = dateInput($item['tanggal_selesai']);
				$data['item'] = $item;
				
				if(permission('PeranPenggunaLogFlag'))
					$data['flag_options'] = flagOptions();

                $data['id_pengguna'] = $id_pengguna;
                
				$this->template->build_admin('peran_pengguna_log_ctr/ubah', $data);
			}else{
				$data = array(
					'label_peran_pengguna_log' => $this->input->post('label'),
					'deskripsi' => $this->input->post('deskripsi'),
					'tanggal_mulai' => dateOutput($this->input->post('tanggal_mulai')),
					'tanggal_selesai' => dateOutput($this->input->post('tanggal_selesai'))
				);

				if(permission('PeranPenggunaLogFlag'))
					$data['flag'] = $this->input->post('flag');

				$this->Mcommon->update($this->table, $data, array('id_peran_pengguna_log' => $id));
				
				$this->session->set_flashdata('pesan', 'Data log peran pengguna berhasil diubah');
				
				redirect(URLBACK . '/log-peran-pengguna/ubah/' . $id . '?id_pengguna=' . $id_pengguna);
			}
		}
    }
    
    public function hapus($id_peran_pengguna_log)
    {
		if(permission('PeranPenggunaLogMenghapus')) {
            $id_pengguna = $this->input->get('id_pengguna');

			$this->Mcommon->update($this->table, array('flag' => 0), array('id_peran_pengguna_log' => $id_peran_pengguna_log));

            $this->session->set_flashdata('pesan', 'Data peran pengguna log berhasil dihapus');
            
            redirect(URLBACK . '/log-peran-pengguna?id_pengguna=' . $id_pengguna);
		}
    }
}