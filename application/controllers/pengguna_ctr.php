<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Pengguna_ctr extends CI_Controller {
	public $table = 'pengguna';
	public $table1 = 'peran_pengguna_log';
	public $select = 'id_pengguna, id_peran_pengguna, nama_pengguna, username, password, foto, tempat_lahir_pengguna, tanggal_lahir_pengguna, email_pengguna, alamat_pengguna, telepon_pengguna, status, jabatan, tanggal_buat, tanggal_ubah, flag';
	public $tableoption1 = 'peran_pengguna';
	public $labeloption1 = 'Peran Pengguna';
	public $option1 = array('id_peran_pengguna','nama_peran_pengguna');
	public $tableoption2 = 'pengaturan_dropdown';
	public $labeloption2 = 'Jabatan';
    public $option2 = array('value_dropdown','label_dropdown');
	
	public $rules = array(
        array('field' => 'nama_pengguna', 'label' => 'Nama Pengguna', 'rules' => 'required'),
        array('field' => 'username', 'label' => 'Username', 'rules' => 'required'),
        array('field' => 'tanggal_lahir_pengguna', 'label' => 'Tanggal Lahir Pengguna', 'rules' => 'required'),
        array('field' => 'tempat_lahir_pengguna', 'label' => 'Tempat Lahir Pengguna', 'rules' => 'required'),
        array('field' => 'telepon_pengguna', 'label' => 'Telepon Pengguna', 'rules' => ''),
        array('field' => 'email_pengguna', 'label' => 'Email Pengguna', 'rules' => 'required'),
		array('field' => 'alamat_pengguna', 'label' => 'Alamat Pengguna', 'rules' => '')
    );
	
	public function __construct() {
        parent::__construct();
		$this->load->model('Mcommon');
		$this->load->helper(array('form', 'url'));
    }

	public function index(){
		if(permission('PenggunaMengakses')) {
			$data['meta_title'] = 'Pengguna';

			$this->template->build_admin('pengguna_ctr/index', $data);
		}
	}
	
	public function get_list_ajax()
    {
		header('Content-Type: application/json;charset=utf-8');
		
		if(permission('PenggunaMengakses')) {
			if(! permission('PenggunaFlag')){
				$this->datatables->select('pen.id_pengguna, pen.nama_pengguna, pen.username, pen.email_pengguna, ppen.nama_peran_pengguna, pen.status');
				$this->datatables->where('pen.flag', 1);
			}else{
				$this->datatables->select('pen.id_pengguna, pen.nama_pengguna, pen.username, pen.email_pengguna, ppen.nama_peran_pengguna, pen.status, pen.flag');
				$this->datatables->edit_column('flag', '$1', 'flag', flagOptions('label'));
			}

			$this->datatables->from('pengguna pen');
			$this->datatables->join('peran_pengguna ppen', 'ppen.id_peran_pengguna = pen.id_peran_pengguna', 'left');
			$this->datatables->edit_column('status', '$1', 'status', statusOptions('label'));
			
			$this->datatables->add_column('log', '<a class="btn btn-sm btn-default" title="Log" href="' . base_url('admin/log-peran-pengguna?id_pengguna=') . '$1"><i class="glyphicon glyphicon-user"></i> Log Peran</a>', 'id_pengguna');

			if(permission('PenggunaEdit')) {
				$this->datatables->add_column('ubah', '<a class="btn btn-sm btn-success" title="Ubah" href="' . base_url('admin/pengguna/ubah') . '/$1"><i class="glyphicon glyphicon-pencil"></i> Ubah</a>', 'id_pengguna');
			}
			
			if(permission('PenggunaMenghapus')) {
				$this->datatables->add_column('hapus', '<a class="btn btn-sm btn-danger" title="Hapus" href="' . base_url('admin/pengguna/hapus') . '/$1"><i class="glyphicon glyphicon-trash"></i> Hapus</a>', 'id_pengguna');
			}
			
			echo $this->datatables->generate('json', 'ISO-8859-1');
		}
    }
	
	/*
	function validasi_upload(){
		if(permission('PenggunaTambah, PenggunaEdit')) {
			if (! $this->upload->do_upload()) {
				$this->form_validation->set_message('validasi_upload', $this->upload->display_errors());
				return false;
			}else{
				return true;
			}
		}
	}
	*/
	
	public function tambah(){
		if(permission('PenggunaTambah')) {
			$data['meta_title'] = 'Tambah Pengguna';
			
			$this->rules[] = array('field' => 'password', 'label' => 'Password', 'rules' => 'required');
			$this->rules[] = array('field' => 'repassword', 'label' => 're-Password', 'rules' => 'required|matches[password]');
			$this->rules[] = array('field' => 'userfile', 'label' => 'Files', 'rules' => 'callback_validasi_upload');
			$this->rules[] = array('field' => 'id_peran_pengguna', 'label' => 'Nama Peran Pengguna', 'rules' => 'required');

			$this->form_validation->set_rules($this->rules);

			if ($this->form_validation->run() === false) {
				$data['item'] = array(
					'nama_pengguna' => '',
					'username' => '',
					'email_pengguna' => '',
					'tempat_lahir_pengguna' => '',
					'tanggal_lahir_pengguna' => '',
					'foto' => 'logobumil1.png',
					'telepon_pengguna' => '',
					'flag' => '',
					'id_peran_pengguna' => '',
					'alamat_pengguna' => '',
					'password' => '',
					'status' => '',
					//'jabatan' => ''
				);
				
				if(permission('PenggunaFlag'))
					$data['flag_options'] = flagOptions();

				$data['status_options'] = statusOptions();
				$data['peran_pengguna_options'] = $this->Mcommon->get_select_options($this->tableoption1, $this->option1, $this->labeloption1);
				//$data['jabatan_options'] = $this->Mcommon->get_select_options($this->tableoption2, $this->option2, $this->labeloption2, array('grup_dropdown' => 'jabatan_pengguna'), 'label_dropdown ASC');

				$this->template->build_admin('pengguna_ctr/tambah', $data);
			} else {
				$this->db->trans_begin();

				$config['upload_path'] = 'uploads/pengguna/';
				$config['allowed_types'] = 'jpg|jpeg|png';
				$config['max_size'] = '2000';
				
				$filename = unggah($config);

				$data = array(
					'id_peran_pengguna' => $this->input->post('id_peran_pengguna'),
					'nama_pengguna' => $this->input->post('nama_pengguna'),
					'username' => $this->input->post('username'),
					'password' => md5($this->input->post('password')),
					'tempat_lahir_pengguna' => $this->input->post('tempat_lahir_pengguna'),
					'tanggal_lahir_pengguna' => dateOutput($this->input->post('tanggal_lahir_pengguna')),
					'email_pengguna' => $this->input->post('email_pengguna'),
					'alamat_pengguna' => $this->input->post('alamat_pengguna'),
					'telepon_pengguna' => $this->input->post('telepon_pengguna'),
					'foto' => 'logobumil1.png',
					'status' => $this->input->post('status'),
					//'jabatan' => $this->input->post('jabatan')
				);

				if(permission('PenggunaFlag'))
					$data['flag'] = $this->input->post('flag');

				$id = $this->Mcommon->insert($this->table, $data);
				
				$data1 = array(
					'id_peran_pengguna' => $this->input->post('id_peran_pengguna'),
					'id_pengguna' => $id,
					'flag' => 1
				);

				$id = $this->Mcommon->insert($this->table1, $data1);

				if ($this->db->trans_status() === FALSE){
					$this->db->trans_rollback();
				}
				else{
					$this->db->trans_commit();

					$this->session->set_flashdata('pesan', 'Data pengguna berhasil ditambah');
				}
				
				redirect(URLBACK . '/pengguna');
			}
		}
	}
	
	public function ubah($id){
		if(permission('PenggunaEdit') || ($this->session->userdata('id_pengguna') == $id)){
			$data['meta_title'] = 'Ubah Pengguna';
			
			$item = $this->Mcommon->find($this->select, $this->table, array('id_pengguna' => $id));
			
			if(permission('PenggunaEdit')){
				$this->rules[] = array('field' => 'id_peran_pengguna', 'label' => 'Nama Peran Pengguna', 'rules' => 'required');
			}

			if($this->input->post('password') && $this->input->post('repassword')){
                $this->rules[] = array('field' => 'password', 'label' => 'Password', 'rules' => 'trim');
                $this->rules[] = array('field' => 'repassword', 'label' => 'Ulangin Password', 'rules' => 'matches[password]|trim');
			}

			if (! $item['foto']) {
				$this->rules[] = array('field' => 'userfile', 'label' => 'Files', 'rules' => 'callback_validasi_upload');
			}
			
			$this->form_validation->set_rules($this->rules);
			
			$item['tanggal_lahir_pengguna'] = dateInput($item['tanggal_lahir_pengguna']);

			$data['item'] = $item;

			if ($this->form_validation->run() === false) {
				if(permission('PenggunaFlag'))
					$data['flag_options'] = flagOptions();

				$data['status_options'] = statusOptions();
				$data['peran_pengguna_options'] = $this->Mcommon->get_select_options($this->tableoption1, $this->option1, $this->labeloption1);
				//$data['jabatan_options'] = $this->Mcommon->get_select_options($this->tableoption2, $this->option2, $this->labeloption2, array('grup_dropdown' => 'jabatan_pengguna'), 'label_dropdown ASC');

				$this->template->build_admin('pengguna_ctr/ubah', $data);
			}else{
				$this->db->trans_begin();
				
				$data = array(
					'nama_pengguna' => $this->input->post('nama_pengguna'),
					'username' => $this->input->post('username'),
					'tempat_lahir_pengguna' => $this->input->post('tempat_lahir_pengguna'),
					'tanggal_lahir_pengguna' => dateOutput($this->input->post('tanggal_lahir_pengguna')),
					'email_pengguna' => $this->input->post('email_pengguna'),
					'alamat_pengguna' => $this->input->post('alamat_pengguna'),
					'telepon_pengguna' => $this->input->post('telepon_pengguna'),
					'status' => $this->input->post('status'),
					//'jabatan' => $this->input->post('jabatan')
				);

				if($this->input->post('password')){
					$data['password'] = md5($this->input->post('password'));
				}

				if(permission('PenggunaEdit')){
					$data['id_peran_pengguna'] = $this->input->post('id_peran_pengguna');

					if(permission('PenggunaFlag'))
						$data['flag'] = $this->input->post('flag');
				}
					
				if(! $item['foto']){
					$config['upload_path'] = 'uploads/pengguna/';
					$config['allowed_types'] = 'jpg|jpeg|png';
					$config['max_size'] = '2000';
					
					$data['foto'] = unggah($config);
				}
	
				$this->Mcommon->update($this->table, $data, array('id_pengguna' => $id));
				
				if(permission('PenggunaEdit')){
					if($item['id_peran_pengguna'] != $this->input->post('id_peran_pengguna')){
						$data1 = array(
							'id_peran_pengguna' => $this->input->post('id_peran_pengguna'),
							'id_pengguna' => $id,
							'flag' => 1
						);

						$this->Mcommon->insert($this->table1, $data1);
					}
				}
				
				if ($this->db->trans_status() === FALSE){
					$this->db->trans_rollback();
				}
				else{
					$this->db->trans_commit();

					$this->session->set_flashdata('pesan', 'Data pengguna berhasil diubah');
				}
				
				redirect(URLBACK . '/pengguna/ubah/' . $id);
			}
		}
	}
	
	public function hapus($id_pengguna)
    {
		if(permission('PenggunaMenghapus')) {
			$this->Mcommon->update($this->table, array('flag' => 0), array('id_pengguna' => $id_pengguna));

			$this->session->set_flashdata('pesan', 'Data pengguna berhasil dihapus');

			redirect(URLBACK . '/pengguna');
		}
    }
	
	public function hapus_foto($id_pengguna)
    {
		if(permission('PenggunaEdit')) {
			$item = $this->Mcommon->find($this->select, $this->table, array('id_pengguna' => $id_pengguna));
			
			$path = getcwd() . '/uploads/pengguna/' . $item['foto'];
			
			if (is_file($path)) {
				unlink($path);
			}

			$this->Mcommon->update($this->table, array('foto' => ''), array('id_pengguna' => $id_pengguna));
				
			$this->session->set_flashdata('pesan', 'Foto pengguna berhasil dihapus');

			redirect(URLBACK . '/pengguna/ubah/' . $id_pengguna);
		}
    }
}