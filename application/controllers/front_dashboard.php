<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Front_dashboard extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('Mcommon');
        $this->load->helper(array('url', 'date'));
    }

	public function index(){
        $data['meta_title'] = 'Dashboard';
        $this->template->build_frontpage('dashboard/index', $data);
        
	}
}