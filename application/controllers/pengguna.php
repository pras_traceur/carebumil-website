<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Pengguna extends CI_Controller {
    var $table = 'pengguna';
    public $tableoption1 = 'pengaturan_dropdown';
	public $labeloption1 = 'Jabatan';
    public $option1 = array('value_dropdown','label_dropdown');

	public function __construct() {
        parent::__construct();
        $this->load->model('Mcommon');
        $this->load->model('Mpengguna');
        $this->load->helper(array('url', 'date'));
    }

    public function index(){
        $data = get_konten(6, 'pengguna');

        $data['meta_title'] = $data['konten_pengguna_judul'];

        $this->load->library('pagination');
        $this->config->load('pagination', true);

        $config = $this->config->item('pagination');
        $keyword = $this->input->get('search');
        $per_page = $this->input->get('per_page');

        $result  = $this->get_pengguna(
            $per_page,
            $config['per_page'],
            $keyword
        );
        
        $total_rows = $this->Mpengguna->paginate_count($keyword);

        $config['total_rows'] = $total_rows['total_rows'];
        $config['base_url'] = base_url('pos?search=' . $keyword);

        $this->pagination->initialize($config);

        $data['pagination'] = $this->pagination->create_links();
        $data['start'] = (max(0, ($this->pagination->cur_page - 1)) * $this->pagination->per_page) + 1;
        $data['get_pengguna'] = $result;

        $this->template->build('pengguna/index', $data);
    }

    function get_pengguna($segment, $per_page, $keyword){
        $data = array();
        
        $result = $this->Mpengguna->paginate($segment, $per_page, $keyword);
        $daftar_jabatan = $this->Mcommon->get_select_options($this->tableoption1, $this->option1, $this->labeloption1, array('grup_dropdown' => 'jabatan_pengguna'));
        
        if($result){
            $i = 0;
            foreach ($result as $row)
            {
                $id_pengguna = !isset($row['id_pengguna']) ? "" : $row['id_pengguna'];
                $nama_pengguna = !isset($row['nama_pengguna']) ? "" : $row['nama_pengguna'];
                $email_pengguna = !isset($row['email_pengguna']) ? "" : $row['email_pengguna'];
                $jabatan = !isset($row['jabatan']) ? "" : $row['jabatan'];
                $foto = $this->simpleimage->resize_image($row['foto'], 'pengguna', 300);
        
                $data[$i] = array(
                    "pengguna_id" => $id_pengguna,
                    "pengguna_nama" => $nama_pengguna,
                    "pengguna_foto" => $foto,
                    "pengguna_email" => $email_pengguna,
                    "pengguna_jabatan" => $daftar_jabatan[$jabatan]
                );
                
                $i++;
            }
        }

		return $data;
    }
}