<?php

if (!defined('BASEPATH')) { exit('No direct script access allowed');}

class Command_ctr extends CI_Controller {
    public $table = 'aplikasi';
    public $select = 'id_aplikasi, nama_aplikasi, kata_kunci, foto, deskripsi, url, lihat, flag, tanggal_buat, tanggal_ubah, dibuat_oleh, status';
    
    public function __construct() {
        parent::__construct();
        $this->load->model('Mcommon');
        $this->load->helper(array('url', 'date'));
    }

	public function crawl_online()
    {
        $result = $this->Mcommon->list_array($this->select, $this->table, array("check" => 1));
     
        if($result){
            $status = "";

            foreach ($result as $row) {
                $id = $row['id_aplikasi'];
                $url = $row['url'];
                $status = $row['status'];
                
                if($this->curl_metode($url)){
                    $status = "ONLINE";
                } else { 
                    if($status != "OFFLINE"){
                        $title = pengaturan()->EMAIL_SERVER_JUDUL;
                        $from = pengaturan()->EMAIL_ALAMAT_PENGIRIM;
                        $to = pengaturan()->EMAIL_ALAMAT_PENERIMA;
                        $administrator = pengaturan()->EMAIL_NAMA_PENGIRIM;

                        $message ="
                            <h1>" . pengaturan()->EMAIL_SERVER_JUDUL . "</h1><br/>
                            Dear Administrator, 
                            <br/>
                            <br/>";

                        $message .= "Website " . $row['nama_aplikasi'] . " yang beralamat di " . $row['url'] . " sedang offline.
                            <br/>
                            <br/>";
                        
                        $message .= pengaturan()->EMAIL_SERVER_PESAN . "<br/><br/>";

                        $message .= "<strong>Regards</strong>
                            <br/>
                            <br/>
                            <br/>";
                        
                        $message .= $administrator;

                        $this->load->library('email');

                        $config = Array(
                            'smtp_host' => pengaturan()->EMAIL_SMTP_MAIL_SERVER,
                            'smtp_port' => pengaturan()->EMAIL_SMTP_PORT,
                            'smtp_user' =>  pengaturan()->EMAIL_SMTP_USERNAME,
                            'smtp_pass' => pengaturan()->EMAIL_SMTP_PASSWORD,
                            'smtp_crypto' => pengaturan()->EMAIL_SMTP_ENCRYPTION,
                            'wordwrap' => FALSE,
                            'wrapchars' => 76,
                            'mailtype' => 'html',
                            'charset' => 'utf-8',
                            'validate' => FALSE,
                            'priority' => 1,
                            'crlf' => "\r\n",
                            'newline' => "\r\n",
                            'bcc_batch_mode' => FALSE,
                            'bcc_batch_size' => 200
                        );

                        $this->email->initialize($config);
                        $this->email->from($from, $administrator);
                        $this->email->to($to);

                        $this->email->subject($title);
                        $this->email->message($message);

                        if ($this->email->send()) {
                            echo 'Email sent.';
                        } else {
                            show_error($this->email->print_debugger());
                        }
                    }

                    $status = "OFFLINE";
                }
                
                $data = array(
					'status' => $status,
				);

                $this->Mcommon->update($this->table, $data, array('id_aplikasi' => $id));
                
                echo $row['nama_aplikasi'] . " : " . $status . "<br/>";
            }
        }
    }
	
	
	public function curl_metode($url){
		$status = false;
		
		if($url){
			$ch = @curl_init(); //Cek URL LPSE
			@curl_setopt($ch, CURLOPT_URL, $url);
			@curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.1) Gecko/20061204 Firefox/2.0.0.1");
			@curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			@curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
			@curl_setopt($ch, CURLOPT_TIMEOUT, 10);

			$response = @curl_exec($ch);
			$errno = @curl_errno($ch);
			$error = @curl_error($ch);

			$response = $response;
			$info = @curl_getinfo($ch); 
			
			if($info['http_code']){ 
				$status = true;
			}
		}
		
		return $status;
	}
	
	public function fopen_metode($url){
		ini_set("default_socket_timeout","05");
		set_time_limit(5);

		if($url){
			$f=@fopen($url, "r");
			if($f){
				$r=@fread($f, 1000);
				fclose($f);

				if(strlen($r)>1) {
					return true;
				}
			}
		}
		
		return false;
	}
}
