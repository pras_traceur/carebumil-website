<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route["admin/dasbor/(:any)"]="dasbor_ctr/$1";
$route["admin/login"]="login_ctr";
$route["admin/daftar"]="login_ctr/daftar";
$route["admin/login/(:any)"]="login_ctr/$1";
$route["admin/pengaturan"]="pengaturan_ctr";
$route["admin/pengaturan/(:any)"]="pengaturan_ctr/$1";
$route["admin/pengguna"]="pengguna_ctr";
$route["admin/pengguna/(:any)"]="pengguna_ctr/$1";
$route["admin/peran-pengguna"]="peran_pengguna_ctr";
$route["admin/peran-pengguna/(:any)"]="peran_pengguna_ctr/$1";
$route["admin/log-peran-pengguna"]="peran_pengguna_log_ctr";
$route["admin/log-peran-pengguna/(:any)"]="peran_pengguna_log_ctr/$1";
$route["admin/reset-password"]="reset_password_ctr";
$route["admin/reset-password/(:any)"]="reset_password_ctr/$1";

$route["admin"]="dasbor_ctr";
$route["admin/tentang"]="dasbor_ctr/tentang";

$route["admin/test/(:any)"]="dasbor_ctr/test/$1";
$route["admin/hasil_test/(:any)"]="dasbor_ctr/hasil_test/$1";

$route["admin/datadiri/(:any)"]="dasbor_ctr/datadiri/$1";
$route["admin/submitdatadiri/(:any)"]="dasbor_ctr/submitdatadiri/$1";

$route["admin/kehamilanini/(:any)"]="dasbor_ctr/kehamilanini/$1";
$route["admin/submitkehamilan/(:any)"]="dasbor_ctr/submitkehamilan/$1";

$route["admin/riwayatobstetri/(:any)"]="dasbor_ctr/riwayatobstetri/$1";
$route["admin/submitriwayatobstetri/(:any)"]="dasbor_ctr/submitriwayatobstetri/$1";

$route["admin/dukungansosial/(:any)"]="dasbor_ctr/dukungansosial/$1";
$route["admin/submitdukungansosial/(:any)"]="dasbor_ctr/submitdukungansosial/$1";

$route["admin/persiapanpersalinan/(:any)"]="dasbor_ctr/persiapanpersalinan/$1";
$route["admin/submitpersiapanpersalinan/(:any)"]="dasbor_ctr/submitpersiapanpersalinan/$1";

$route["admin/newtest"]="dasbor_ctr/newtest";
$route["admin/showtest/(:any)"]="dasbor_ctr/showtest/$1";


$route["admin/submittest/(:any)"]="dasbor_ctr/submittest/$1";

$route["admin/detail/(:any)"]="dasbor_ctr/detail/$1";

$route["admin/tata_laksana"]="dasbor_ctr/tata_laksana";

$route['default_controller'] = "login_ctr";
$route['404_override'] = '';

$route["admin/submitdaftar"]="login_ctr/submitdaftar";

$route["api/loginApi"] = "api/loginApi";
$route["api/signUpApi"] = "api/signUpApi";
$route["api/getLastTest"] = "api/getLastTest";
$route["api/getResultTest"] = "api/getResultTest";
$route["api/apiSubmitTest"] = "api/apiSubmitTest";
$route["api/getDukunganSosial"] = "api/getDukunganSosial";
$route["api/apiSubmitDukunganSosial"] = "api/apiSubmitDukunganSosial";
$route["api/getPersiapanPersalinan"] = "api/getPersiapanPersalinan";
$route["api/apiSubmitPersiapanPersalinan"] = "api/apiSubmitPersiapanPersalinan";
$route["api/getRiwayatObstetri"] = "api/getRiwayatObstetri";
$route["api/apiSubmitRiwayatObstetri"] = "api/apiSubmitRiwayatObstetri";
$route["api/getDatadiri"] = "api/getDatadiri";
$route["api/apiSubmitDataDiri"] = "api/apiSubmitDataDiri";
$route["api/getKehamilan"] = "api/getKehamilan";
$route["api/apiSubmitKehamilan"] = "api/apiSubmitKehamilan";


/* End of file routes.php */
/* Location: ./application/config/routes.php */