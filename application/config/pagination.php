<?php

/*
$config['uri_segment'] = 2;
$config['full_tag_open'] = '<div class="col-sm-12"><ul class="pagination">';
$config['full_tag_close'] = '</ul></div>';
$config['num_tag_open'] = '<li>';
$config['num_tag_close'] = '</li>';
$config['cur_tag_open'] = '<li class="active"><a href="#">';
$config['cur_tag_close'] = '</a></li>';
$config['next_tag_open'] = '<li>';
$config['next_tag_close'] = '</li>';
$config['prev_tag_open'] = '<li>';
$config['prev_tag_close'] = '</li>';
$config['first_tag_open'] = '<li>';
$config['first_tag_close'] = '</li>';
$config['last_tag_open'] = '<li>';
$config['last_tag_close'] = '</li>';
$config['per_page'] = 10;
*/

$config['per_page'] = 12;
$config['full_tag_open'] = '<div class="col-sm-12 text-center">';
$config['full_tag_close'] = '</div>';

$config['use_page_numbers'] = true;
$config['reuse_query_string'] = true;
$config['page_query_string'] = true;
